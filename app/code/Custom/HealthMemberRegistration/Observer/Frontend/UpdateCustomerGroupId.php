<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Custom\HealthMemberRegistration\Observer\Frontend;

class UpdateCustomerGroupId implements \Magento\Framework\Event\ObserverInterface
{

    const CUSTOMER_GROUP_ID = 4;

    protected $_customerRepositoryInterface;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerRepositoryInterface,
        \Psr\Log\LoggerInterface $logger

    ) {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        $this->_logger->debug('Change customer group for: '. $customer->getId());
        $this->_logger->debug('Current group id: '. $customer->getGroupId());
        $this->_logger->debug('New group id: '. $customer->getMembershipCode());
        $attr_membership_code = $customer->getMembershipCode();
        if (!empty($attr_membership_code)) {
            if ( strtoupper ( $attr_membership_code ) == '100MEMB' ) {
                $customer->setGroupId(self::CUSTOMER_GROUP_ID);
            } else {
                $customer->setGroupId(1);
            }
        }
    }
}

