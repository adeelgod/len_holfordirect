<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Ui\DataProvider\Form\Report\Modifier;

use Amasty\ReportBuilder\Api\ColumnInterface;
use Amasty\ReportBuilder\Api\Data\ReportInterface;
use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Amasty\ReportBuilder\Model\ReportRegistry;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;

class Relations implements ModifierInterface
{
    const SCHEME_DATA_KEY = 'scheme_relations';
    const SCHEME_DATA_NAME = 'entity_name';
    const SCHEME_DATA_ENTITY_INDEX = 'entity_index';
    const SCHEME_DATA_RELATIONS_QTY = 'relationsQty';

    /**
     * @var ReportRegistry
     */
    private $reportRegistry;

    /**
     * @var Provider
     */
    private $schemeProvider;

    public function __construct(
        ReportRegistry $reportRegistry,
        Provider $schemeProvider
    ) {
        $this->reportRegistry = $reportRegistry;
        $this->schemeProvider = $schemeProvider;
    }

    public function modifyData(array $data)
    {
        $report = $this->reportRegistry->getReport();

        $relations = [];
        $index = 0;

        foreach ($report->getRelationScheme() as $relationData) {
            $entityName = $relationData[ReportInterface::SCHEME_SOURCE_ENTITY];
            $relatedEntity = $relationData[ReportInterface::SCHEME_ENTITY];

            if (!isset($relations[$entityName])) {
                $relations[$entityName] = $this->getEntityData($entityName, $index++);
            }

            if (!isset($relations[$relatedEntity])) {
                $relations[$relatedEntity] = $this->getEntityData($relatedEntity, $index++);
            }
        }

        if (empty($relations) && $report->getMainEntity()) {
            $relations[] = $this->getEntityData($report->getMainEntity(), $index);
        }

        $data[$report->getReportId()][self::SCHEME_DATA_KEY] = array_values($relations);

        return $data;
    }

    public function modifyMeta(array $meta)
    {
        return $meta;
    }

    private function getEntityData(string $entityName, int $index): array
    {
        $scheme = $this->schemeProvider->getEntityScheme();
        $relatedEntities = $scheme->getEntityByName($entityName)->getRelatedEntities();
        return [
            self::SCHEME_DATA_NAME => $entityName,
            self::SCHEME_DATA_ENTITY_INDEX => $index,
            self::SCHEME_DATA_RELATIONS_QTY => count($relatedEntities)
        ];
    }
}
