<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Ui\DataProvider\Form\Report\Modifier;

use Amasty\ReportBuilder\Model\Report\EntityProvider;
use Amasty\ReportBuilder\Model\ReportRegistry;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;

class Entities implements ModifierInterface
{
    const DATA_KEY = 'entities';

    /**
     * @var ReportRegistry
     */
    private $reportRegistry;

    /**
     * @var EntityProvider
     */
    private $entityProvider;

    public function __construct(
        ReportRegistry $reportRegistry,
        EntityProvider $entityProvider
    ) {
        $this->reportRegistry = $reportRegistry;
        $this->entityProvider = $entityProvider;
    }

    public function modifyData(array $data)
    {
        $report = $this->reportRegistry->getReport();
        if (isset($data[$report->getReportId()]) && $report->getMainEntity()) {
            $data[$report->getReportId()][self::DATA_KEY] = $this->getEntitiesData();
        }

        return $data;
    }

    public function modifyMeta(array $meta)
    {
        return $meta;
    }

    private function getEntitiesData(): array
    {
        $report = $this->reportRegistry->getReport();
        $entities = array_unique(array_merge([$report->getMainEntity()], $report->getAllEntities()));

        return array_values($this->entityProvider->getEntities($entities));
    }
}
