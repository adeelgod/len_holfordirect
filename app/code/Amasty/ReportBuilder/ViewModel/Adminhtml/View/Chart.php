<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\ViewModel\Adminhtml\View;

use Amasty\ReportBuilder\Api\ColumnInterface;
use Amasty\ReportBuilder\Model\EntityScheme\Column\OptionsResolver;
use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Amasty\ReportBuilder\Model\ReportResolver;
use Amasty\ReportBuilder\Model\ResourceModel\Report\Data\Collection;
use Amasty\ReportBuilder\Model\ResourceModel\Report\Data\CollectionFactory;
use Amasty\ReportBuilder\Model\Source\IntervalType;
use Amasty\ReportBuilder\Model\View\FilterApplier;
use Amasty\ReportBuilder\Model\View\FiltersProvider;
use Amasty\ReportBuilder\Model\View\ReportFiltersResolver;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Chart implements ArgumentInterface
{
    const AXIS_TYPE_TEXT = 'text';

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var ReportResolver
     */
    private $resolver;

    /**
     * @var FilterApplier
     */
    private $filterApplier;

    /**
     * @var FiltersProvider
     */
    private $filtersProvider;

    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var ReportFiltersResolver
     */
    private $reportFiltersResolver;

    /**
     * @var OptionsResolver
     */
    private $optionsResolver;

    public function __construct(
        CollectionFactory $collectionFactory,
        ReportResolver $resolver,
        FilterApplier $filterApplier,
        FiltersProvider $filtersProvider,
        Provider $provider,
        ReportFiltersResolver $reportFiltersResolver,
        OptionsResolver $optionsResolver
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->resolver = $resolver;
        $this->filterApplier = $filterApplier;
        $this->filtersProvider = $filtersProvider;
        $this->provider = $provider;
        $this->reportFiltersResolver = $reportFiltersResolver;
        $this->optionsResolver = $optionsResolver;
    }

    public function getCollection(): Collection
    {
        $report = $this->resolver->resolve();
        $collection = $this->collectionFactory->create();
        $collection->setReportId($report->getReportId());
        $this->filterApplier->execute($report, $collection);

        $reportFilters = $this->reportFiltersResolver->resolve();
        foreach ($reportFilters as $filterAlias => $filter) {
            $collection->addFieldToFilter($filterAlias, $filter);
        }

        return $collection;
    }

    public function getChartData(): array
    {
        $report = $this->resolver->resolve();
        $scheme = $this->provider->getEntityScheme();
        $axisX = $scheme->getColumnById($report->getChartAxisX());
        $axisXOptions = $this->optionsResolver->resolve($axisX);
        $axisY = $scheme->getColumnById($report->getChartAxisY());
        $axisYOptions = $this->optionsResolver->resolve($axisY);

        $chartData = [];
        foreach ($this->getCollection() as $item) {
            $chartData[] = [
                'valueX' => $this->getItemData($axisX, $item, $axisXOptions),
                'valueY' => $this->getItemData($axisY, $item, $axisYOptions),
            ];
        }

        return $chartData;
    }

    private function getItemData(ColumnInterface $column, DataObject $item, array $options = []): string
    {
        if (in_array($column->getFrontendModel(), ['select', 'multiselect']) && !empty($options)) {
            $aggregatedOptions = explode(',', (string)$item->getData($column->getAlias()));
            $preparedOptions = [];
            foreach ($aggregatedOptions as $option) {
                foreach ($options as $columnOption) {
                    if ($columnOption['value'] == $option) {
                        $preparedOptions[] = $columnOption['label'];
                    }
                }
            }
            if (!$preparedOptions) {
                $preparedOptions = $aggregatedOptions;
            }
            return str_replace('-', ' ', implode(', ', $preparedOptions));
        }

        return str_replace('-', ' ', $item->getData($column->getAlias()));
    }

    public function getXAxisType(): string
    {
        $report = $this->resolver->resolve();
        return $this->getAxisType($report->getChartAxisX());
    }

    public function getYAxisType(): string
    {
        $report = $this->resolver->resolve();
        return $this->getAxisType($report->getChartAxisY());
    }

    private function getAxisType(string $columnId): string
    {
        $scheme = $this->provider->getEntityScheme();
        $columnScheme = $scheme->getColumnById($columnId);

        if (in_array($columnScheme->getFrontendModel(), ['select', 'multiselect'])) {
            return self::AXIS_TYPE_TEXT;
        }

        return $columnScheme->getType();
    }

    public function getInterval(): string
    {
        return $this->filtersProvider->getInterval() ?: IntervalType::TYPE_DAY;
    }

    public function isShowChart(): bool
    {
        $report = $this->resolver->resolve();

        return $report->getDisplayChart();
    }
}
