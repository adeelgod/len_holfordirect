<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Controller\Adminhtml\Report;

use Amasty\ReportBuilder\Model\Report\EntityProvider;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;

class MainEntity extends Action implements HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'Amasty_ReportBuilder::report_edit';
    const PARAM_NAME = 'entityName';

    /**
     * @var EntityProvider
     */
    private $entityProvider;

    /**
     * @var Json
     */
    private $resultJson;

    public function __construct(
        Context $context,
        EntityProvider $entityProvider
    ) {
        parent::__construct($context);
        $this->entityProvider = $entityProvider;
    }

    public function execute()
    {
        $entityName = $this->getRequest()->getParam(self::PARAM_NAME);
        $this->resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if (!$entityName) {
            return $this->resultJson->setData(['error' => __('We can\'t find entity name.')]);
        }

        try {
            $entitiesData = $this->entityProvider->getEntities([$entityName]);
        } catch (\Exception $e) {
            return $this->resultJson->setData(['error' => $e->getMessage()]);
        }

        return $this->resultJson->setData($entitiesData);
    }
}
