<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Controller\Adminhtml\Report;

use Amasty\ReportBuilder\Model\View\ReportLoader;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;

class View extends Action implements HttpGetActionInterface
{
    const ADMIN_RESOURCE = 'Amasty_ReportBuilder::report_view';

    /**
     * @var ReportLoader
     */
    private $reportLoader;

    public function __construct(
        Context $context,
        ReportLoader $reportLoader
    ) {
        parent::__construct($context);
        $this->reportLoader = $reportLoader;
    }

    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        try {
            $report = $this->reportLoader->execute();
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage(__('This Report no longer exists.'));
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/');
        }

        $resultPage->getConfig()->getTitle()->prepend($report->getName());

        return $resultPage;
    }
}
