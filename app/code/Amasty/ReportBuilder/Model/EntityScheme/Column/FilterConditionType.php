<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\EntityScheme\Column;

class FilterConditionType
{
    const CONDITION_VALUE = 'value';
    const CONDITION_FROM = 'from';
    const CONDITION_TO = 'to';
}
