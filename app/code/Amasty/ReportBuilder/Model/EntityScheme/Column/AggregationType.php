<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\EntityScheme\Column;

class AggregationType
{
    const TYPE_NONE = 'none';
    const TYPE_MIN = 'min';
    const TYPE_MAX = 'max';
    const TYPE_AVG = 'avg';
    const TYPE_SUM = 'sum';
    const TYPE_COUNT = 'count';
    const TYPE_GROUP_CONCAT = 'group_concat';

    public function getSimpleAggregationsType(): array
    {
        return [
            self::TYPE_NONE => '%s',
            self::TYPE_MIN => 'MIN(%s)',
            self::TYPE_MAX => 'MAX(%s)',
            self::TYPE_AVG => 'AVG(%s)',
            self::TYPE_SUM => 'SUM(%s)',
            self::TYPE_COUNT => 'COUNT(DISTINCT %s)',
            self::TYPE_GROUP_CONCAT => 'GROUP_CONCAT(DISTINCT %s separator ",")',
        ];
    }

    /**
     * Check if expression correct for requested type
     *
     * @param string $expression
     * @param string $requestedType
     * @return bool
     */
    public function checkTypeExpression(string $expression, string $requestedType): bool
    {
        $aggregations = $this->getSimpleAggregationsType();

        return isset($aggregations[$requestedType]) && $aggregations[$requestedType] == $expression;
    }

    /**
     * Retrieve aggregation type for main select
     *
     * @param string $aggregationType
     * @return string
     */
    public function getParentAggregationExpression(string $aggregationType): string
    {
        if ($aggregationType == self::TYPE_COUNT) {
            $aggregationType = self::TYPE_SUM;
        }

        $types = $this->getSimpleAggregationsType();

        return $types[$aggregationType];
    }
}
