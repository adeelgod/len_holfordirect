<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\EntityScheme\Builder\Xml\Reader\Converter\NodeParser;

use Amasty\ReportBuilder\Api\ColumnInterface;

class ColumnParser implements ParserInterface
{
    public function parse(\DOMNode $childNode): array
    {
        $output = [];
        foreach ($childNode->getElementsByTagName('column') as $columnNode) {
            $name = $columnNode->getAttribute(ColumnInterface::NAME);
            $output[$name] = [
                ColumnInterface::NAME => $name,
                ColumnInterface::EAV_ATTRIBUTE => filter_var(
                    $columnNode->getAttribute(ColumnInterface::EAV_ATTRIBUTE),
                    FILTER_VALIDATE_BOOLEAN
                ),
                ColumnInterface::USE_FOR_PERIOD => filter_var(
                    $columnNode->getAttribute(ColumnInterface::USE_FOR_PERIOD_ATTRIBUTE),
                    FILTER_VALIDATE_BOOLEAN
                ),
                ColumnInterface::FRONTEND_MODEL => $columnNode->getAttribute(ColumnInterface::FRONTEND_MODEL_ATTRIBUTE)
                    ?: '',
                ColumnInterface::PRIMARY => filter_var(
                    $columnNode->getAttribute(ColumnInterface::PRIMARY),
                    FILTER_VALIDATE_BOOLEAN
                ),
            ];

            foreach ($columnNode->childNodes as $item) {
                if ($item->nodeType != XML_ELEMENT_NODE) {
                    continue;
                }
                if ($item->nodeName == 'options') {
                    $output[$name]['options'] = $this->parseOptions($item);
                } elseif ($item->nodeName === ColumnInterface::HIDDEN) {
                    $output[$name][ColumnInterface::HIDDEN] = filter_var(
                        $item->nodeValue,
                        FILTER_VALIDATE_BOOLEAN
                    );
                } else {
                    $output[$name][$item->nodeName] = $item->nodeValue;
                }
            }
        }

        return $output;
    }

    private function parseOptions(\DOMNode $item): array
    {
        $options = [];
        foreach ($item->getElementsByTagName('option') as $optionNode) {
            $optionName = $optionNode->getAttribute('name');
            $options[$optionName] = $optionNode->nodeValue;
        }

        return $options;
    }
}
