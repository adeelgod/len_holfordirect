<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\EntityScheme\Relation;

class Type
{
    const TYPE_COLUMN = 'column';
    const TYPE_TABLE = 'table';

    const ONE_TO_ONE = 'one_to_one';
    const MANY_TO_ONE = 'many_to_one';
    const ONE_TO_MANY = 'one_to_many';
    const MANY_TO_MANY = 'many_to_many';
}
