<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\Backend\Report\DataCollector;

use Amasty\ReportBuilder\Api\Data\ReportInterface;
use Amasty\ReportBuilder\Model\Backend\Report\DataCollectorInterface;
use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;

class Scheme implements DataCollectorInterface
{
    const SCHEME_DATA_KEY = 'scheme_relations';
    const SCHEME_DATA_NAME = 'entity_name';

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var Provider
     */
    private $schemeProvider;

    public function __construct(
        Json $serializer,
        Provider $schemeProvider
    ) {
        $this->serializer = $serializer;
        $this->schemeProvider = $schemeProvider;
    }

    public function collect(ReportInterface $report, array $inputData): array
    {
        $result = [];
        if (!isset($inputData[self::SCHEME_DATA_KEY])) {
            return $result;
        }

        $entitiesData = $this->prepareEntitiesData($inputData);

        $result[ReportInterface::SCHEME] = [];
        foreach ($entitiesData as $entityName => $relations) {
            foreach ($relations as $relation) {
                if (isset($entitiesData[$relation])) {
                    $result[ReportInterface::SCHEME][] = [
                        ReportInterface::SCHEME_SOURCE_ENTITY => $entityName,
                        ReportInterface::SCHEME_ENTITY => $relation
                    ];
                }
            }
            unset($entitiesData[$entityName]);
        }
        
        return $result;
    }

    private function prepareEntitiesData(array $inputData): array
    {
        try {
            $schemeData = $this->serializer->unserialize($inputData[self::SCHEME_DATA_KEY]);
        } catch (\InvalidArgumentException $e) {
            throw new LocalizedException(__('The problem occurred while parsing scheme\'s json'), $e);
        }

        $result = [];
        $entityScheme = $this->schemeProvider->getEntityScheme();
        foreach ($schemeData as $entity) {
            if (isset($entity[self::SCHEME_DATA_NAME])) {
                $entityObject = $entityScheme->getEntityByName($entity[self::SCHEME_DATA_NAME]);
                $result[$entity[self::SCHEME_DATA_NAME]] = $entityObject->getRelatedEntities();
            }
        }

        return $result;
    }
}
