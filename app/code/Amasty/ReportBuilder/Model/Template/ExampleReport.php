<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\Template;

use Amasty\ReportBuilder\Model\ReportRepository;
use Magento\Framework\Filesystem\Glob;

class ExampleReport
{
    /**
     * @var ReportRepository
     */
    private $reportRepository;

    public function __construct(
        ReportRepository $reportRepository
    ) {
        $this->reportRepository = $reportRepository;
    }

    public function createExampleReport(): void
    {
        $paths = $this->getXmlTemplatesPaths();
        foreach ($paths as $path) {
            $xmlDoc = simplexml_load_file($path);
            $templateData = $this->parseNode($xmlDoc);
            $this->createReport($templateData);
        }
    }

    private function parseNode(\SimpleXMLElement $node): array
    {
        $data = [];
        foreach ($node as $keyNode => $value) {
            if (count($value)) {
                $data[$keyNode] = $this->parseNode($value);
            } else {
                $data[$keyNode] = (string)$value;
            }
        }

        return $data;
    }

    private function createReport(array $data): void
    {
        $report = $this->reportRepository->getNew();
        $report->addData($data);
        $this->reportRepository->save($report);
    }

    private function getXmlTemplatesPaths(): array
    {
        $p = strrpos(__DIR__, DIRECTORY_SEPARATOR);
        $directoryPath = $p ? substr(__DIR__, 0, $p) : __DIR__;
        $directoryPath .= '/../etc/adminhtml/example/';

        return Glob::glob($directoryPath . '*.xml');
    }
}
