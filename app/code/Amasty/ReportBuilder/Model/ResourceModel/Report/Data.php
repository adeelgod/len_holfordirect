<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\ResourceModel\Report;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Data extends AbstractDb
{
    // phpcs:disable Magento2.CodeAnalysis.EmptyBlock.DetectedFunction
    protected function _construct()
    {
    }
}
