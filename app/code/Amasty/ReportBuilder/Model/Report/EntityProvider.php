<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\Report;

use Amasty\ReportBuilder\Api\EntityInterface;
use Amasty\ReportBuilder\Model\EntityScheme\Provider;

class EntityProvider
{
    /**
     * @var array
     */
    private $entityOptions = [];

    /**
     * @var Provider
     */
    private $entitySchemeProvider;

    /**
     * @var EntitiesDataModifierInterface
     */
    private $dataModifier;

    public function __construct(
        Provider $entitySchemeProvider,
        EntitiesDataModifierInterface $dataModifier
    ) {
        $this->entitySchemeProvider = $entitySchemeProvider;
        $this->dataModifier = $dataModifier;
    }

    public function getEntities(array $entityNames): array
    {
        $entityScheme = $this->entitySchemeProvider->getEntityScheme();
        $entities = [];
        $processedEntities = [];
        foreach ($entityNames as $entityName) {
            $entity = $entityScheme->getEntityByName($entityName);
            $entities[] = $this->prepareRelationLabels($entity->toArray());
            $processedEntities[] = $entityName;
        }

        foreach ($processedEntities as $entityName) {
            $entity = $entityScheme->getEntityByName($entityName);
            foreach ($entity->getRelatedEntities() as $relatedEntityName) {
                if (!in_array($relatedEntityName, $processedEntities)) {
                    $entity = $entityScheme->getEntityByName($relatedEntityName);
                    $entities[] = $this->prepareRelationLabels($entity->toArray());
                    $processedEntities[] = $relatedEntityName;
                }
            }
        }

        return $this->dataModifier->modify($entities);
    }

    private function prepareRelationLabels(array $entityData = []): array
    {
        if (isset($entityData[EntityInterface::RELATIONS])) {
            foreach ($entityData[EntityInterface::RELATIONS] as $relationName => &$relationData) {
                $relationData[EntityInterface::TITLE] = $this->getRelationTitleByName($relationName);
            }
        }

        return $entityData;
    }

    private function getRelationTitleByName(string $name): string
    {
        if (empty($this->entityOptions)) {
            $scheme = $this->entitySchemeProvider->getEntityScheme();
            $this->entityOptions = $scheme->getAllEntitiesOptionArray();
        }

        return $this->entityOptions[$name] ?? '';
    }
}
