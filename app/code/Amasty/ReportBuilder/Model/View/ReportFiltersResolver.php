<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\View;

use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Amasty\ReportBuilder\Model\ReportResolver;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnFilter\FilterConditionResolver;
use Amasty\ReportBuilder\Model\SelectResolver\Context;
use Magento\Framework\Serialize\Serializer\Json;

class ReportFiltersResolver
{
    /**
     * @var ReportResolver
     */
    private $reportResolver;

    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var FilterConditionResolver
     */
    private $conditionResolver;

    /**
     * @var Json
     */
    private $serializer;

    public function __construct(
        Context $context,
        FilterConditionResolver $conditionResolver,
        Json $serializer
    ) {
        $this->reportResolver = $context->getReportResolver();
        $this->provider = $context->getEntitySchemeProvider();
        $this->conditionResolver = $conditionResolver;
        $this->serializer = $serializer;
    }

    public function resolve(): array
    {
        $filters = [];
        $report = $this->reportResolver->resolve();
        $scheme = $this->provider->getEntityScheme();

        foreach ($report->getAllFilters() as $columnId => $filter) {
            $filter = $this->serializer->unserialize($filter);

            if (is_array($filter)) {
                $column = $scheme->getColumnById($columnId);
                $condition = $this->conditionResolver->resolve($column->getType(), $filter);
                $filters[str_replace('.', '_', $columnId)] = $condition;
            }
        }

        return $filters;
    }
}
