<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\View\Ui\Component\Listing;

use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Api\BookmarkManagementInterface;
use Magento\Ui\Api\BookmarkRepositoryInterface;

class BookmarkCleaner
{
    const VIEW_LISTING = 'amreportbuilder_view_listing';

    /**
     * @var BookmarkRepositoryInterface
     */
    private $bookmarkRepository;

    /**
     * @var BookmarkManagementInterface
     */
    private $bookmarkManagement;

    public function __construct(
        BookmarkRepositoryInterface $bookmarkRepository,
        BookmarkManagementInterface $bookmarkManagement
    ) {
        $this->bookmarkManagement = $bookmarkManagement;
        $this->bookmarkRepository = $bookmarkRepository;
    }

    /**
     * @param int $reportId
     */
    public function cleanByReportId(int $reportId): void
    {
        $namespace = sprintf('%s_%s', self::VIEW_LISTING, $reportId);

        if (!empty($namespace)) {
            $bookmarks = $this->bookmarkManagement->loadByNamespace($namespace);

            foreach ($bookmarks->getItems() as $bookmark) {
                try {
                    $this->bookmarkRepository->delete($bookmark);
                } catch (LocalizedException $e) {
                    continue;
                }
            }
        }
    }
}
