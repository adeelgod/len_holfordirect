<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\View\Ui\Component\Listing;

use Amasty\ReportBuilder\Api\ColumnInterface;
use Amasty\ReportBuilder\Model\ReportResolver;
use Amasty\ReportBuilder\Model\SelectResolver\Context;
use Amasty\Base\Model\Serializer;

class DefaultConfigurationProvider
{
    /**
     * @var ReportResolver
     */
    private $reportResolver;

    /**
     * @var \Amasty\ReportBuilder\Model\EntityScheme\Provider
     */
    private $schemeProvider;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(
        Context $context,
        Serializer $serializer
    ) {
        $this->reportResolver = $context->getReportResolver();
        $this->schemeProvider = $context->getEntitySchemeProvider();
        $this->serializer = $serializer;
    }

    public function execute(): array
    {
        $report = $this->reportResolver->resolve();
        if (!$report->getReportId()) {
            return [];
        }

        $config = $this->getDefaultConfig();
        $this->modifyFilters($config);
        $this->modifyOrders($config);

        return $config;
    }

    private function getDefaultConfig(): array
    {
        $config = [];
        $scheme = $this->schemeProvider->getEntityScheme();
        $report = $this->reportResolver->resolve();
        $counter = 0;
        foreach ($report->getAllColumns() as $columnId => $columnData) {
            $column = $scheme->getColumnById($columnId);
            $config['current']['columns'][$column->getAlias()]['sorting'] = false;
            $config['current']['columns'][$column->getAlias()]['visible'] = true;
            $config['current']['positions'][$column->getAlias()] = $counter++;
        }

        if (!isset($config['current']['filters'])) {
            $config['current']['filters']['applied']['placeholder'] = true;
        }
        $config['current']['displayMode'] = 'grid';

        return $config;
    }

    private function modifyFilters(&$config): void
    {
        $report = $this->reportResolver->resolve();
        $scheme = $this->schemeProvider->getEntityScheme();
        foreach ($report->getAllColumns() as $columnId => $columnData) {
            if (isset($columnData[ColumnInterface::FILTER]) && $columnData[ColumnInterface::FILTER]) {
                $filter = $this->serializer->unserialize($columnData[ColumnInterface::FILTER]);
                $column = $scheme->getColumnById($columnId);
                $config['current']['filters']['applied'][$column->getAlias()] = $filter['value'] ?? $filter;
            }
        }
    }

    private function modifyOrders(&$config): void
    {
        $report = $this->reportResolver->resolve();
        $scheme = $this->schemeProvider->getEntityScheme();
        $columnId = $report->getSortingColumnId();
        if ($columnId) {
            $alias = $scheme->getColumnById($columnId)->getAlias();
        } else {
            $mainEntity = $scheme->getEntityByName($report->getMainEntity());
            $alias = $mainEntity->getPrimaryColumn()->getAlias();
            if ($report->getUsePeriod()) {
                $alias = $mainEntity->getPeriodColumn()->getAlias();
            }
        }

        $config['current']['columns'][$alias]['sorting'] = strtolower($report->getSortingColumnExpression());
    }
}
