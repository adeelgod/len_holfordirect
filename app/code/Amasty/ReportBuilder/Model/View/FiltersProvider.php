<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\View;

use Amasty\ReportBuilder\Model\ReportRegistry;
use Amasty\ReportBuilder\Model\Source\IntervalType;
use Amasty\ReportBuilder\ViewModel\Adminhtml\View\Toolbar;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\Store;

class FiltersProvider
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var ReportRegistry
     */
    private $reportRegistry;

    public function __construct(
        RequestInterface $request,
        TimezoneInterface $timezone,
        ReportRegistry $reportRegistry
    ) {
        $this->request = $request;
        $this->timezone = $timezone;
        $this->reportRegistry = $reportRegistry;
    }

    public function getDateFilter(): array
    {
        $filters = $this->request->getParam('report');
        if ($filters && isset($filters['from']) && isset($filters['to'])) {
            $filter = ['from' => $filters['from'], 'to' => $filters['to']];
        } else {
            $filter = [
                'from' => $this->timezone->date(strtotime(Toolbar::DEFAULT_FROM)),
                'to' => $this->timezone->date(time())
            ];
        }

        return $filter;
    }

    public function getInterval(): string
    {
        $interval = '';
        $filters = $this->request->getParam('report');
        if ($filters && isset($filters['interval']) && $filters['interval']) {
            $interval = $filters['interval'];
        }

        return (string) $interval;
    }

    public function getStoreId(): int
    {
        $filters = $this->request->getParam('report');
        if ($filters && isset($filters['store']) && $filters['store']) {
            $filter = $filters['store'];
        } else {
            $storeIds = $this->reportRegistry->getReport()->getStoreIds();
            $filter = $storeIds[0] ?? Store::DEFAULT_STORE_ID;
        }

        return (int) $filter;
    }
}
