<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver;

use Amasty\ReportBuilder\Model\EntityScheme\Column\AggregationType;
use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Amasty\ReportBuilder\Model\ReportResolver;
use Amasty\ReportBuilder\Model\ResourceModel\Report;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolverInterface;
use Amasty\ReportBuilder\Model\SelectResolver\EntitySimpleRelationResolver;

class ColumnExpressionResolver implements ColumnExpressionResolverInterface
{
    /**
     * @var ColumnResolverInterface
     */
    private $columnResolver;

    /**
     * @var EntitySimpleRelationResolver
     */
    private $simpleRelationResolver;

    /**
     * @var ReportResolver
     */
    private $reportResolver;

    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var AggregationType
     */
    private $aggregationType;

    /**
     * @var Report
     */
    private $reportResource;

    public function __construct(
        ColumnResolverInterface $columnResolver,
        EntitySimpleRelationResolver $simpleRelationResolver,
        ReportResolver $reportResolver,
        Provider $provider,
        AggregationType $aggregationType,
        Report $reportResource
    ) {
        $this->columnResolver = $columnResolver;
        $this->simpleRelationResolver = $simpleRelationResolver;
        $this->reportResolver = $reportResolver;
        $this->provider = $provider;
        $this->aggregationType = $aggregationType;
        $this->reportResource = $reportResource;
    }
    
    public function resolve(string $columnAlias, bool $useInternal = false): string
    {
        $columnId = $this->resolveColumnId($columnAlias);
        $columns = $this->columnResolver->resolve();
        $simpleRelations = $this->simpleRelationResolver->resolve();
        $scheme = $this->provider->getEntityScheme();

        if (isset($columns[$columnId])) {
            $columnData = $columns[$columnId];

            $alias = $columnId;

            if (!$useInternal) {
                if ($scheme->getColumnById($columnId)->getEavAttribute()
                    || !in_array($columnData[ColumnResolverInterface::ENTITY_NAME], $simpleRelations)
                ) {
                    $alias = $columnData[ColumnResolverInterface::ALIAS];
                }
            }

            if ($columnData[ColumnAggregationTypeResolver::USE_AGGREGATION]) {
                $expression = $columnData[ColumnAggregationTypeResolver::AGGREGATED_EXPRESSION];
                if (!$useInternal) {
                    $columnAggregationType = $scheme->getColumnById($columnId)->getAggregationType();
                    $expression = $this->aggregationType->getParentAggregationExpression($columnAggregationType);
                }
                $alias = sprintf($expression, $alias);
            }

            return $alias;

        }

        return $columnAlias;
    }

    private function resolveColumnId(string $columnAlias): string
    {
        $columns = $this->columnResolver->resolve();

        foreach ($columns as $columnId => $columnData) {
            if ($columnData[ColumnResolverInterface::ALIAS] == $columnAlias) {
                return $columnId;
            }
        }

        return $columnAlias;
    }
}
