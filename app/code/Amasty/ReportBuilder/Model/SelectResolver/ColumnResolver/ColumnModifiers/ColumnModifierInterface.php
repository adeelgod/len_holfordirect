<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnModifiers;

interface ColumnModifierInterface
{
    public function modify(array $columns): array;
}
