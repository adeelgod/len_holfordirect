<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnModifiers\AggregationTypeModifiers;

use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnModifiers\ColumnModifierInterface;

class ComplexAggregationModifier implements ColumnModifierInterface
{
    public function modify(array $columns): array
    {
        return $columns;
    }
}
