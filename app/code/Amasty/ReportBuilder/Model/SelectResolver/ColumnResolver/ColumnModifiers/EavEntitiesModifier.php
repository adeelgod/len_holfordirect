<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnModifiers;

use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Magento\Framework\Exception\LocalizedException;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolverInterface;

class EavEntitiesModifier implements ColumnModifierInterface
{
    /**
     * @var Provider
     */
    private $provider;

    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param array $columns
     * @return array
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function modify(array $columns): array
    {
        foreach ($columns as $key => $column) {
            $schemeColumn = $this->provider->getEntityScheme()->getColumnById($key);

            if (!$schemeColumn->getEavAttribute()) {
                continue;
            }

            $entityName = explode('.', $key)[0] ?? '';
            $alias = str_replace('.', '_', $key);
            $columns[$key] = [
                ColumnResolverInterface::ALIAS => $alias,
                ColumnResolverInterface::EXPRESSION => sprintf('%s_table.value', $alias),
                ColumnResolverInterface::ENTITY_NAME => $entityName,
                ColumnResolverInterface::ATTRIBUTE_ID => $schemeColumn->getAttributeId()
            ];
        }

        return $columns;
    }
}
