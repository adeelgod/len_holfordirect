<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnModifiers\AggregationTypeModifiers;

use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Amasty\ReportBuilder\Model\EntityScheme\Column\AggregationType;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnAggregationTypeResolver;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnModifiers\ColumnModifierInterface;
use Magento\Framework\Exception\LocalizedException;

class SimpleAggregationModifier implements ColumnModifierInterface
{
    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var AggregationType
     */
    private $aggregationType;

    public function __construct(
        Provider $provider,
        AggregationType $aggregationType
    ) {
        $this->provider = $provider;
        $this->aggregationType = $aggregationType;
    }

    /**
     * @param array $columns
     * @return array
     * @throws LocalizedException
     * @SuppressWarnings("unused")
     */
    public function modify(array $columns): array
    {
        $simpleAggregationsType = $this->aggregationType->getSimpleAggregationsType();
        foreach ($columns as $key => $column) {
            $schemeColumn = $this->provider->getEntityScheme()->getColumnById($key);
            $aggregationType = $schemeColumn->getAggregationType();
            if (in_array($aggregationType, array_keys($simpleAggregationsType))) {
                $columns[$key][ColumnAggregationTypeResolver::AGGREGATED_EXPRESSION] =
                    $simpleAggregationsType[$aggregationType];
            }
        }

        return $columns;
    }
}
