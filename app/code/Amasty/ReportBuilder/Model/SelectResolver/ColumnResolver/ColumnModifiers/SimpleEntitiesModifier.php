<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnModifiers;

use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolverInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;

class SimpleEntitiesModifier implements ColumnModifierInterface
{
    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var ResourceConnection
     */
    private $connectionResource;

    public function __construct(
        Provider $provider,
        ResourceConnection $connectionResource
    ) {
        $this->provider = $provider;
        $this->connectionResource = $connectionResource;
    }

    /**
     * @param array $columns
     * @return array
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function modify(array $columns): array
    {
        foreach ($columns as $key => $columnData) {
            $column = $this->provider->getEntityScheme()->getColumnById($key);

            if ($column->getEavAttribute()) {
                continue;
            }

            $entityName = $column->getEntityName();

            $columns[$key] = [
                ColumnResolverInterface::ALIAS => str_replace('.', '_', $key),
                ColumnResolverInterface::EXPRESSION => $this->getExpression($entityName, $column->getName()),
                ColumnResolverInterface::ENTITY_NAME => $entityName
            ];

        }

        return $columns;
    }

    private function getExpression(string $entityName, string $columnName): string
    {
        $schemeEntity = $this->provider->getEntityScheme()->getEntityByName($entityName);

        return sprintf(
            '%s.%s',
            $this->connectionResource->getTableName($schemeEntity->getMainTable()),
            $columnName
        );
    }
}
