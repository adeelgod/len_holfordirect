<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnModifiers\AggregationTypeModifiers;

use Amasty\ReportBuilder\Model\EntityScheme\Column\AggregationType;
use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Amasty\ReportBuilder\Model\ReportResolver;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnAggregationTypeResolver;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnModifiers\ColumnModifierInterface;
use Amasty\ReportBuilder\Model\SelectResolver\EntitySimpleRelationResolver;
use Magento\Framework\Exception\LocalizedException;

class AggregationUsageModifier implements ColumnModifierInterface
{
    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var ReportResolver
     */
    private $reportResolver;

    /**
     * @var AggregationType
     */
    private $aggregationType;

    /**
     * @var EntitySimpleRelationResolver
     */
    private $simpleRelationResolver;

    public function __construct(
        Provider $provider,
        ReportResolver $reportResolver,
        AggregationType $aggregationType,
        EntitySimpleRelationResolver $simpleRelationResolver
    ) {
        $this->provider = $provider;
        $this->reportResolver = $reportResolver;
        $this->aggregationType = $aggregationType;
        $this->simpleRelationResolver = $simpleRelationResolver;
    }

    /**
     * @param array $columns
     * @return array
     * @throws LocalizedException
     * @SuppressWarnings("unused")
     */
    public function modify(array $columns): array
    {
        $scheme = $this->provider->getEntityScheme();
        $schemeRelations = $this->simpleRelationResolver->resolve();
        $aggregationTypes = $this->aggregationType->getSimpleAggregationsType();
        $report = $this->reportResolver->resolve();
        foreach ($columns as $key => $column) {
            $schemeColumn = $scheme->getColumnById($key);
            $expression = $columns[$key][ColumnAggregationTypeResolver::AGGREGATED_EXPRESSION];
            if ((!$report->getUsePeriod() && in_array($schemeColumn->getEntityName(), $schemeRelations))
                || $expression == $aggregationTypes[AggregationType::TYPE_NONE]
            ) {
                $columns[$key][ColumnAggregationTypeResolver::USE_AGGREGATION] = false;
            } else {
                $columns[$key][ColumnAggregationTypeResolver::USE_AGGREGATION] = true;
            }

        }

        return $columns;
    }
}
