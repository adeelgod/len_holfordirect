<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver;

class ColumnAggregationTypeResolver
{
    const AGGREGATED_EXPRESSION = 'aggregated_expression';
    const USE_AGGREGATION = 'use_aggregation';

    /**
     * @var array
     */
    private $pool;

    public function __construct(array $pool = [])
    {
        $this->pool = $pool;
    }

    public function build(array $columns): array
    {
        foreach ($this->pool as $modifier) {
            $columns = $modifier->modify($columns);
        }

        return $columns;
    }
}
