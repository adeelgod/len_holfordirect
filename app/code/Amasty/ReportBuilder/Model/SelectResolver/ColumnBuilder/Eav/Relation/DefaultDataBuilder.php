<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnBuilder\Eav\Relation;

use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolverInterface;
use Amasty\ReportBuilder\Model\SelectResolver\RelationBuilder;
use Magento\Framework\App\ResourceConnection;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnBuilder\Eav\Relation\BuilderInterface;

class DefaultDataBuilder implements BuilderInterface
{
    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var ResourceConnection
     */
    private $connectionResource;

    public function __construct(
        Provider $provider,
        ResourceConnection $connectionResource
    ) {
        $this->provider = $provider;
        $this->connectionResource = $connectionResource;
    }

    public function execute(array $columnData, string $linkedField, string $indexField, string $tableName): array
    {
        $tableAlias = sprintf('%s_table', $columnData[ColumnResolverInterface::ALIAS]);
        return [
            RelationBuilder::TYPE => \Zend_Db_Select::LEFT_JOIN,
            RelationBuilder::ALIAS => $tableAlias,
            RelationBuilder::TABLE => $tableName,
            RelationBuilder::CONDITION => sprintf(
                '%s.%s = %s.%s AND %s.attribute_id = \'%d\' AND %s.store_id = 0',
                $tableAlias,
                $indexField,
                $columnData[ColumnResolverInterface::ENTITY_NAME],
                $linkedField,
                $tableAlias,
                $columnData[ColumnResolverInterface::ATTRIBUTE_ID],
                $tableAlias
            )
        ];
    }
}
