<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnBuilder;

use Amasty\ReportBuilder\Model\ResourceModel\Report\Data\Select;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolverInterface;
use Amasty\ReportBuilder\Model\SelectResolver\Context;
use Amasty\ReportBuilder\Model\SelectResolver\RelationResolverInterface;

class SimpleEntity
{
    /**
     * @var \Amasty\ReportBuilder\Model\SelectResolver\ColumnResolverInterface
     */
    private $columnResolver;

    /**
     * @var \Amasty\ReportBuilder\Model\EntityScheme\Provider
     */
    private $schemeProvider;

    /**
     * @var \Amasty\ReportBuilder\Model\SelectResolver\EntitySimpleRelationResolver
     */
    private $simpleRelationResolver;

    /**
     * @var \Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnExpressionResolverInterface
     */
    private $columnExpressionResolver;

    /**
     * @var RelationResolverInterface
     */
    private $relationResolver;

    /**
     * @var RelationHelper
     */
    private $relationHelper;

    public function __construct(
        Context $context,
        RelationResolverInterface $relationResolver,
        RelationHelper $relationHelper
    ) {
        $this->columnResolver = $context->getColumnResolver();
        $this->schemeProvider = $context->getEntitySchemeProvider();
        $this->simpleRelationResolver = $context->getSimpleRelationResolver();
        $this->columnExpressionResolver = $context->getColumnExpressionResolver();
        $this->relationResolver = $relationResolver;
        $this->relationHelper = $relationHelper;
    }

    public function build(Select $select): void
    {
        $columns = $this->columnResolver->resolve();
        $scheme = $this->schemeProvider->getEntityScheme();
        $relations = $this->relationResolver->resolve();
        $simpleRelations = $this->simpleRelationResolver->resolve();

        foreach ($columns as $columnId => $columnData) {
            $column = $scheme->getColumnById($columnId);
            if ($column->getEavAttribute()
                || !isset($relations[$column->getEntityName()])
                || $this->relationHelper->isColumnInSelect($select, $column)
            ) {
                continue;
            }

            $expression = $this->columnExpressionResolver->resolve($columnId);
            if (in_array($column->getEntityName(), $simpleRelations)) {
                $select->columns([$columnData[ColumnResolverInterface::ALIAS] => $expression]);
            } elseif (isset($relations[$column->getEntityName()])) {
                $relation = $relations[$column->getEntityName()];
                $internalExpression = $this->columnExpressionResolver->resolve($columnId, true);
                if ($relation[RelationResolverInterface::TABLE] instanceof Select) {
                    $subSelect = $relation[RelationResolverInterface::TABLE];
                    $subSelect->columns([$columnData[ColumnResolverInterface::ALIAS] => $internalExpression]);
                    $this->relationHelper->throwRelations($select, $columnData, $relation);
                } else {
                    $parentRelation = $this->relationHelper->getParentSubSelectRelation($relation, $relations);
                    if ($parentRelation) {
                        $subSelect = $parentRelation[RelationResolverInterface::TABLE];
                        $subSelect->columns([$columnData[ColumnResolverInterface::ALIAS] => $internalExpression]);
                        $this->relationHelper->throwRelations($select, $columnData, $parentRelation);
                    } else {
                        $select->columns([$columnData[ColumnResolverInterface::ALIAS] => $internalExpression]);
                    }
                }
            }
        }
    }
}
