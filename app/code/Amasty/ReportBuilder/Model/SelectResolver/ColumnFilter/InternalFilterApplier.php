<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver\ColumnFilter;

use Amasty\ReportBuilder\Api\ColumnInterface;
use Amasty\ReportBuilder\Model\EntityScheme\Column\AggregationType;
use Amasty\ReportBuilder\Model\ReportResolver;
use Amasty\ReportBuilder\Model\ResourceModel\Report;
use Amasty\ReportBuilder\Model\ResourceModel\Report\Data\Select;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnAggregationTypeResolver;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnExpressionResolverInterface;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolverInterface;
use Amasty\ReportBuilder\Model\SelectResolver\Context;
use Amasty\ReportBuilder\Model\SelectResolver\EntitySimpleRelationResolver;
use Amasty\ReportBuilder\Model\SelectResolver\RelationResolver;
use Amasty\ReportBuilder\Model\SelectResolver\RelationResolverInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class InternalFilterApplier
{
    /**
     * @var FilterResolverInterface
     */
    private $filterResolver;

    /**
     * @var ColumnResolverInterface
     */
    private $columnResolver;

    /**
     * @var EntitySimpleRelationResolver
     */
    private $simpleRelationResolver;

    /**
     * @var ColumnExpressionResolverInterface
     */
    private $columnExpressionResolver;

    /**
     * @var ReportResolver
     */
    private $reportResolver;

    /**
     * @var \Amasty\ReportBuilder\Model\EntityScheme\Provider
     */
    private $schemeProvider;

    /**
     * @var RelationResolver
     */
    private $relationResolver;

    /**
     * @var Report
     */
    private $reportResource;

    public function __construct(
        Context $context,
        RelationResolver $relationResolver,
        Report $reportResource
    ) {
        $this->filterResolver = $context->getFilterResolver();
        $this->columnResolver = $context->getColumnResolver();
        $this->simpleRelationResolver = $context->getSimpleRelationResolver();
        $this->reportResolver = $context->getReportResolver();
        $this->columnExpressionResolver = $context->getColumnExpressionResolver();
        $this->schemeProvider = $context->getEntitySchemeProvider();
        $this->relationResolver = $relationResolver;
        $this->reportResource = $reportResource;
    }

    public function apply(string $filter, array $conditions): bool
    {
        $column = $this->resolveColumn($filter);
        if ($column === null) {
            return false;
        }

        if (!$this->getUseInternalFilterAggregation($column)) {
            return false;
        }

        if ($column->getEavAttribute()) {
            $relationName = sprintf('%s_attribute', $column->getAlias());
            $relation = $this->relationResolver->getRelationByName($relationName);
            if (!isset($relation[RelationResolverInterface::TABLE])
                || !$relation[RelationResolverInterface::TABLE] instanceof Select
            ) {
                return false;
            }
            $subSelect = $relation[RelationResolverInterface::TABLE];
            //@TODO add filter by store
            $whereConditionsString = $this->prepareWhereConditions(
                sprintf('%s_table.value', $column->getAlias()),
                $conditions
            );
            $subSelect->where($whereConditionsString);
        } else {
            $relation = $this->relationResolver->getRelationByName($column->getEntityName());
            if (!isset($relation[RelationResolverInterface::TABLE])
                || !$relation[RelationResolverInterface::TABLE] instanceof Select
            ) {
                return false;
            }
            $subSelect = $relation[RelationResolverInterface::TABLE];
            $whereConditionsString = $this->prepareWhereConditions(
                $column->getColumnId(),
                $conditions
            );
            $subSelect->where($whereConditionsString);
        }

        return true;
    }

    private function prepareWhereConditions(string $alias, array $conditions): string
    {
        $connection = $this->reportResource->getConnection();
        $whereConditions = [];
        foreach ($conditions as $key => $condition) {
            $whereConditions[] = $connection->prepareSqlCondition($alias, [$key => $condition]);
        }

        return implode(' AND ', $whereConditions);
    }

    private function getUseInternalFilterAggregation(ColumnInterface $column): bool
    {
        $report = $this->reportResolver->resolve();
        $simpleRelations = $this->simpleRelationResolver->resolve();
        $columns = $this->columnResolver->resolve();
        if (($report->getUsePeriod() || !in_array($column->getEntityName(), $simpleRelations))
            && !$columns[$column->getColumnId()][ColumnAggregationTypeResolver::USE_AGGREGATION]
        ) {
            return true;
        }

        return false;
    }

    private function resolveColumn(string $filter): ?ColumnInterface
    {
        foreach ($this->columnResolver->resolve() as $columnId => $columnData) {
            if ($filter == $columnId || $filter == $columnData[ColumnResolverInterface::ALIAS]) {
                return $this->schemeProvider->getEntityScheme()->getColumnById($columnId);
            }
        }

        return null;
    }
}
