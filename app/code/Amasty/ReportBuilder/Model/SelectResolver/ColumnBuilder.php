<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver;

use Amasty\ReportBuilder\Model\ResourceModel\Report\Data\Select;

class ColumnBuilder implements ColumnBuilderInterface
{
    /**
     * @var array
     */
    private $pool;

    public function __construct(
        array $pool = []
    ) {
        $this->pool = $pool;
    }

    public function build(Select $select): void
    {
        foreach ($this->pool as $builder) {
            $builder->build($select);
        }
    }
}
