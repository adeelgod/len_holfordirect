<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\SelectResolver;

use Amasty\ReportBuilder\Model\ReportResolver;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnAggregationTypeResolver;
use Amasty\ReportBuilder\Model\SelectResolver\ColumnResolver\ColumnStorageInterface;

class ColumnResolver implements ColumnResolverInterface
{
    /**
     * @var ColumnAggregationTypeResolver
     */
    private $columnAggregationTypeResolver;

    /**
     * @var array
     */
    private $pool;

    /**
     * @var ColumnStorageInterface
     */
    private $storage;

    /**
     * @var ReportResolver
     */
    private $reportResolver;

    public function __construct(
        ColumnAggregationTypeResolver $columnAggregationTypeResolver,
        ColumnStorageInterface $columnStorage,
        ReportResolver $reportResolver,
        array $pool = []
    ) {
        $this->pool = $pool;
        $this->columnAggregationTypeResolver = $columnAggregationTypeResolver;
        $this->storage = $columnStorage;
        $this->reportResolver = $reportResolver;
    }

    public function resolve(): array
    {
        $columns = $this->storage->getAllColumns();
        if (empty($columns)) {
            $columns = $this->reportResolver->resolve()->getAllColumns();
            foreach ($this->pool as $modifier) {
                $columns = $modifier->modify($columns);
            }
            $columns = $this->columnAggregationTypeResolver->build($columns);
            $this->storage->setColumns($columns);
        }

        return $columns;
    }
}
