<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Model\Indexer\Eav;

class Processor extends \Magento\Framework\Indexer\AbstractProcessor
{
    const INDEXER_ID = 'amasty_reportbuilder_eav_indexer';
}
