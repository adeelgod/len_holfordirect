define([
    'jquery',
    'uiComponent',
    'rjsResolver',
    'ko',
    'mage/translate',
    'amrepbuilder_helpers',
    'underscore',
    'mage/calendar',
    'mage/dropdown'
], function ($, Component, resolver, ko, $t, helpers, _) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Amasty_ReportBuilder/builder/column/toolbar/wrapper',
            buttonTmpl: 'Amasty_ReportBuilder/components/button',
            filters: {
                dateTmpl: 'Amasty_ReportBuilder/builder/column/filters/date_range',
                textTmpl: 'Amasty_ReportBuilder/builder/column/filters/text',
                textRangeTmpl: 'Amasty_ReportBuilder/builder/column/filters/text_range',
                selectTmpl: 'Amasty_ReportBuilder/builder/column/filters/select',
                multiselectTmpl: 'Amasty_ReportBuilder/builder/column/filters/multiselect'
            },
            calendarIcon: 'Amasty_ReportBuilder/components/icons/calendar',
            visibilityIcon: 'Amasty_ReportBuilder/components/icons/visibility',
            filterIcon: 'Amasty_ReportBuilder/components/icons/filter',
            sortIcon: 'Amasty_ReportBuilder/components/icons/sort',
            removeIcon: 'Amasty_ReportBuilder/components/icons/remove',
            modules: {
                chosenOptions: 'index = chosen_options',
                entitiesList: 'index = entities_list',
                form: 'index = amreportbuilder_report_form',
                popup: 'index = amasty_report_builder_popup'
            },
            selectors: {
                datepickerFrom: '[data-amrepbuilder-js="datepicker-from"]',
                datepickerTo: '[data-amrepbuilder-js="datepicker-to"]'
            }
        },

        /**
         * Invokes initialize method of parent class,
         * contains initialization logic
         */
        initialize: function () {
            var self = this;

            this._super();

            this.elems([
                'Amasty_ReportBuilder/builder/column/toolbar/buttons/date',
                'Amasty_ReportBuilder/builder/column/toolbar/buttons/sort',
                'Amasty_ReportBuilder/builder/column/toolbar/buttons/filter',
                'Amasty_ReportBuilder/builder/column/toolbar/buttons/visibility',
                'Amasty_ReportBuilder/builder/column/toolbar/buttons/remove'
            ]);

            resolver(function () {
                self.chosenOptions = self.chosenOptions();
                self.entitiesList = self.entitiesList();
                self.form = self.form();
                self.popup = self.popup();
            });
        },

        /**
         * Column filtration object initialization
         *
         * @param {object} column
         */
        initFiltration: function (column) {
            if (!column.filtration) {
                column.filtration = {
                    isActive: ko.observable(false)
                };
            } else {
                column.filtration.isActive = ko.observable(column.filtration.isActive)
            }

            switch (column.frontend_model) {
                case 'multiselect':
                    this.initMultiselectFilter(column);
                    break;
                case 'text':
                    this.initTextFilter(column);
                    break;
                case 'select':
                    this.initSelectFilter(column);
                    break;
                default:
                    this._initRange(column);
            }
        },

        /**
         * DateRange picker initialization
         *
         * @param {object} column
         */
        initSelectFilter: function (column) {
            var option;

            column.filtration.value = ko.observable(+column.filtration.value || '');
            column.filtration.title = ko.observable('Select Option');

            if (column.filtration.value()) {
                option = ko.utils.arrayFirst(column.options, function (option) {
                    return +option.value === column.filtration.value();
                });

                column.filtration.title = ko.observable(option.label);
            }
        },

        /**
         * Select Dropdown initialization
         *
         * @param {object} node datepicker container
         */
        initDropdown: function (node) {
            $(node).dropdown();
        },

        /**
         * Text Filter Initialization
         *
         * @param {object} column
         */
        initTextFilter: function (column) {
            column.filtration.value = ko.observable(column.filtration.value || '');
        },

        /**
         * Multiselect initialization
         *
         * @param {object} column
         */
        initMultiselectFilter: function (column) {
            // TODO: need to check multiselect
            column.filtration.value = ko.observableArray(column.filtration.value || []);

            column.filtration.isEmpty = ko.computed(function () {
                var result = true;

                column.filtration.value.each(function (element) {
                    if (element.isChecked()) {
                        result = false;
                    }
                })

                return result;
            }, column);
        },

        /**
         * DateRange picker initialization
         *
         * @param {object} node
         * @param {object} column
         */
        initDatePicker: function (node, column) {
            column.isFilter.subscribe(function (value) {
                if (value) {
                    $(node).dateRange({
                        onClose: function (value, event) {
                            if ($(event.input).attr('name') === 'from') {
                                column.filtration.value.from(value);
                            } else {
                                column.filtration.value.to(value);
                            }
                        },
                        dateFormat: 'mm/dd/yy',
                        from: {
                            id: 'datepicker-from-' + column.name
                        },
                        to: {
                            id: 'datepicker-to-' + column.name
                        }
                    });
                }
            });
        },

        /**
         * Remove target item from chosen list
         *
         * @param {object} item
         */
        removeItem: function (item) {
            var self = this,
                itemEntity = self.entitiesList.elems()[item.entity_index],
                columnsList,
                relationsList;

            if (!itemEntity && itemEntity.chosenColumnsList().length >= 1) {
                self.chosenOptions.removeColumn(item);

                return false;
            }

            relationsList = _.uniq(self.entitiesList.getRelationEntitiesList(itemEntity));
            columnsList = self.entitiesList.getEntitiesChosenColumns(relationsList);

            if (columnsList.length) {
                self.popup.open({
                    header: 'Are you sure?',
                    content: $t('The related columns: <b>(' + helpers.concatArray(columnsList) + ')</b> will be deleted as well. ' +
                        'You still can set ‘invisible’ to the column to hide it from the report grid (in this case' +
                        ' related columns will remain in the report.'),
                    confirmCallback: function () {
                        self.form.removeRelations(itemEntity.stackIndex());
                        self.entitiesList.clearEntityList(relationsList);
                        self.chosenOptions.removeColumn(item);
                    },
                    type: 'prompt'
                });

                return false;
            }

            if (relationsList.length) {
                self.entitiesList.clearEntityList(relationsList);
            }

            self.chosenOptions.removeColumn(item);
        },

        /**
         * Toggling sort status
         *
         * @param {object} item
         */
        toggleSort: function (item) {
            var nextSortStatus = item.sortStatus() + 1 > 2 ? 0 : item.sortStatus() + 1;

            if (!item.sortStatus()) {
                this.chosenOptions.resetSorting();
            }

            if (!nextSortStatus) {
                this.entitiesList.currentPrimaryColumn().sortStatus(2);
            }

            item.sortStatus(nextSortStatus);
        },

        /**
         * Toggling sort status
         *
         * @param {object} item
         */
        toggleDate: function (item) {
            var self = this,
                nextValue = !item.isDate();

            if (nextValue) {
                self.chosenOptions.elems.each(function (column) {
                    column.isDate(false);
                });

                item.sortStatus(false);
                item.filtration.isActive(false);
            }

            item.isDate(nextValue);
        },

        /**
         * Clearing Multiselect values in target item
         *
         * @param {object} item
         */
        clearMultiselect: function (item) {
            item.isFilter(false);
            item.filtration.isActive(false);

            _.each(item.filtration.value(), function (filter) {
                filter.isChecked(false);
            });
        },

        /**
         * Clearing Select values in target item
         *
         * @param {object} item
         */
        clearSelect: function (item) {
            item.filtration.isActive(false);
            item.isFilter(false);
            item.filtration.value(false);
            item.filtration.title('Select Option');
        },

        /**
         * Clearing filtration values in target item
         *
         * @param {object} item
         */
        clearRange: function (item) {
            item.filtration.isActive(false);
            item.isFilter(false);

            _.each(item.filtration.value, function (filter) {
                filter('');
            });
        },

        /**
         * Clearing text filtration values in target item
         *
         * @param {object} item
         */
        clearText: function (item) {
            item.filtration.isActive(false);
            item.filtration.value('');
            item.isFilter(false);
        },

        /**
         * Applying filtration values in target item
         *
         * @param {object} item
         */
        applyFiltration: function (item) {
            item.isFilter(false);
            item.filtration.isActive(true);
        },

        /**
         * Range picker observers initialization
         *
         * @param {object} column
         */
        _initRange: function (column) {
            if (!column.filtration.value) {
                column.filtration.value = {
                    from: ko.observable(''),
                    to: ko.observable('')
                }
            } else {
                column.filtration.value.from = ko.observable(column.filtration.value.from);
                column.filtration.value.to = ko.observable(column.filtration.value.to);
            }

            column.filtration.isEmpty = ko.computed(function () {
                return !(column.filtration.value.from() || column.filtration.value.to());
            }, column);
        }
    });
});
