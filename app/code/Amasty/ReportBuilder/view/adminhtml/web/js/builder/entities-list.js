define([
    'jquery',
    'uiComponent',
    'ko',
    'underscore',
    'rjsResolver',
    'jquery/ui'
], function ($, Component, ko, _, resolver) {
    'use strict';

    return Component.extend({
        defaults: {
            currentPrimaryColumn: ko.observable(),
            isLoading: ko.observable(true),
            maxColumnQty: 15,
            columnHeaderTmpl: 'Amasty_ReportBuilder/builder/available_options/columns/header',
            columnFooterTmpl: 'Amasty_ReportBuilder/builder/available_options/columns/footer',
            template: 'Amasty_ReportBuilder/builder/available_options/entities_list/wrapper',
            entityTmpl: 'Amasty_ReportBuilder/builder/available_options/entities_list/entity',
            columnTmpl: 'Amasty_ReportBuilder/builder/available_options/columns/column',
            dependTmpl: 'Amasty_ReportBuilder/builder/available_options/entities_list/dependencies',
            columnsListTmpl: 'Amasty_ReportBuilder/builder/available_options/columns/columns_list',
            buttonTmpl: 'Amasty_ReportBuilder/components/button',
            mainEntity: {
                entity_index: 0,
                entity_name: null,
                relationsQty: 1,
                primaryColumn: ko.observable(),
                periodColumn: ko.observable()
            },
            modules: {
                form: 'index = amreportbuilder_report_form',
                chosenList: 'index = chosen_options',
                builder: 'index = amasty_report_builder',
                storeIds: 'index = store_ids'
            }
        },
        selectors: {
            chosenList: '[data-amrepbuilder-js="chosen-list"]'
        },

        /**
         * Invokes initialize method of parent class,
         * contains initialization logic
         */
        initialize: function () {
            var self = this;

            self._super();

            self.elems = ko.observableArray([]).extend({deferred: true});

            resolver(function () {
                self.form = self.form();
                self.chosenList = self.chosenList();
                self.storeIds = self.storeIds();
                self.builder = self.builder();

                if (self.source.data.entities) {
                    self.elems(self.source.data.entities);
                }
            });
        },

        /**
         * Initialize particular entity
         *
         * @param {object} item target entity
         * @param {number} index target entity index
         */
        initEntity: function (item, index) {
            item.index = index;
            item.stackIndex = ko.observable(false);
            item.isActive = ko.observable(false);
            item.isViewAll = ko.observable(false);
            item.chosenColumnsList = ko.observableArray([]);
            item.columns = ko.observable(item.columns);

            if (index === 0) {
                this._initMainEntity(item);
            }
        },

        /**
         * Initialize particular column
         *
         * @param {object} column target column
         * @param {object} entity column entity
         * @param {number} index column
         */
        initColumn: function (column, entity, index) {
            column.index = index;
            column.entity_index = entity.index;

            if (!column.isDisabled) {
                column.isDisabled = ko.observable(false);
            }

            if (column.entity_index === 0 && column.use_for_period !== undefined) {
                this._initPeriodColumn(column);
            }

            if (column.entity_index === 0 && column.primary) {
                this.mainEntity.primaryColumn(column);

                if (!this.source.data.chosen_data || !this.source.data.chosen_data.length) {
                    this.chosenList.initColumn(column);
                    column.sortStatus(2);
                    this.setPrimaryColumn(column);
                }
            }

            if (column.entity_index === this.elems().length - 1 && column.index === Object.keys(entity.columns()).length - 1) {
                this.isLoading(false);
            }
        },

        /**
         * Setting Primary column§
         *
         * @param {object} column target column
         */
        setPrimaryColumn: function (column) {
            var self = this;

            self.chosenList.initColumn(column);

            if (self.currentPrimaryColumn()) {
                self.currentPrimaryColumn().isDisabled(false);
            }

            if (column.position !== undefined) {
                self.chosenList.removeColumn(column);
            }

            self.currentPrimaryColumn(column);

            column.isDisabled(true);
        },

        /**
         * Initialize Drag and Drop functionality for target column
         *
         * @param {object} item prototype column element
         * @param {object} nodeElement
         */
        initDnD: function (item, nodeElement) {
            var self = this;

            $(nodeElement).draggable({
                helper: 'clone',
                connectToSortable: self.selectors.chosenList
            }).data({
                item: item
            });
        },

        /**
         * Update available entities list
         *
         * @param {string} prototype which was added
         */
        update: function (prototype) {
            var self = this,
                stackIndex,
                entityName = prototype.entity_name,
                prototypeEntity = self.elems()[prototype.entity_index];

            prototypeEntity.chosenColumnsList.push(prototype.index);

            if (self.mainEntity.entity_name === entityName || prototypeEntity.chosenColumnsList().length > 1) {
                return;
            }

            self.getData([entityName], function (data) {
                data = self.prepareData(data);
                self.elems.push.apply(self.elems, data);
            });

            stackIndex = self.form.pushRelationsData({
                entity_index: prototype.entity_index,
                entity_name: entityName
            });

            prototypeEntity.stackIndex(stackIndex);
        },

        /**
         *  Remove an entry from the target entity chosen column list by target column index
         *
         *  @param {number} entityIndex target entity index
         *  @param {number} columnIndex
         */
        removeChosenColumn: function (entityIndex, columnIndex) {
            this.elems()[entityIndex].chosenColumnsList.remove(function (item) {
                return item === columnIndex;
            });
        },

        /**
         * Get entities list by relations and run callback function with data
         *
         * @param {array} entityNames added entity names
         * @param {object} callback target function for preparing data
         *
         */
        getData: function (entityNames, callback) {
            var self = this;

            $.ajax({
                url: self.source.relations_url,
                method: 'POST',
                data: {
                    entityNames: entityNames
                },
                showLoader: true
            }).done(function (data) {
                callback(data);
            });
        },

        /**
         * Get columns list which related from current entity
         *
         * @param {object} itemEntity
         * @returns {array} concat array
         */
        getRelationEntitiesList: function (itemEntity) {
            var self = this,
                result = [];

            _.each(itemEntity.relations, function (entity) {
                var relationEntity = self.getEntityByName(entity.name);

                if (!relationEntity || relationEntity.index <= self.mainEntity.relationsQty || itemEntity.index >= relationEntity.index) {
                    return false;
                }

                result = result.concat(self.getRelationEntitiesList(relationEntity), relationEntity.index);
            });

            return result;
        },

        /**
         * Get entity by name
         *
         * @param {string} entityName
         */
        getEntityByName: function (entityName) {
            return ko.utils.arrayFirst(this.elems(), function (entity) {
                return entity.name === entityName;
            });
        },

        /**
         * Get main entity with relations by entity name
         *
         * @param {string} entityName entity name
         * @param {function} callback target function for preparing data
         *
         */
        getEntity: function (entityName, callback) {
            var self = this;

            $.ajax({
                url: self.source.main_entity_url,
                method: 'POST',
                data: {
                    entityName: entityName
                },
                showLoader: true
            }).done(function (data) {
                callback(data);
            });
        },

        /**
         *  Get chosen columns from entity list
         *
         *  @param {array} entityList
         *  @returns {array} list of the chosen columns
         */
        getEntitiesChosenColumns: function (entityList) {
            var self = this,
                result = [];

            _.each(entityList, function (index) {
                result = result.concat(self.getEntityChosenColumns(index));
            })

            return result;
        },

        /**
         *
         *  Get chosen columns from entity
         *
         *  @param {number} entityIndex
         *  @returns {array} list of the chosen columns
         */
        getEntityChosenColumns: function (entityIndex) {
            var self = this,
                entity = self.elems()[entityIndex],
                column = {},
                result = [];

            _.each(entity.chosenColumnsList(), function (columnIndex) {
                column = self.getColumnByIndex(entityIndex, columnIndex);

                result.push(column.title);
            })

            return result;
        },

        /**
         *  Get Column by target index from target entity
         *
         *  @param {number} entityIndex
         *  @param {number} columnIndex
         *  @returns {object} particular column
         */
        getColumnByIndex: function (entityIndex, columnIndex) {
            return this.elems()[entityIndex].columns()[Object.keys(this.elems()[entityIndex].columns())[columnIndex]];
        },

        /**
         *  Pushing new elems beginning with target index
         *
         *  @param {array} elems data
         *  @param {number} startIndex
         */
        pushElems: function (elems, startIndex) {
            this.elems.splice(startIndex, this.elems().length);
            this.elems.push.apply(this.elems, elems);
        },

        /**
         *  Clearing all chosen options from target entity
         *
         *  @param {number} entityIndex entity
         */
        clearEntity: function (entityIndex) {
            var self = this,
                column,
                entity = this.elems()[entityIndex],
                entityChosenList = _.clone(entity.chosenColumnsList());

            entity.stackIndex(null);

            _.each(entityChosenList, function (columnIndex) {
                column = self.getColumnByIndex(entityIndex, columnIndex);

                if (column) {
                    self.chosenList.removeColumn(column);
                }
            });
        },

        /**
         *  Clearing all entities by list
         *
         *  @param {number} entityList
         */
        clearEntityList: function (entityList) {
            var self = this;

            _.each(entityList, function (index) {
                self.clearEntity(index);
            });

            _.each(entityList, function (index) {
                self.elems.splice(index);
            });
        },

        /**
         *  Clearing Columns properties
         *
         *  @param {number} entityIndex
         *  @param {string} columnName
         */
        clearColumn: function (entityIndex, columnName) {
            var column = this.elems()[entityIndex].columns()[columnName];

            column.isDisabled(false);

            if (column.position !== undefined) {
                column.isDate(false);
                column.isFilter(false);
                column.filtration.isActive(false);
                column.sortStatus(0);
                column.isVisible(true);
                delete column.position;
            }
        },

        /**
         * Comparing with existing elements and removing duplicate ones
         *
         * @param {array} data of the new elements
         * @returns {array} cleared array of elements
         */
        prepareData: function (data) {
            var self = this,
                index = 0;

            $(data).each(function (i, item) {
                $(self.elems()).each(function (elemIndex, elem) {
                    if (item.name === elem.name) {
                        data.splice(index, 1);
                        index--;
                    }
                });

                index++;
            });

            return data;
        },

        /**
         * Main entity initialization
         *
         * @param {object} entity target entity
         */
        _initMainEntity: function (entity) {
            this.storeIds.visible(entity.columns().store_id !== undefined);

            entity.isActive(true);

            this.mainEntity.entity_name = entity.name;
            this.mainEntity.periodColumn(false);

            this.mainEntity.relationsQty = Object.keys(entity.relations).length;
        },

        /**
         * Use period column initialization
         *
         * @param {object} column target column
         */
        _initPeriodColumn: function (column) {
            this.builder.isUsePeriod.visible(true);

            if (column.use_for_period) {
                this.mainEntity.periodColumn(column);
            }
        }
    });
});
