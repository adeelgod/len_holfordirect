define([
    'jquery',
    'amcharts'
], function ($, amcharts) {
    'use strict';

    $.widget('mage.linearCharts', {
        options: {
            data: {},
            selectorInit: 'chart-view',
            interval: 'day',
            shadow: false
        },

        _create: function () {
            var self = this;

            self.chart = am4core.create(self.options.selectorInit, amcharts.XYChart);
            am4core.useTheme(am4themes_animated);

            self.chart.data = self.options.data;
            self.chart.cursor = new am4charts.XYCursor();
            self.chart.scrollbarX = new am4core.Scrollbar();

            self._initSeries();
            self._initAxis('x');
            self._initAxis('y');

            self._initExport();
            self._renderChart();
        },

        /**
         * Axis initialization
         */
        _initAxis: function (axis) {
            switch (this.options[axis + 'AxisType']) {
                case 'int':
                case 'smallint':
                case 'decimal':
                    this._initValue(axis);
                    break;
                case 'text':
                case 'varchar':
                    this._initCategory(axis);
                    break;
                default:
                    this._initDate(axis);
            }
        },

        /**
         * Chart rendering
         */
        _renderChart: function () {
            var self = this,
                labelX,
                labelY;

            self.valueAxisX.events.on("sizechanged", function (ev) {
                var axis = ev.target;

                axis.renderer.labels.template.maxWidth = axis.pixelWidth / (axis.endIndex - axis.startIndex);
            });

            self.chart.xAxes.push(self.valueAxisX);
            self.chart.yAxes.push(self.valueAxisY);
            self.chart.series.push(self.series)

            labelX = self.valueAxisX.renderer.labels.template;
            labelX.maxWidth = 200;
            labelX.truncate = true;
            labelX.tooltipText = "{valueX}";

            labelY = self.valueAxisY.renderer.labels.template;
            labelY.maxWidth = 200;
            labelY.truncate = true;
            labelY.wrap = true;
            labelY.tooltipText = "{valueY}";

            self.chart.zoomOutButton.marginRight = 40;
        },

        /**
         * Axis Type Value initialization
         *
         * @param {string} axis target axis name
         */
        _initValue: function (axis) {
            var self = this,
                axisUpperCase = axis.toUpperCase()

            self['valueAxis' + axisUpperCase] = new am4charts.ValueAxis();
            self['valueAxis' + axisUpperCase].dataFields['value' + axisUpperCase] = 'value' + axisUpperCase;
            self.series.dataFields['value' + axisUpperCase] = 'value' + axisUpperCase;
        },

        /**
         * Axis Type Category initialization
         *
         * @param {string} axis target axis name
         */
        _initCategory: function (axis) {
            var self = this,
                axisUpperCase = axis.toUpperCase()

            self.category = 'value' + axisUpperCase;

            self['valueAxis' + axisUpperCase] = new am4charts.CategoryAxis();
            self['valueAxis' + axisUpperCase].dataFields.category = 'value' + axisUpperCase;
            self.series.dataFields['category' + axisUpperCase] = 'value' + axisUpperCase;
        },

        /**
         * Axis Type Date initialization
         *
         * @param {string} axis target axis name
         */
        _initDate: function (axis) {
            var self = this,
                axisUpperCase = axis.toUpperCase();

            self['valueAxis' + axisUpperCase] = new am4charts.DateAxis();
            self.series.dataFields['date' + axisUpperCase] = 'value' + axisUpperCase;
            self['valueAxis' + axisUpperCase].baseInterval = {
                "timeUnit": self.options.interval,
                "count": 1
            };

            self.chart.cursor[axis + 'Axis'] = self['valueAxis' + axisUpperCase];
        },

        /**
         * Axis Series initialization
         */
        _initSeries: function () {
            var self = this;

            self.series = new am4charts.LineSeries();

            self.chart.cursor.snapToSeries = self.series;
        },

        /**
         * Chart data exporting initialization
         */
        _initExport: function () {
            var self = this;

            self.chart.exporting.menu = new am4core.ExportMenu();
            self.chart.exporting.menu.items = [{
                label: "...",
                menu: [{
                    label: "Image",
                    menu: [
                        {type: "png", label: "PNG"},
                        {type: "jpg", label: "JPG"},
                        {type: "svg", label: "SVG"},
                        {type: "pdf", label: "PDF"}
                    ]
                }, {
                    label: "Print", type: "print"
                }]
            }];
        }
    });

    return $.mage.linearCharts;
});
