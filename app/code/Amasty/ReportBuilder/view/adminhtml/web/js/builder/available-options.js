define([
    'jquery',
    'uiComponent'
], function ($, Component) {
    'use strict';

    return Component.extend({
        defaults: {
            headerTmpl: 'Amasty_ReportBuilder/builder/header',
            columnToolbarTmpl: 'Amasty_ReportBuilder/builder/column/toolbar',
            template: 'Amasty_ReportBuilder/builder/available_options/wrapper',
            title: 'Available options',
            descr: 'To activate the widget, drag the column you need to the edit field on the right'
        },

        /**
         * Invokes initialize method of parent class,
         * contains initialization logic
         */
        initialize: function () {
            this._super();
        }
    });
});
