/**
 * Reports Builder helpers
 */

define([
    'jquery',
    'ko'
], function ($, ko) {
    'use strict';

    /**
     * Update ko subscribe method with silent possibilities
     */
    ko.observable.fn.silentUpdate = function (value) {
        this.notifySubscribers = function () {};
        this(value);

        this.notifySubscribers = function () {
            ko.subscribable.fn.notifySubscribers.apply(this, arguments);
        };
    };

    return {

        /**
         * Concat array with all nesting to string
         *
         * @node {object} datepicker container
         * @returns {string} result
         */
        concatArray: function (array) {
            return Array.prototype.concat.apply([], array).join(', ')
        }
    }
});
