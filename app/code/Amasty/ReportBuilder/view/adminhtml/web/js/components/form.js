/**
 * Magento form component extend for Report builder
 */
define([
    'Magento_Ui/js/form/form',
    'ko',
    'underscore',
    'rjsResolver'
], function (uiForm, ko, _, resolver) {
    'use strict';

    return uiForm.extend({
        defaults: {
            modules: {
                chosenOptions: 'index = chosen_options',
                entitiesList: 'index = entities_list',
                popup: 'index = amasty_report_builder_popup'
            }
        },

        /**
         * Invokes initialize method of parent class,
         * contains initialization logic
         */
        initialize: function () {
            var self = this;

            this._super();

            resolver(function () {
                self.chosenOptions = self.chosenOptions();
                self.entitiesList = self.entitiesList();
                self.popup = self.popup();

                self.initRelations();

                if (!self.source.data.scheme_relations().length) {
                    self.pushRelationsData(self.entitiesList.mainEntity);
                }
            });
        },

        /**
         * @inheritdocs
         */
        save: function (redirect, data) {
            if (this.validateData()) {
                this.validate();

                if (!this.additionalInvalid && !this.source.get('params.invalid')) {
                    this.updateData();
                    this.clearFormData();
                    this.setAdditionalData(data).submit(redirect);
                } else {
                    this.focusInvalid();
                }
            }
        },

        /**
         *  Init Relations data
         */
        initRelations: function () {
            var data = this.source.data;

            data.scheme_relations = ko.observableArray(data.scheme_relations || []);
        },

        /**
         *  Push new relations data into relations array
         *
         *  @param {object} data
         */
        pushRelationsData: function (data) {
            var relations = this.source.data.scheme_relations;

            relations.push(data);

            return relations().length - 1;
        },

        /**
         *  Push new relations data into relations array
         */
        validateData: function () {
            var hasVisible = false;

            _.each(this.chosenOptions.elems(), function (column, index) {
                if (index === 0) {
                    return false;
                }

                if (column.isVisible()) {
                    hasVisible = true;
                }
            });

            if (this.chosenOptions.elems().length < 2 || !hasVisible) {
                this.popup.open({
                    header: 'Oops!',
                    description: 'You can\'t save and display a report with only default column. Please choose and' +
                        ' add at least one additional visible column to the Chosen options block.',
                    type: 'alert'
                });

                return false;
            }

            return true;
        },

        /**
         *  Remove all relations after target index from the stack
         *
         *  @param {number} index
         */
        removeRelations: function (index) {
            var self = this,
                relations = self.source.data.scheme_relations;

            relations.splice(index);
        },

        /**
         *  Get all relations entity names
         *
         *  @returns {array} array with entities names
         */
        getRelationsNames: function () {
            var self = this,
                result = [],
                relations = self.source.data.scheme_relations;

            relations.each(function (item) {
                result.push(item.entity_name);
            });

            return result;
        },

        /**
         *  Update current data json value from chosen options list elems
         */
        updateData: function () {
            var data = this.source.data;

            data.chosen_data = ko.toJSON(this.chosenOptions.elems());
            data.scheme_relations = ko.toJSON(data.scheme_relations());
        },

        /**
         *  Preparing form data before sending to server
         */
        clearFormData: function () {
            delete this.source.data.entities;
        }
    });
});
