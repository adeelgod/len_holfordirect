define([
    'jquery',
    'uiComponent',
    'ko',
    'rjsResolver'
], function ($, Component, ko, resolver) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Amasty_ReportBuilder/builder/chosen_options/wrapper',
            columnTmpl: 'Amasty_ReportBuilder/builder/chosen_options/columns/column',
            columnsListTmpl: 'Amasty_ReportBuilder/builder/chosen_options/columns/columns_list',
            columnToolbarTmpl: 'Amasty_ReportBuilder/builder/column/toolbar',
            buttonTmpl: 'Amasty_ReportBuilder/components/button',
            headerTmpl: 'Amasty_ReportBuilder/builder/header',
            columnHeaderTmpl: 'Amasty_ReportBuilder/builder/chosen_options/columns/header',
            columnFooterTmpl: 'Amasty_ReportBuilder/builder/chosen_options/columns/footer',
            title: 'Chosen options',
            descr: 'Add the columns here that you would like to configure to be displayed on the report page',
            elems: ko.observableArray([]).extend({deferred: true}),
            entitiesList: {},
            modules: {
                entitiesList: 'index = entities_list',
                form: 'index = amreportbuilder_report_form',
                toolbar: 'index = component_toolbar',
                builder: 'index = amasty_report_builder',
                popup: 'index = amasty_report_builder_popup'
            }
        },
        selectors: {
            column: '[data-amrepbuilder-js="column"]:not(.-disabled)'
        },
        classes: {
            hover: '-hovered',
            disabled: '-disabled'
        },

        /**
         * Invokes initialize method of parent class,
         * contains initialization logic
         */
        initialize: function () {
            var self = this;

            self._super();

            resolver(function () {
                self.entitiesList = self.entitiesList();
                self.toolbar = self.toolbar();
                self.builder = self.builder();
                self.popup = self.popup();
                self.form = self.form();

                self._initPrimaryColumn();

                if (self.source.data.chosen_data) {
                    self.entitiesList.isLoading.subscribe(function (value) {
                        if (!value) {
                            self._initChosenData();
                        }
                    })
                }
            });
        },

        /**
         * Initialize DropZone
         *
         * @param {object} element
         */
        initDropZone: function (element) {
            var self = this;

            self.dropZone = $(element);

            self.dropZone.droppable({
                over: function () {
                    self.dropZone.addClass(self.classes.hover);
                },
                out: function () {
                    self.dropZone.removeClass(self.classes.hover);
                },
                drop: function (event, ui) {
                    self.currentDropItem = ui.draggable;
                    self.dropZone.removeClass(self.classes.hover);
                }
            });
        },

        /**
         * Initialize Drag and Drop functionality for target column
         *
         * @param {object} item prototype column node element
         */
        initDnD: function (item) {
            var self = this;

            $(item).sortable({
                items: self.selectors.column,
                receive: function (event, ui) {
                    var prototype = $(ui.item).data('item');

                    self._addColumn(prototype);
                },
                start: function (event, ui) {
                    self.isSorting = true;
                    self.currentSortingItemIndex = ui.item.index();
                },
                stop: function (event, ui) {
                    if (self.isSorting) {
                        self._sortColumn(ui);
                    }
                }
            });
        },

        /**
         * Initialize particular column
         *
         * @param {object} column target column
         */
        initColumn: function (column) {
            if (column.isFilter) {
                return false;
            }

            column.isDate = ko.observable(false);
            column.isFilter = ko.observable(false);
            column.isVisible = ko.observable(true);
            column.sortStatus = ko.observable(0);

            this.toolbar.initFiltration(column);
        },

        /**
         * Reset sorting in all chosen columns
         */
        resetSorting: function () {
            this.elems.each(function (column) {
                column.sortStatus(0);
            });
        },

        /**
         * Remove target column from chosen list
         *
         * @param {object} item target item
         */
        removeColumn: function (item) {
            if (item.sortStatus()) {
                this.entitiesList.currentPrimaryColumn().sortStatus(2);
            }

            this.builder.removeAxis(item.id);
            this.entitiesList.removeChosenColumn(item.entity_index, item.index);
            this.elems.remove(function (column) {
                return column.position === item.position;
            });
            this.entitiesList.clearColumn(item.entity_index, item.name);
        },

        /**
         * Clearing all elems from chosen list and enabling prototypes elements
         */
        clearAll: function () {
            var self = this;

            self.popup.open({
                header: 'Are you sure?',
                description: 'Do you really want to delete all columns?',
                confirmCallback: function () {
                    self.elems.splice(0);
                    self.entitiesList.elems.splice(0);
                    self.form.removeRelations(0);
                    self.builder.clearAxes();

                    self.entitiesList.getEntity(self.entitiesList.mainEntity.entity_name, function (data) {
                        self.entitiesList.pushElems(data, 0);
                    });
                },
                type: 'prompt'
            });
        },

        /**
         *  Sorting columns Method
         *
         *  @param {object} ui DnD helper class
         */
        _sortColumn: function (ui) {
            var self = this,
                targetIndex = ui.item.index(),
                prototype;

            if (targetIndex !== this.currentSortingItemIndex) {
                prototype = self.elems.splice(self.currentSortingItemIndex, 1)[0];

                self.elems.splice(targetIndex, 0, prototype);
            }
        },

        /**
         * Chosen Data initialization
         */
        _initChosenData: function () {
            var self = this,
                entity,
                prototype;

            _.each(self.source.data.chosen_data, function (column) {
                column.isDate = ko.observable(column.isDate);
                column.isFilter = ko.observable(column.isFilter);
                column.isVisible = ko.observable(column.isVisible);
                column.sortStatus = ko.observable(column.sortStatus);
                column.entity_index = column.entity_index ? column.entity_index : 0;
                self.toolbar.initFiltration(column);

                entity = self.entitiesList.elems()[column.entity_index];
                prototype = entity.columns()[column.name];

                if (prototype) {
                    entity.chosenColumnsList.push(prototype.index);

                    prototype.position = column.position;
                    prototype.isDate = ko.observable(column.isDate());
                    prototype.isFilter = ko.observable(column.isFilter());
                    prototype.isVisible = ko.observable(column.isVisible());
                    prototype.sortStatus = ko.observable(column.sortStatus());

                    self.toolbar.initFiltration(prototype);

                    column.isDisabled = prototype.isDisabled;
                    column.isDisabled(true);
                }
            });

            self.entitiesList.currentPrimaryColumn.silentUpdate(self.source.data.chosen_data[0]);
            self.elems(self.source.data.chosen_data);
        },

        /**
         *  Adding target column from available list to chosen list via DnD into chosen position
         *
         *  @param {object} prototype target prototype
         */
        _addColumn: function (prototype) {
            var self = this,
                currentDropItemIndex = self.currentDropItem.index();

            self.initColumn(prototype);

            self.isSorting = false;
            prototype.isDisabled(true);

            if (self.elems().length === currentDropItemIndex) {
                self.elems.push(prototype);
            } else {
                self.elems.splice(currentDropItemIndex, 0, prototype);
            }

            self.currentDropItem.remove();
            self.entitiesList.update(prototype);
            self.builder.addAxis(prototype);
        },

        /**
         * Initialize primary column
         */
        _initPrimaryColumn: function () {
            var self = this;

            self.entitiesList.currentPrimaryColumn.subscribe(function (column) {
                if (self.elems()[0]) {
                    self.removeColumn(self.elems()[0]);
                }

                self.elems.unshift(column);
            });
        },

        /**
         * Checking column filtration status
         *
         * @returns {boolean}
         */
        _checkColumnFiltrationStatus: function (column) {
            if (column.position === undefined) {
                return false
            }

            return column.isDate() || !column.isVisible() || column.isFilter() || column.sortStatus();
        }
    });
});
