define([
    'jquery',
    'uiRegistry'
], function ($, registry) {

    $.widget('mage.amreportbuilderToolbar', {
        options: {
            contentSelector: '[data-role="amrepbuilder-content"]',
            componentName: 'index=amreportbuilder_view_listing_data_source',
            reloadUrl: '',
            reportId: 0
        },

        form: null,

        reload: function () {
            var formData = $(this.element).serializeArray(),
                requestData = {},
                contentBlock = $(this.options.contentSelector);

            for (var i = 0; i < formData.length; i++) {
                var input = formData[i];

                requestData[input.name] = input.value;
            }

            contentBlock.css({opacity: .3});

            $.ajax({
                url: this.options.reloadUrl,
                method: 'POST',
                data: {report: requestData, report_id: this.options.reportId},
                success: function (response) {
                    if (!response) {
                        return;
                    }
                    if ('AmCharts' in window) {
                        AmCharts.clear();
                    }
                    contentBlock.html($(response).html());
                    contentBlock.css({opacity: 1});
                    contentBlock.trigger('contentUpdated');
                }
            });

            var dataSource = registry.get(this.options.componentName);

            if (dataSource) {
                dataSource.params.report = requestData;
                dataSource.reload();
            }
        },

        _create: function () {
            $(this.element).change($.proxy(this.reload, this));
        }
    });

    return $.mage.amreportbuilderToolbar;
});
