define([
    'jquery',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'rjsResolver',
    'ko',
    'Magento_Ui/js/modal/modal'
], function ($, uiRegistry, select, resolver, ko) {
    'use strict';

    return select.extend({
        defaults: {
            isNotShowNoticeChangeEntity: ko.observable(false),
            currentValue: ko.observable(false),
            modules: {
                form: 'index = amreportbuilder_report_form',
                storeIds: 'index = store_ids',
                entitiesList: 'index = entities_list',
                chosenList: 'index = chosen_options',
                builder: 'index = amasty_report_builder',
                popup: 'index = amasty_report_builder_popup',
                displayChart: 'index = display_chart',
                axisXSelect: 'index = chart_axis_x',
                axisYSelect: 'index = chart_axis_y',
                isUsePeriod: 'index = is_use_period'
            }
        },

        initialize: function () {
            var self = this;

            self._super();

            resolver(function () {
                self.form = self.form();
                self.entitiesList = self.entitiesList();
                self.chosenList = self.chosenList();
                self.builder = self.builder();
                self.popup = self.popup();
                self.displayChart = self.displayChart();
                self.axisXSelect = self.axisXSelect();
                self.axisYSelect = self.axisYSelect();
                self.isUsePeriod = self.isUsePeriod();
                self.storeIds = self.storeIds();
                self.currentValue(self.value());

                self._displayChartInit();
                self.resetVisibility(self.value());
            });
        },

        /**
         * Entity switcher listener
         *
         * @param {string} value
         */
        onUpdate: function (value) {
            var self = this;

            if (value === self.currentValue()) {
                return false;
            }

            if (self.chosenList.elems().length > 1
                && self.entitiesList.elems().length
                && !self.isNotShowNoticeChangeEntity()) {
                self.popup.open({
                    header: 'Are you sure to perform the action?',
                    description: 'The configured data will be lost.',
                    checkbox: {
                        title: 'Do not display this message again.',
                        observer: self.isNotShowNoticeChangeEntity
                    },
                    cancelCallback: function () {
                        self.value(self.currentValue());
                    },
                    confirmCallback: function () {
                        self.changeEntity(value);
                    },
                    type: 'prompt'
                });
            } else {
                self.changeEntity(value);
            }

            return this._super();
        },

        /**
         * Change current entity
         *
         * @param {string} value entity name
         */
        changeEntity: function (value) {
            var self = this;

            self.currentValue(value);
            this.storeIds.value(['0']);
            self.resetVisibility(value);

            self.chosenList.elems.splice(0);

            self.builder.clearAxes();
            self.builder.isUsePeriod.visible(false);
            self.builder.isUsePeriod.checked(false);

            delete self.source.data.chosen_data;

            self.entitiesList.getEntity(value, function (data) {
                self.entitiesList.pushElems(data, 0);
                self.form.removeRelations(0);
                self.form.pushRelationsData({
                    entity_index: 0,
                    entity_name: value
                });
            });
        },

        /**
         * Clearing visibility method
         *
         * @param {string} value
         */
        resetVisibility: function (value) {
            value = !!value;

            this.displayChart.visible(value);
            this.isUsePeriod.visible(value);
            this.storeIds.visible(value);
        },

        /**
         * Display chart initialization
         */
        _displayChartInit: function () {
            var self = this;

            self.axisXSelect.visible(self.displayChart.value());
            self.axisYSelect.visible(self.displayChart.value());

            self.displayChart.value.subscribe(function (value) {
                self.axisXSelect.visible(value);
                self.axisYSelect.visible(value);
            });
        }
    });
});
