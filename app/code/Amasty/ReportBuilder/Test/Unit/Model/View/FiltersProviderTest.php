<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Test\Unit\Model\View;

use Amasty\ReportBuilder\Model\View\FiltersProvider;
use Amasty\ReportBuilder\Test\Unit\Traits;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * @see FiltersProvider
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * phpcs:ignoreFile
 */
class FiltersProviderTest extends \PHPUnit\Framework\TestCase
{
    use Traits\ObjectManagerTrait;
    use Traits\ReflectionTrait;

    /**
     * @var FiltersProvider
     */
    private $model;

    /**
     * @covers FiltersProvider::getDateFilter
     * @dataProvider getDateFilterDataProvider
     */
    public function testGetDateFilter(array $params, $result): void
    {
        $request = $this->createMock(RequestInterface::class);
        $timezone = $this->createMock(TimezoneInterface::class);

        $request->expects($this->any())->method('getParam')->willReturn($params);
        $timezone->expects($this->any())->method('date')->willReturn(10);

        $this->model = $this->getObjectManager()->getObject(
            FiltersProvider::class,
            [
                'request' => $request,
                'timezone' => $timezone,
            ]
        );

        $this->assertEquals($result, $this->model->getDateFilter());
    }

    /**
     * Data provider for getDateFilter test
     * @return array
     */
    public function getDateFilterDataProvider(): array
    {
        return [
            [['from' => 20, 'to' => 30], ['from' => 20, 'to' => 30]],
            [[], ['from' => 10, 'to' => 10]]
        ];
    }
}
