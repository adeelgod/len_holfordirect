<?php

declare(strict_types=1);

namespace Amasty\ReportBuilder\Test\Unit\Model\Backend\Report\DataCollector;

use Amasty\ReportBuilder\Api\Data\ReportInterface;
use Amasty\ReportBuilder\Api\EntityInterface;
use Amasty\ReportBuilder\Api\EntityScheme\SchemeInterface;
use Amasty\ReportBuilder\Model\Backend\Report\DataCollector\Scheme;
use Amasty\ReportBuilder\Model\EntityScheme\Provider;
use Amasty\ReportBuilder\Test\Unit\Traits;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * @see Scheme
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * phpcs:ignoreFile
 */
class SchemeTest extends \PHPUnit\Framework\TestCase
{
    use Traits\ObjectManagerTrait;
    use Traits\ReflectionTrait;

    /**
     * @var Scheme
     */
    private $model;

    /**
     * @covers Scheme::collect
     */
    public function testCollect(): void
    {
        $serializer = $this->createMock(Json::class);
        $schemeProvider = $this->createMock(Provider::class);
        $report = $this->createMock(ReportInterface::class);
        $scheme = $this->createMock(SchemeInterface::class);
        $entity = $this->createMock(EntityInterface::class);

        $serializer->expects($this->any())->method('unserialize')->willReturn([
            [Scheme::SCHEME_DATA_NAME => 'relationEntity']
        ]);
        $schemeProvider->expects($this->any())->method('getEntityScheme')->willReturn($scheme);
        $scheme->expects($this->any())->method('getEntityByName')->willReturn($entity);
        $entity->expects($this->any())->method('getRelatedEntities')->willReturn(['entityName' => 'relationEntity']);

        $this->model = $this->getObjectManager()->getObject(
            Scheme::class,
            [
                'serializer' => $serializer,
                'schemeProvider' => $schemeProvider,
            ]
        );

        $this->assertEquals([], $this->model->collect($report, []));
        $this->assertEquals(
            [
                ReportInterface::SCHEME => [[
                    ReportInterface::SCHEME_SOURCE_ENTITY => 'relationEntity',
                    ReportInterface::SCHEME_ENTITY => 'relationEntity'
                ]]
            ],
            $this->model->collect($report, [Scheme::SCHEME_DATA_KEY => 'test'])
        );
    }
}
