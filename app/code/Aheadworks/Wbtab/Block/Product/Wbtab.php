<?php
namespace Aheadworks\Wbtab\Block\Product;

use \Aheadworks\Wbtab\Helper\Config;
use \Aheadworks\Wbtab\Model\Source\ProductPageBlock\Position;
use \Aheadworks\Wbtab\Block\WbtabAbstract;

/**
 * Class Wbtab
 * @package Aheadworks\Wbtab\Block\Product
 */
class Wbtab extends \Aheadworks\Wbtab\Block\WbtabAbstract
{
    /**
     * @var string
     */
    protected $isEnabledXmlPath = Config::XML_PRODUCT_PAGE_ENABLE;

    /**
     * @var string
     */
    protected $blockNameXmlPath = Config::XML_PRODUCT_PAGE_BLOCK_NAME;

    /**
     * @var string
     */
    protected $positionXmlPath = Config::XML_PRODUCT_PAGE_BLOCK_POSITION;

    /**
     * @var string
     */
    protected $displayAddToCartXmlPath = Config::XML_PRODUCT_PAGE_DISPLAY_ADD_TO_CART;

    /**
     * @return int|null
     */
    public function getProductId()
    {
        /* @var $product \Magento\Catalog\Model\Product */
        $product = $this->coreRegistry->registry('product');
        return $product ? $product->getId() : null;
    }

    /**
     * @return string|null
     */
    public function getBlockType()
    {
        return WbtabAbstract::PRODUCT_PAGE_BLOCK;
    }

    /**
     * @return boolean
     */
    public function shouldReplaceNativeBlock()
    {
        return ($this->isBlockEnabled() && $this->getNameInLayout() == Position::INSTEAD_NATIVE_VALUE);
    }
}