<?php
namespace Aheadworks\Wbtab\Block\Wbtab;

use \Aheadworks\Wbtab\Block\WbtabAbstract as WbtabContainer;
/**
 * Class ProductList
 * @package Aheadworks\Wbtab\Block\Wbtab
 */
class ProductList extends \Magento\Catalog\Block\Product\ProductList\Related
{
    /**
     * Path to template.
     *
     * @var string
     */
    protected $_template = 'wbtab/product_list.phtml';

    /**
     * Product collection factory
     *
     * @var \Aheadworks\Wbtab\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Checkout\Model\ResourceModel\Cart $checkoutCart
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Aheadworks\Wbtab\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Checkout\Model\ResourceModel\Cart $checkoutCart,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Module\Manager $moduleManager,
        \Aheadworks\Wbtab\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = []
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        parent::__construct(
            $context, $checkoutCart, $catalogProductVisibility, $checkoutSession, $moduleManager, $data
        );
    }

    /**
     * @return $this
     */
    protected function _prepareData()
    {
        if (!$productId = $this->_getProductId()) {
            return $this;
        }
        /** @var \Aheadworks\Wbtab\Model\ResourceModel\Product\Collection $wbtabCollection */
        $wbtabCollection = $this->productCollectionFactory->create();

        $quoteProductIds = $this->_getQuoteProductIds();
        $wbtabCollection
            ->addAttributeToSelect('required_options')
            ->addWbtabFilter($productId, $quoteProductIds);

        if ($this->moduleManager->isEnabled('Magento_Checkout')) {
            $this->_addProductAttributesAndPrices($wbtabCollection);
        }
        $wbtabCollection->load();

        foreach ($wbtabCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }
        $this->_itemCollection = $wbtabCollection;
        return $this;
    }

    /**
     * @return int|null
     */
    protected function _getProductId()
    {
        $productId = null;
        if ($this->getBlockType() == WbtabContainer::CATALOG_PAGE_BLOCK) {
            if ($quote = $this->_checkoutSession->getQuote()) {
                $items = $quote->getAllVisibleItems();
                if ($quoteItem = array_shift($items)) {
                    $productId = $quoteItem->getProductId();
                }
            }
        } else {
            $productId = $this->getData('product_id');
        }
        return $productId;
    }

    /**
     * @return int
     */
    protected function _getQuoteProductIds()
    {
        $quoteProductIds = [];
        if ($quote = $this->_checkoutSession->getQuote()) {
            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $quoteProductIds[] = $quoteItem->getProductId();
            }
        }
        return $quoteProductIds;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|null
     */
    public function getItems()
    {
        if ($this->_itemCollection && $this->_itemCollection->getSize()) {
            return $this->_itemCollection;
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getBlockTitle()
    {
        return $this->getData('block_title');
    }

    /**
     * @return bool
     */
    public function isDisplayAddToCart()
    {
        return $this->getData('display_add_to_cart');
    }

    /**
     * @return bool
     */
    public function getAddToCartBackUrl()
    {
        return $this->getData('add_to_cart_back_url');
    }
}