<?php
namespace Aheadworks\Wbtab\Block;

/**
 * Class WbtabAbstract
 * @package Aheadworks\Wbtab\Block
 */
class WbtabAbstract extends \Magento\Backend\Block\Template
{
    const PRODUCT_PAGE_BLOCK = 'product_page';
    const CATALOG_PAGE_BLOCK = 'catalog_page';

    /**
     * Path to templates file in theme.
     *
     * @var string
     */
    protected $_template = 'wbtab.phtml';

    /**
     * @var \Magento\Framework\Url\Helper\Data
     */
    protected $urlHelper;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var string
     */
    protected $isEnabledXmlPath;

    /**
     * @var string
     */
    protected $blockNameXmlPath;

    /**
     * @var string
     */
    protected $positionXmlPath;

    /**
     * @var string
     */
    protected $displayAddToCartXmlPath;

    /**
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->urlHelper = $urlHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return int|null
     */
    public function getProductId()
    {
        return null;
    }

    /**
     * @return string|null
     */
    public function getBlockType()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getAddToCartBackUrl()
    {
        return $this->urlHelper->getEncodedUrl();
    }

    /**
     * @return $this
     */
    public function shouldReplaceNativeBlock()
    {
        return false;
    }

    /**
     * @return mixed
     */
    public function getBlockTitle() {
        return $this->_getConfigValue($this->blockNameXmlPath);
    }

    /**
     * @return bool
     */
    public function isDisplayAddToCart() {
        return $this->_getConfigValue($this->displayAddToCartXmlPath);
    }

    /**
     * @return bool
     */
    public function isBlockEnabled() {
        return $this->_getConfigValue($this->isEnabledXmlPath) &&
            $this->getNameInLayout() == $this->_getConfigValue($this->positionXmlPath);
    }

    /**
     * @param $configXmlPath
     * @return mixed
     */
    protected function _getConfigValue($configXmlPath)
    {
        return $this->_scopeConfig->getValue(
            $configXmlPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}