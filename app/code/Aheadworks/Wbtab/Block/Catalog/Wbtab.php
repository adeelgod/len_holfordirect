<?php
namespace Aheadworks\Wbtab\Block\Catalog;

use \Aheadworks\Wbtab\Helper\Config;
use \Aheadworks\Wbtab\Block\WbtabAbstract;

/**
 * Class Wbtab
 * @package Aheadworks\Wbtab\Block\Catalog
 */
class Wbtab extends \Aheadworks\Wbtab\Block\WbtabAbstract
{
    /**
     * @var string
     */
    protected $isEnabledXmlPath = Config::XML_CATALOG_PAGE_ENABLE;

    /**
     * @var string
     */
    protected $blockNameXmlPath = Config::XML_CATALOG_PAGE_BLOCK_NAME;

    /**
     * @var string
     */
    protected $positionXmlPath = Config::XML_CATALOG_PAGE_BLOCK_POSITION;

    /**
     * @var string
     */
    protected $displayAddToCartXmlPath = Config::XML_CATALOG_PAGE_DISPLAY_ADD_TO_CART;

    /**
     * @return string|null
     */
    public function getBlockType()
    {
        return WbtabAbstract::CATALOG_PAGE_BLOCK;
    }
}