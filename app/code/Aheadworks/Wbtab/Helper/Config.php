<?php
namespace Aheadworks\Wbtab\Helper;

use Magento\Store\Model\ScopeInterface;

/**
 * Config helper
 */
class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PRODUCT_PAGE_ENABLE = 'aw_wbtab/product_page/enable';
    const XML_PRODUCT_PAGE_BLOCK_NAME = 'aw_wbtab/product_page/block_name';
    const XML_PRODUCT_PAGE_BLOCK_POSITION = 'aw_wbtab/product_page/block_position';
    const XML_PRODUCT_PAGE_DISPLAY_ADD_TO_CART = 'aw_wbtab/product_page/display_add_to_cart';
    const XML_CATALOG_PAGE_ENABLE = 'aw_wbtab/catalog_page/enable';
    const XML_CATALOG_PAGE_BLOCK_NAME = 'aw_wbtab/catalog_page/block_name';
    const XML_CATALOG_PAGE_BLOCK_POSITION = 'aw_wbtab/catalog_page/block_position';
    const XML_CATALOG_PAGE_DISPLAY_ADD_TO_CART = 'aw_wbtab/catalog_page/display_add_to_cart';

    /**
     * @param string   $path
     * @param int|null $storeId
     * @param int|null $websiteId
     * @return mixed
     */
    public function getValue($path, $storeId = null, $websiteId = null)
    {
        if ($storeId !== null) {
            return $this->scopeConfig
                ->getValue($path, ScopeInterface::SCOPE_STORE, $storeId);
        } elseif ($websiteId !== null) {
            return $this->scopeConfig
                ->getValue($path, ScopeInterface::SCOPE_WEBSITE, $websiteId);
        } else {
            return $this->scopeConfig->getValue($path);
        }
    }
}
