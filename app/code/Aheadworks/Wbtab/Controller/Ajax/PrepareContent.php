<?php
namespace Aheadworks\Wbtab\Controller\Ajax;

/**
 * Class PrepareContent
 * @package Aheadworks\Wbtab\Controller\Ajax
 */
class PrepareContent extends \Magento\Framework\App\Action\Action
{
    /**
     * @return $this
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
        $data = $this->getRequest()->getParams();

        $blockInstance = $resultPage
            ->getLayout()
            ->createBlock('\Aheadworks\Wbtab\Block\Wbtab\ProductList', 'wbtab_product_list');

        $result = [];
        if ($blockInstance) {
            $blockInstance->setData($data);
            $result['success'] = true;
            $result['wbtab_block_content'] = $blockInstance->toHtml();
        }

        return $resultJson->setData($result);
    }
}
