<?php
namespace Aheadworks\Wbtab\Model\ResourceModel\Product;

use \Magento\Sales\Model\Order;

class Collection extends \Magento\Catalog\Model\ResourceModel\Product\Collection
{
    const WBTAB_PRODUCTS_LIMIT = 10;

    /**
     * @param int $productId
     * @param array $inCartProductIds
     * @return $this
     */
    public function addWbtabFilter($productId, $inCartProductIds)
    {
        $select = $this->getConnection()->select();
        $select->from(['soi' => $this->getTable('sales_order_item')], '')
            ->columns(['rating' => 'count(*)'])
            ->join(['so' => $this->getTable('sales_order')], 'so.entity_id = soi.order_id', '')
            ->join(
                ['related_soi' => $this->getTable('sales_order_item')],
                'related_soi.order_id = soi.order_id AND soi.product_id != related_soi.product_id',
                ['wbtab_product_id' => 'related_soi.product_id']
            )
            ->where('so.total_item_count > 1')
            ->where('so.state = ?', Order::STATE_COMPLETE)
            ->where('soi.parent_item_id IS null')
            ->where('related_soi.parent_item_id IS null')
            ->where('soi.product_id = ?', $productId)
            ->group(['soi.product_id', 'related_soi.product_id'])
        ;
        if ($inCartProductIds) {
            $excludeProductsInCartCondition = $this->getConnection()
                ->prepareSqlCondition('related_soi.product_id', ['nin' => $inCartProductIds]);
            $select->where($excludeProductsInCartCondition);
        }

        $this->getSelect()
            ->join(
                ['wbtab' => $select],
                'wbtab.wbtab_product_id = e.entity_id',
                ['rating' => 'wbtab.rating']
            )
            ->order('wbtab.rating DESC')
            ->limit(self::WBTAB_PRODUCTS_LIMIT);

        return $this;
    }
}