<?php
namespace Aheadworks\Wbtab\Model\Source\ProductPageBlock;

/**
 * Class Position
 * @package Aheadworks\Wbtab\Model\Source
 */
class Position implements \Magento\Framework\Option\ArrayInterface
{
    const INSTEAD_NATIVE_VALUE = 'wbtab_instead_native';
    const BEFORE_NATIVE_VALUE = 'wbtab_before_native';
    const AFTER_NATIVE_VALUE = 'wbtab_after_native';
    const CONTENT_TOP_VALUE = 'wbtab_content_top';
    const CONTENT_BOTTOM_VALUE = 'wbtab_content_bottom';

    const INSTEAD_NATIVE_LABEL = 'Instead of native Related Products block';
    const BEFORE_NATIVE_LABEL = 'Before native Related Products block';
    const AFTER_NATIVE_LABEL = 'After native Related Products block';
    const CONTENT_TOP_LABEL = 'Content top';
    const CONTENT_BOTTOM_LABEL = 'Content bottom';

    /**
     * @var null|array
     */
    protected $optionArray = null;

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            self::INSTEAD_NATIVE_VALUE => __(self::INSTEAD_NATIVE_LABEL),
            self::BEFORE_NATIVE_VALUE => __(self::BEFORE_NATIVE_LABEL),
            self::AFTER_NATIVE_VALUE => __(self::AFTER_NATIVE_LABEL),
            self::CONTENT_TOP_VALUE => __(self::CONTENT_TOP_LABEL),
            self::CONTENT_BOTTOM_VALUE => __(self::CONTENT_BOTTOM_LABEL)
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->optionArray === null) {
            $this->optionArray = [];
            foreach ($this->getOptions() as $value => $label) {
                $this->optionArray[] = ['value' => $value, 'label' => $label];
            }
        }
        return $this->optionArray;
    }

    /**
     * @param int $value
     * @return null|\Magento\Framework\Phrase
     */
    public function getOptionLabelByValue($value)
    {
        $options = $this->getOptions();
        if (array_key_exists($value, $options)) {
            return $options[$value];
        }
        return null;
    }
}
