<?php
namespace Aheadworks\Wbtab\Model\Source\CatalogPageBlock;

/**
 * Class Position
 * @package Aheadworks\Wbtab\Model\Source
 */
class Position implements \Magento\Framework\Option\ArrayInterface
{
    const CONTENT_TOP_VALUE = 'wbtab_content_top';
    const CONTENT_BOTTOM_VALUE = 'wbtab_content_bottom';

    const CONTENT_TOP_LABEL = 'Content top';
    const CONTENT_BOTTOM_LABEL = 'Content bottom';

    /**
     * @var null|array
     */
    protected $optionArray = null;

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            self::CONTENT_TOP_VALUE => __(self::CONTENT_TOP_LABEL),
            self::CONTENT_BOTTOM_VALUE => __(self::CONTENT_BOTTOM_LABEL)
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->optionArray === null) {
            $this->optionArray = [];
            foreach ($this->getOptions() as $value => $label) {
                $this->optionArray[] = ['value' => $value, 'label' => $label];
            }
        }
        return $this->optionArray;
    }

    /**
     * @param int $value
     * @return null|\Magento\Framework\Phrase
     */
    public function getOptionLabelByValue($value)
    {
        $options = $this->getOptions();
        if (array_key_exists($value, $options)) {
            return $options[$value];
        }
        return null;
    }
}
