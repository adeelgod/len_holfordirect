<?php
namespace Aheadworks\Wbtab\Test\Integration\Model\ResourceModel\Product;

/**
 * Class CollectionTest
 * @package Aheadworks\Wbtab\Test\Integration\Model\ResourceModel\Product
 */
class CollectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Aheadworks\Wbtab\Model\ResourceModel\Product\Collection
     */
    protected $productCollection;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    protected function setUp()
    {

        $this->objectManager = \Magento\TestFramework\Helper\Bootstrap::getObjectManager();
        $this->productCollection = $this->objectManager->create('Aheadworks\Wbtab\Model\ResourceModel\Product\Collection');
    }

    /**
     * @magentoDataFixture ../../../../app/code/Aheadworks/Wbtab/Test/Integration/_files/order_with_two_products.php
     */
    public function testWbtabFilter()
    {
        $testProductId = 10;
        $wbtabProductId = 11;
        $wbtabProductIds = $this->productCollection->addWbtabFilter($testProductId, null)->getAllIds();
        \PHPUnit_Framework_Assert::assertCount(1, $wbtabProductIds);
        \PHPUnit_Framework_Assert::assertContains($wbtabProductId, $wbtabProductIds);
    }
}