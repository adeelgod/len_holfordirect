<?php
namespace Objectsource\Reports\Controller\Adminhtml\Report;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

abstract class AbstractCsvExport extends \Magento\Backend\App\Action
{

    public function __construct(
        Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Model\CountryFactory $countryFactory
    )
    {
        parent::__construct($context);
        $this->filesystem = $filesystem;
        $this->directory = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->fileFactory = $fileFactory;
        $this->localeDate = $localeDate;
        $this->countryFactory = $countryFactory;
    }


    public function getFilters()
    {
        //get the filters from the filter get param and decode them
        $filter = $this->getRequest()->getParam('filter');
        parse_str(urldecode(base64_decode($filter)), $filters);
        return $filters;
    }

    public function convertToMysqlDate($date) {

        $curFormat = $this->localeDate->getDateFormatWithLongYear();
        if($curFormat == 'M/d/Y') {
            $dateFormat = 'm/d/Y';
        } else {
            $dateFormat = 'd/m/Y';
        }

        $date = \DateTime::createFromFormat($dateFormat, $date);
        return $date->format('Y-m-d');
    }


    public function getCsvFile($data, $exportType)
    {
        $name = md5(microtime());
        $file = 'export/' . $exportType . $name . '.csv';

        $this->directory->create('export');
        $stream = $this->directory->openFile($file, 'w+');
        $stream->lock();
        $totalCount = (int) count($data);
        if ($totalCount > 0) {
            foreach ($data as $row) {
                $stream->writeCsv($row);
            }

        }
        $stream->unlock();
        $stream->close();

        return [
            'type' => 'filename',
            'value' => $file,
            'rm' => true  // can delete file after use
        ];
    }

    /**
     * Add filter rows and empty row at top of report
     *
     * @param $filters
     * @return array
     */
    public function getFilterRows($filters) {
        $orderFrom = empty($filters['order_from']) ? '' : $filters['order_from'];
        $orderTo = empty($filters['order_to']) ? '' : $filters['order_to'];
        $rows = [
            ['Order from', 'Order to'],
            [$orderFrom, $orderTo],
            []
        ];

        return $rows;
    }
}
