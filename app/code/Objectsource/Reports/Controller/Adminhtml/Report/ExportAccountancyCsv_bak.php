<?php

namespace Objectsource\Reports\Controller\Adminhtml\Report;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportAccountancyCsv extends AbstractCsvExport
{
    public function execute()
    {
        $filters = $this->getFilters();


        $fileName = 'accountancy_report_'.date('Ymd').'.csv';
        $orderFrom = empty($filters['order_from']) ? null : $this->convertToMysqlDate($filters['order_from']) . " 00:00:00";
        $orderTo = empty($filters['order_to']) ? null : $this->convertToMysqlDate($filters['order_to']) . " 23:59:00";
        $filterRows = $this->getFilterRows($filters);

        $orders = $this->orderCollectionFactory->create();
        if ($orderFrom) {
            $orders->addAttributeToFilter('created_at', ['gteq' => $orderFrom]);
        }
        if ($orderTo) {
            $orders->addAttributeToFilter('created_at', ['lteq' => $orderTo]);
        }

        $data = [
            [
                'Increment ID',
                'Status',
                'Website',
                'Shipping Country',
                'Billing Country',
                'Customer Name',
                'Item Sku',
                'Item Type',
                'Item Name',
                'Item Qty',
                'Item VAT',
                'Sub Total',
                'Discount Total',
                'Row Total',
                'Date'
            ]
        ];
        /** @var \Magento\Sales\Model\Order $order */
        foreach ($orders as $order) {
            foreach ($order->getItemsCollection() as $item) {
                $data []= [
                    $order->getIncrementId(),
                    $order->getStore()->getWebsite()->getName(),
                    $order->getStatusLabel()->getText(),
                    $this->formatAddress($order->getShippingAddress()),
                    $this->formatAddress($order->getBillingAddress()),
                    $this->getName($order),
                    $item->getSku(),
                    $this->getItemType($item),
                    $item->getName(),
                    $item->getQtyOrdered(),
                    $item->getBaseTaxAmount(),
                    $item->getRowTotal(),
                    $item->getDiscountAmount(),
                    $item->getRowTotal() - $item->getDiscountAmount(),
                    $this->localeDate->date($order->getCreatedAt())->format('d/m/Y'),
                ];
            }

            if (!$order->getIsVirtual()) {
                $data []= [
                    $order->getIncrementId(),
                    $order->getStore()->getWebsite()->getName(),
                    $order->getStatusLabel()->getText(),
                    $this->formatAddress($order->getShippingAddress()),
                    $this->formatAddress($order->getBillingAddress()),
                    $this->getName($order),
                    '',
                    'Shipping',
                    '',
                    '',
                    $order->getShippingTaxAmount(),
                    $order->getShippingAmount(),
                    $order->getShippingDiscountAmount(),
                    $order->getShippingAmount() - $order->getShippingDiscountAmount(),
                    $this->localeDate->date($order->getCreatedAt())->format('d/m/Y'),

                ];
            }
        }
        $exportType = 'order_items_ordered';
        $data = array_merge($filterRows, $data);
        return $this->fileFactory->create($fileName, $this->getCsvFile($data, $exportType), DirectoryList::VAR_DIR);
    }

    public function getItemType($item)
    {
        $product = $item->getProduct();

        if ($product) {
            return $product->getAttributeText('item_type_for_report');
        }

        return "Product not found";
    }

    public function formatAddress($address)
    {
        if ($address) {
            $country =  $this->countryFactory->create()->loadByCode(
                $address->getCountryId()
            )->getName();

            return $country;
        }
        return 'N/a';

    }

    public function getName($order)
    {
        if ($shippingAddress = $order->getShippingAddress()) {
            return $shippingAddress->getName();
        } else if ($billingAddress = $order->getBillingAddress()) {
            return $billingAddress->getName();
        }

        return $order->getCustomerName();
    }

}

