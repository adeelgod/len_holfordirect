<?php
namespace Objectsource\Reports\Controller\Adminhtml\Report;

class Accountancy extends \Magento\Reports\Controller\Adminhtml\Report\AbstractReport
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_addBreadcrumb(__('Reports'), __('Reports'));

        $this->_setActiveMenu(
            'Objectsource_Reports::report_orders'
        )->_addBreadcrumb(
            __('Accountancy Report'),
            __('Accountancy Report')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Accountancy Products Ordered Report'));

        $filterFormBlock = $this->_view->getLayout()->getBlock('grid.filter.form');

        $this->_initReportAction([$filterFormBlock]);

        $this->_view->renderLayout();
    }
}
