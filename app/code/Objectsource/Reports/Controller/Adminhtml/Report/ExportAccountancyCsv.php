<?php

namespace Objectsource\Reports\Controller\Adminhtml\Report;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportAccountancyCsv extends AbstractCsvExport
{
    public function execute()
    {
        $filters = $this->getFilters();


        $fileName = 'accountancy_report_'.date('Ymd').'.csv';
        $orderFrom = empty($filters['order_from']) ? null : $this->convertToMysqlDate($filters['order_from']) . " 00:00:00";
        $orderTo = empty($filters['order_to']) ? null : $this->convertToMysqlDate($filters['order_to']) . " 23:59:00";
        $filterRows = $this->getFilterRows($filters);

        $orders = $this->orderCollectionFactory->create();
        if ($orderFrom) {
            $orders->addAttributeToFilter('created_at', ['gteq' => $orderFrom]);
        }
        if ($orderTo) {
            $orders->addAttributeToFilter('created_at', ['lteq' => $orderTo]);
        }

        $data = [
            [
                'Increment ID',
                'Status',
                'Website',
                'Shipping Country',
                'Billing Country',
                'Customer Name',
                'Item Sku',
                'Item Type',
                'Item Name',
                'Item Qty',
                'Item VAT',
                'Sub Total',
                'Discount Total',
                'Row Total',
                'Date'
            ]
        ];
        /** @var \Magento\Sales\Model\Order $order */
        foreach ($orders as $order) {
            foreach ($order->getItemsCollection() as $item) {

                // calculate vat for all items before 30/09/2017
                $order_date = $this->localeDate->date($order->getCreatedAt())->format('d/m/Y');
                $cut_of_date = "31/09/2017";
                // UK vat amount
                // $vat = 0.2;
                $vat = $item->getTaxPercent();

                $item_type = $this->getItemType($item);

                // set defaults
                $baseTaxAmount = $item->getBaseTaxAmount();
                $itemDiscountAmount = $item->getDiscountAmount();
                $orderTotal = $item->getRowTotalInclTax() - $itemDiscountAmount;
                $orderSubTotal = $item->getRowTotalInclTax();

                // VAT applies, recalculate including discount logic
                if ( $vat > 0 ) {
                   $baseTaxAmount = ( $this->calculateVAT( $orderSubTotal, $vat ) ) - ( $this->calculateVAT( $itemDiscountAmount, $vat ) ); 
                }
                
                $shippingAddress = $this->formatAddress($order->getShippingAddress());
                $billingAddress = $this->formatAddress($order->getBillingAddress());

                if ( strtotime($order_date) < strtotime($cut_of_date) ) {

                    if ( $this->isItemTaxable($item_type) and $this->isCountryTaxable($billingAddress) ) {

                        $itemDiscountAmount = $item->getDiscountAmount();
                        $orderTotal = $item->getRowTotal() - $itemDiscountAmount;
                        $orderSubTotal = $item->getRowTotal();
                        
                        $baseTaxAmount = ( $this->calculateVAT( $orderSubTotal, $vat ) ) - ( $this->calculateVAT( $itemDiscountAmount, $vat ) );

                    }
                
                }

                $baseTaxAmount = number_format((float)$baseTaxAmount, 2, '.', '');
                $orderTotal = number_format((float)$orderTotal, 2, '.', '');
                $orderSubTotal = number_format((float)$orderSubTotal, 2, '.', '');

                $data []= [
                    $order->getIncrementId(),
                    $order->getStore()->getWebsite()->getName(),
                    $order->getStatusLabel()->getText(),
                    $shippingAddress,
                    $billingAddress,
                    $this->getName($order),
                    $item->getSku(),
                    $item_type,
                    $item->getName(),
                    $item->getQtyOrdered(),
                    $baseTaxAmount,
                    $orderSubTotal,
                    $itemDiscountAmount,
                    $orderTotal,
                    $order_date,
                ];
            }

            if (!$order->getIsVirtual()) {

                $shippingTaxAmount = $order->getShippingTaxAmount();
                $shippingAmount = $order->getShippingAmount() + $shippingTaxAmount;
                $shippingDiscountAmount = $order->getShippingDiscountAmount();
                $shippingTotal = $shippingAmount - $order->getShippingDiscountAmount();

                if ( strtotime($order_date) < strtotime($cut_of_date) ) {

                    if ( $this->isCountryTaxable($billingAddress) ) {

                        $shippingTaxAmount = ( $this->calculateVAT( $shippingAmount, $vat ) ) - ( $this->calculateVAT( $shippingDiscountAmount, $vat ) );
                        $shippingTaxAmount = number_format((float)$shippingTaxAmount, 2, '.', '');
                    }
                
                }

                $data []= [
                    $order->getIncrementId(),
                    $order->getStore()->getWebsite()->getName(),
                    $order->getStatusLabel()->getText(),
                    $shippingAddress,
                    $billingAddress,
                    $this->getName($order),
                    '',
                    'Shipping',
                    '',
                    '',
                    $shippingTaxAmount,
                    $shippingAmount,
                    $shippingDiscountAmount,
                    $shippingTotal,
                    $order_date,
                ];

            }
        }
        $exportType = 'order_items_ordered';
        $data = array_merge($filterRows, $data);
        return $this->fileFactory->create($fileName, $this->getCsvFile($data, $exportType), DirectoryList::VAR_DIR);
    }

    public function getItemType($item)
    {
        $product = $item->getProduct();

        if ($product) {
            return $product->getAttributeText('item_type_for_report');
        }

        return "Product not found";
    }

    public function formatAddress($address)
    {
        if ($address) {
            $country =  $this->countryFactory->create()->loadByCode(
                $address->getCountryId()
            )->getName();

            return $country;
        }
        return 'N/a';

    }

    public function getName($order)
    {
        if ($shippingAddress = $order->getShippingAddress()) {
            return $shippingAddress->getName();
        } else if ($billingAddress = $order->getBillingAddress()) {
            return $billingAddress->getName();
        }

        return $order->getCustomerName();
    }

    public function isCountryTaxable($country_name)
    {
        $eu_taxable = array("United Kingdom", "Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus", "Czech Republic", "Denmark", "Estonia", "Finland", "France", "Germany", "Greece", "Hungary", "Ireland", "Italy", "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland", "Portugal", "Romania", "Slovakia", "Slovenia", "Spain", "Sweden");

        if ( in_array( $country_name, $eu_taxable ) ) {
            return true;
        }
        
    }

    public function isItemTaxable($item_type)
    {
        $type_taxable = array("Supplement", "Shipping", "Food VAT");

        if ( in_array( $item_type, $type_taxable ) ) {
            return true;
        }
        
    }

     
    public function calculateVAT($total, $vat)
    {
        return $total - ($total / ( 1 + $vat));
        
    }

}

