<?php

namespace Objectsource\Reports\Block\Adminhtml\Report\Filter\Form;

class Accountancy extends \Magento\Reports\Block\Adminhtml\Filter\Form
{
    protected function _prepareForm()
    {

        $actionUrl = $this->getUrl('*/*/exportAccountancyCsv');

        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'filter_form',
                    'action' => $actionUrl,
                    'method' => 'get'
                ]
            ]
        );

        $htmlIdPrefix = 'sales_report_';
        $form->setHtmlIdPrefix($htmlIdPrefix);
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Filter')]);

        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);

        $fieldset->addField('store_ids', 'hidden', ['name' => 'store_ids']);

        $fieldset->addField(
            'order_from',
            'date',
            [
                'name' => 'order_from',
                'date_format' => $dateFormat,
                'label' => __('Order From'),
                'title' => __('Order From'),
                'required' => false,
                'class' => 'admin__control-text'
            ]
        );

        $fieldset->addField(
            'order_to',
            'date',
            [
                'name' => 'order_to',
                'date_format' => $dateFormat,
                'label' => __('Order To'),
                'title' => __('Order To'),
                'required' => false,
                'class' => 'admin__control-text'
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        return $this;
    }
}
