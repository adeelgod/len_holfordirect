<?php

namespace Objectsource\Autoemail\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_ENABLED = 'autoemail/email/enabled';

    const XML_PATH_EMAIL_TEMPLATE = 'autoemail/email/template';

    const XML_PATH_EMAIL_DAYS = 'autoemail/email/reminder_days';

    public function isEnabled($store = null)
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    public function getEmailTemplate($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_EMAIL_TEMPLATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    public function getReminderDays($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_EMAIL_DAYS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }
}
