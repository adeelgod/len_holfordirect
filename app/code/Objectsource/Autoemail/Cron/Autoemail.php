<?php

namespace Objectsource\Autoemail\Cron;

use Magento\Sales\Model\Order\Email\Container\OrderIdentity;
use Magento\Sales\Model\Order\Email\Container\Template;

class Autoemail {
 
    protected $_logger;
    protected $_orderCollectionFactory;
    protected $_transportBuilder;
    protected $_emulation;
    protected $_storeManager;
    protected $_productRepository;
    protected $_helper;
    
    const AUTOEMAIL_STORE_ID = 1;
 
    public function __construct(
        \Magento\Store\Model\App\Emulation $emulation,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Objectsource\Autoemail\Helper\Data $helper
    ) {
        $this->_emulation = $emulation;
        $this->_transportBuilder = $transportBuilder;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_logger = $logger;
        $this->_storeManager = $storeManager;
        $this->_productRepository = $productRepository;
        $this->_helper = $helper;
    }

    protected function sendAutoemail($order, $product) {

        $initialEnvironmentInfo = $this->_emulation->startEnvironmentEmulation(self::AUTOEMAIL_STORE_ID);
        
        $transport = $this->_transportBuilder
            ->setTemplateIdentifier($this->_helper->getEmailTemplate())
            ->setTemplateOptions(array(
                'area'      => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store'     => $this->_storeManager->getStore()->getId()
            ))
            ->setTemplateVars(array(
                "store"                 => $this->_storeManager->getStore(),
                "product_name"          => $product->getName(),
                "product_url"           => $product->setStoreId($this->_storeManager->getStore()->getId())->getUrlInStore(),
                "product_review_url"    => $product->setStoreId($this->_storeManager->getStore()->getId())->getUrlInStore() . "#reviews",
                "daily_usage"           => $product->getDailyUsageAmount(),
                "usage_qty"             => $product->getQuantityUnit(),
                "customer_name"         => $order->getCustomerName(),
            ))
            ->setFrom(array(
                "name"      => 'Holford Direct',
                "email"     => "holfordirect.com"
            ))
            ->addTo($order->getCustomerEmail())
            ->getTransport();
        
        $transport->sendMessage();
        
        $this->_emulation->stopEnvironmentEmulation();
    }
    
    public function execute() {
        $this->_logger->info(__METHOD__);
 
        if ($this->_helper->isEnabled()) {
 
            $_orders = $this->_orderCollectionFactory->create();
            //$_orders->addAttributeToFilter("autoemail_sent", array("neq" => 1));
            $_orders->load();

            foreach ($_orders as $_order) {
                
                if ($_order->getAutoemailSent() || $_order->getStatus() != "complete") { continue; }
                
                foreach ($_order->getItems() as $_item) {

                    $product = $this->_productRepository->getById($_item->getProductId());
                    $quantity = $product->getQuantity();
                    $dailyUsage = $product->getDailyUsageAmount();
                    
                    if ($quantity && $dailyUsage) {
                        $numDays = round($quantity / $dailyUsage);
                        $curDate = date("Y-m-d");
                        $endDate = date("Y-m-d", strtotime($_order->getCreatedAt(). " + $numDays days"));
                        $numDays -= $this->_helper->getReminderDays();
                        $remDate = date("Y-m-d", strtotime($_order->getCreatedAt(). " + $numDays days"));
                        
                        if ($curDate>$remDate && $curDate<$endDate) {
                            $this->sendAutoEmail($_order, $product);
                            $_order->setAutoemailSent(1);
                            $_order->save();
                            break;
                        }
                    }
                }
            }
        }
        
        return $this;
    }
}