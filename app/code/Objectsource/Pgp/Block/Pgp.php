<?php

namespace Objectsource\Pgp\Block;

class Pgp extends \Magento\Framework\View\Element\Template
{
    protected $_json;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
    
    public function getProductCollection()
    {
        $this->_json = $this->getRequest()->getPost();
        $this->_json = json_decode($this->_json["data"]);
        $skus = array();
        foreach ($this->_json->basket as $sku) {
            $skus[] = $sku->sku;
        }
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
        $productCollection->addAttributeToSelect('*');
        $productCollection->addAttributeToFilter("sku", array("in" => $skus));
        $productCollection->load();

        return $productCollection;
    }
    
    public function getImage($product, $imageId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $imageBuilder = $objectManager->create('Magento\Catalog\Block\Product\ImageBuilder');
        return $imageBuilder->setProduct($product)->setImageId($imageId)->create();
    }
    
    public function getAddActionUrl()
    {
        return $this->getUrl('pgp/index/add');
    }
}