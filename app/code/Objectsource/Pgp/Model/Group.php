<?php

namespace Objectsource\Pgp\Model;

use Objectsource\Pgp\Api\GroupInterface;

class Group implements GroupInterface
{
    // Deprecated
    public function setgroup($json)
    {
        return "";
    }
}