<?php

namespace Objectsource\Pgp\Api;

interface GroupInterface
{
    /**
     * Sets the product group
     *
     * @param string $json
     * @return string
     */
    public function setgroup($json);
}