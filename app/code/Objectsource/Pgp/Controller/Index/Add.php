<?php

namespace Objectsource\Pgp\Controller\Index; 

class Add extends \Magento\Framework\App\Action\Action {

    public function __construct(\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
     
    public function execute()
    { 
        try {
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $cart = $om->create('Magento\Checkout\Model\Cart');
            $params = $this->getRequest()->getParams();

            foreach ($params["pg"] as $pgId) {
                $cart->addProduct($pgId, array("qty" => 1));
            }
            $cart->save();

            $this->messageManager->addSuccess(__('Add to cart successfully.'));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addException(
            $e,
            __('%1', $e->getMessage())
            );
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('error.'));
        }
        
        /*cart page*/
        $this->getResponse()->setRedirect('/checkout/cart/index');
    }
}