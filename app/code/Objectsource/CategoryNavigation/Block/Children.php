<?php

namespace Objectsource\CategoryNavigation\Block;

class Children extends \Magento\Framework\View\Element\Template
{
    /** @var \Magento\Catalog\Model\Category $_category */
    protected $_category;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data)
    {
        $this->_category = $registry->registry('current_category');

        \Magento\Framework\View\Element\Template::__construct($context, $data);
    }

    public function getChildCategories()
    {
        $childCategories = $this->_category->getChildrenCategories();
        if (count($childCategories)) {
            return $childCategories;
        }
        return $this->_category->getParentCategory()->getChildrenCategories();
    }

    public function getClassName($category)
    {
        /**
         * Get a CSS classname from a category. Do this by:
         *
         * 1. Lower casing the output
         * 2. Swapping spaces for dashes
         * 3. Removing any non alpha numberic characters
         */
        return strtolower(str_replace(" ", "", preg_replace("/[^A-Za-z0-9 ]/", '', $category->getName())));
    }

    public function getParentCategory()
    {
        return $this->_category;
    }
}
