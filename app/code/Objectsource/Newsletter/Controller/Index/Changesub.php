<?php

namespace Objectsource\Newsletter\Controller\Index;

class Changesub extends \Magento\Framework\App\Action\Action
{
    protected $_request;
    protected $_jsonResultFactory;
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_jsonResultFactory = $jsonResultFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_request = $request;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->_jsonResultFactory->create();

        $this->_checkoutSession->setData('email_optin_ph', $this->_request->getParam('email-optin-ph') == "true");
        $this->_checkoutSession->setData('email_optin_hd', $this->_request->getParam('email-optin-hd') == "true");

        return $result;
    }
}
