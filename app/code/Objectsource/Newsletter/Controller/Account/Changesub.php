<?php

namespace Objectsource\Newsletter\Controller\Account;

class Changesub extends \Magento\Framework\App\Action\Action
{
    protected $_request;
    protected $_resultRedirectFactory;
    protected $_customerSession;
    protected $_customerFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_request = $request;
        $this->_resultRedirectFactory = $resultRedirectFactory;
        $this->_customerSession = $customerSession;
        $this->_customerFactory = $customerFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->_request->getParams();
        $subject = $this->_customerSession->getCustomer();
        $subject = $this->_customerFactory->create()->updateData($subject->getDataModel());

        $emailOptinPH = isset($params['email_opt_in_ph']) ? 1 : 0;
        $emailOptinHD = isset($params['email_opt_in_hd']) ? 1 : 0;

        $subject->setData('email_opt_in_ph', $emailOptinPH);
        $subject->setData('email_opt_in_hd', $emailOptinHD);
        $subject->save();

        $result = $this->resultRedirectFactory->create();
        $result->setPath('newsletter/manage/index');
        return $result;
    }

}
