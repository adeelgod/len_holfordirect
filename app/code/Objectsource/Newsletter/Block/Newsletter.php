<?php

namespace Objectsource\Newsletter\Block;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * Customer front  newsletter manage block
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class Newsletter extends \Magento\Customer\Block\Account\Dashboard
{
    protected $_template = 'form/newsletter.phtml';

    public function isCustomerSubscribedToPH()
    {
        $customer = $this->customerSession->getCustomer();
        return $customer->getData('email_opt_in_ph');
    }

    public function isCustomerSubscribedToHD()
    {
        $customer = $this->customerSession->getCustomer();
        return $customer->getData('email_opt_in_hd');
    }
}
