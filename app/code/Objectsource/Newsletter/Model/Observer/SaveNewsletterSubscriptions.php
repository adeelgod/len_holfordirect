<?php

namespace Objectsource\Newsletter\Model\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SaveNewsletterSubscriptions implements ObserverInterface
{
    protected $_customerSession;
    protected $_checkoutSession;
    protected $_configScope;

    protected $_customerFactory;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Config\ScopeInterface $configScope,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_configScope = $configScope;
        $this->_customerFactory = $customerFactory;
    }


    public function execute(EventObserver $observer)
    {
        if ($this->_customerSession->isLoggedIn()) {
            $subject = $this->_customerSession->getCustomer();
            $subject = $this->_customerFactory->create()->updateData($subject->getDataModel());

            $emailOptinPH = $this->_checkoutSession->getData('email_optin_ph');
            $emailOptinHD = $this->_checkoutSession->getData('email_optin_hd');

            if ($emailOptinPH) {
                $subject->setData('email_opt_in_ph', $emailOptinPH);
                $this->_checkoutSession->unsetData('email_optin_ph');
            }

            if ($emailOptinHD) {
                $subject->setData('email_opt_in_hd', $emailOptinHD);
                $this->_checkoutSession->unsetData('email_optin_hd');
            }
            $subject->save();
        }
        return $this;
    }

}

