var config = {
    map: {
        '*': {
            jcarousel: 'Objectsource_Slideshow/js/lib/jquery.jcarousel.min',
            touchwipe: 'Objectsource_Slideshow/js/lib/jquery.touchwipe.min',
            pikachoose: 'Objectsource_Slideshow/js/lib/jquery.pikachoose.min'
        }
    },

    shim: {
        jcarousel: {
            deps: ['jquery']
        },
        touchwipe: {
            deps: ['jquery']
        },
        pikachoose: {
            deps: ['jquery']
        }
    }

};