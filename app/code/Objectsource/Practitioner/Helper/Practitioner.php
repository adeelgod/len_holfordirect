<?php

namespace Objectsource\Practitioner\Helper;

class Practitioner
{
    protected $practitionerGroupId = null;
    /**
     * @var \Magento\Customer\Model\ResourceModel\CustomerRepository
     */
    private $customerRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var \Magento\Customer\Model\ResourceModel\GroupRepository
     */
    private $customerGroupRepository;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Magento\Customer\Model\ResourceModel\GroupRepository $customerGroupRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->customerGroupRepository = $customerGroupRepository;

        $searchCriteria = $this->searchCriteriaBuilder->addFilter('customer_group_code', 'Practitioner')->create();
        $result = $this->customerGroupRepository->getList($searchCriteria);
        $customerGroups = $result->getItems();
        if (count($customerGroups)) {
            $this->practitionerGroupId = $customerGroups[0]->getId();
            // We've just search for all customer groups named "Practitioner", so in reality there's only going to be one.
        }
    }

    public function getPractitionerFromCode($code)
    {
        if (!$this->practitionerGroupId) {
            return false;
        }

        // Get all customers who are practitioners and have $code assigned as their practitioner code. There will only
        // be one.
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('group_id', $this->practitionerGroupId)
            ->addFilter('practitioner_code', $code)
            ->create();

        $result = $this->customerRepository->getList($searchCriteria);
        $practitioners = $result->getItems();

        // In reality there's only going to be one
        if (count($practitioners)) {
            return $practitioners[0];
        }

        return false;
    }
}
