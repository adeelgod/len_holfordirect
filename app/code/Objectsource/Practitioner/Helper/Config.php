<?php

namespace Objectsource\Practitioner\Helper;

class Config
{
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function isPractitionerCode($code)
    {
        // $code is a practitioner code if it starts with the prefix defined in system config.
        return (0 === strpos($code, $this->getPractitionerCodePrefix()));
    }

    public function getPractitionerCodePrefix()
    {
        return $this->scopeConfig->getValue('patrickholford_practitioner/general/code_prefix');
    }
}
