<?php

namespace Objectsource\Practitioner\Controller\Adminhtml\Report;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Objectsource\Reports\Controller\Adminhtml\Report\AbstractCsvExport;

class ExportPractitionerCsv extends AbstractCsvExport
{
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    private $customerFactory;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    private $customerResourceModel;
    /**
     * @var \Objectsource\Practitioner\Helper\Practitioner
     */
    private $practitionerHelper;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Objectsource\Practitioner\Helper\Practitioner $practitionerHelper
    ) {
        parent::__construct($context, $filesystem, $orderCollectionFactory, $fileFactory, $localeDate, $countryFactory);
        $this->practitionerHelper = $practitionerHelper;
    }

    public function execute()
    {
        $filters = $this->getFilters();

        $fileName = 'practitioner_report_'.date('Ymd').'.csv';
        $orderFrom = empty($filters['order_from']) ? null : $this->convertToMysqlDate($filters['order_from']) . " 00:00:00";
        $orderTo = empty($filters['order_to']) ? null : $this->convertToMysqlDate($filters['order_to']) . " 23:59:00";
        $filterRows = $this->getFilterRows($filters);

        $orders = $this->orderCollectionFactory->create();
        if ($orderFrom) {
            $orders->addAttributeToFilter('created_at', ['gteq' => $orderFrom]);
        }
        if ($orderTo) {
            $orders->addAttributeToFilter('created_at', ['lteq' => $orderTo]);
        }

        $orders->addAttributeToFilter('practitioner_code', ['notnull' => true]);

        $data = [
            [
                'Increment ID',
                'Customer Name',
                'Item Sku',
                'Item Qty',
                'Item Price',
                'Item Practitioner Price',
                'Row Total',
                'Discount Total',
                'Discounted Row Total',
                'Practitioner Row Total',
                'Practitioner Code',
                'Practitioner Name',
                'Practitioner Email',
                'Practitioner Commission'
            ]
        ];
        /** @var \Magento\Sales\Model\Order $order */
        foreach ($orders as $order) {
            foreach ($order->getItemsCollection() as $item) {
                $practitionerRowTotal = $this->getPractitionerRowTotal($item);
                if($practitionerRowTotal){
                    $practitionerCommission = $item->getBaseRowTotal() - $item->getBaseDiscountAmount() - $practitionerRowTotal;
                } else {
                    continue; // SKIPP
                }

                $practitioner = $this->practitionerHelper->getPractitionerFromCode($order->getPractitionerCode());
                if ($practitioner) {
                    $name = $practitioner->getFirstname() . ' ' . $practitioner->getLastname();
                    $email = $practitioner->getEmail();
                } else {
                    $name = 'Unknown';
                    $email = 'Unknown';
                }

                $data []= [
                    $order->getIncrementId(),
                    $this->getName($order),
                    $item->getSku(),
                    $item->getQtyOrdered(),
                    $item->getBasePrice(),
                    $item->getPractitionerPrice(),
                    $item->getBaseRowTotal(),
                    $item->getBaseDiscountAmount(),
                    $item->getBaseRowTotal() - $item->getBaseDiscountAmount(),
                    $practitionerRowTotal,
                    $order->getPractitionerCode(),
                    $name,
                    $email,
                    $practitionerCommission
                ];
            }
        }
        $exportType = 'order_items_ordered';
        $data = array_merge($filterRows, $data);
        return $this->fileFactory->create($fileName, $this->getCsvFile($data, $exportType), DirectoryList::VAR_DIR);
    }

    public function getItemType($item)
    {
        $product = $item->getProduct();

        if ($product) {
            return $product->getAttributeText('item_type_for_report');
        }

        return "Product not found";
    }

    public function formatAddress($address)
    {
        if ($address) {
            $country =  $this->countryFactory->create()->loadByCode(
                $address->getCountryId()
            )->getName();

            return $country;
        }
        return 'N/a';

    }

    public function getName($order)
    {
        if ($shippingAddress = $order->getShippingAddress()) {
            return $shippingAddress->getName();
        } else if ($billingAddress = $order->getBillingAddress()) {
            return $billingAddress->getName();
        }

        return $order->getCustomerName();
    }

    public function getPractitionerRowTotal($item)
    {
        $practitionerPrice = $item->getPractitionerPrice();
        if($practitionerPrice){
            return ($practitionerPrice * $item->getQtyOrdered());
        }
        return null;
    }
}

