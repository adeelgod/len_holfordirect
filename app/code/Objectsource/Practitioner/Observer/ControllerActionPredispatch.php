<?php

namespace Objectsource\Practitioner\Observer;

class ControllerActionPredispatch implements \Magento\Framework\Event\ObserverInterface
{
    protected $customerSession;
    protected $checkoutSession;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;
    /**
     * @var \Magento\SalesRule\Model\CouponFactory
     */
    private $couponFactory;

    /**
     * ControllerActionPredispatch constructor.
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\SalesRule\Model\CouponFactory $couponFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\SalesRule\Model\CouponFactory $couponFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->messageManager = $messageManager;
        $this->couponFactory = $couponFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->customerSession->getHasAppliedPractitionerDiscount()) {
            $customer = $this->customerSession->getCustomer();
            if ($customer->getId()) {
                if ($practitionerCode = $customer->getPractitionerCode()) {
                    $coupon = $this->couponFactory->create();
                    $coupon->load($practitionerCode, 'code');
                    if ($coupon->getId()) {
                        $this->checkoutSession->getQuote()->setCouponCode($practitionerCode)->save();
                        $this->customerSession->setHasAppliedPractitionerDiscount(true);
                    } else {
                        $this->messageManager->addErrorMessage(__('Unable to apply your practitioner code. Please contact us to resolve this problem.'));
                    }
                }
            }
        }
    }
}
