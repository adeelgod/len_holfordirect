<?php

namespace Objectsource\Practitioner\Observer;

use Magento\Framework\Event\ObserverInterface;

class AfterQuoteSubmitSuccess implements ObserverInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;
    /**
     * @var \Magento\Customer\Model\ResourceModel\CustomerRepository
     */
    private $customerRepository;
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var \Objectsource\Practitioner\Helper\Config
     */
    private $configHelper;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;
    /**
     * @var \Magento\Customer\Model\ResourceModel\GroupRepository
     */
    private $customerGroupRepository;
    /**
     * @var \Magento\Sales\Api\OrderItemRepositoryInterface
     */
    private $orderItemRepository;

    /**
     * AfterQuoteSubmitSuccess constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Objectsource\Practitioner\Helper\Config $configHelper
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Magento\Customer\Model\ResourceModel\GroupRepository $customerGroupRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository,
        \Objectsource\Practitioner\Helper\Config $configHelper
    )
    {
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->orderRepository = $orderRepository;
        $this->configHelper = $configHelper;
        $this->checkoutSession = $checkoutSession;
        $this->customerGroupRepository = $customerGroupRepository;
        $this->orderItemRepository = $orderItemRepository;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();
        $customer = $quote->getCustomer();
        $couponCode = $quote->getCouponCode();

        if ($this->configHelper->isPractitionerCode($couponCode)) {
            if ($customer->getId()) {
                $customer->setCustomAttribute('practitioner_code', $couponCode);
                $this->customerRepository->save($customer);
            }

            $order->setData('practitioner_code', $couponCode);
            $this->orderRepository->save($order);

            $this->storePractitionerPricesForItems($order);
        } else {
if($customer->getGroupId()) {
            $group = $this->customerGroupRepository->getById($customer->getGroupId());
            $practitionerCode = $customer->getCustomAttribute('practitioner_code');
            if ($group->getCode() == '100% Health Member' && $practitionerCode) {
                $order->setData('practitioner_code', $practitionerCode->getValue());
                $this->orderRepository->save($order);
                $this->storePractitionerPricesForItems($order);
            }
}
        }

        $this->customerSession->setHasAppliedPractitionerDiscount(false);
    }
    
    public function storePractitionerPricesForItems($order)
    {
        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($order->getItems() as $item) {
            $product = $item->getProduct();
            $tierPrices = $product->getTierPrices();
            foreach ($tierPrices as $tierPrice) {
if($tierPrice->getCustomerGroupId() != null) {
                $group = $this->customerGroupRepository->getById($tierPrice->getCustomerGroupId());
                if ($group->getCode() == 'Practitioner') {
                    $item->setData('practitioner_price', $tierPrice->getValue());
                    $this->orderItemRepository->save($item);
                    break;
                }
}
            }
        }
    }
}
