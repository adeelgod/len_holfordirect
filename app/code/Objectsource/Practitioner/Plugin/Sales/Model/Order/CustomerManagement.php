<?php

namespace Objectsource\Practitioner\Plugin\Sales\Model\Order;

class CustomerManagement
{
    protected $orderRepository;
    protected $searchCriteriaBuilder;
    protected $customerRepository;
    protected $configHelper;

    public function __construct(
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Objectsource\Practitioner\Helper\Config $configHelper
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->customerRepository = $customerRepository;
        $this->configHelper = $configHelper;
    }

    public function afterCreate($subject, $result)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('customer_id', $result->getId())->create();

        // Get the list of orders attached to this customer. There will only ever be one, because this customer has just been created
        // after the order has been placed.
        $orders = $this->orderRepository->getList($searchCriteria);
        if ($orders->getSize()) {
            $order = $orders->getFirstItem();
            $couponCode = $order->getCouponCode();
            if ($this->configHelper->isPractitionerCode($couponCode)) {
                $result->setCustomAttribute('practitioner_code', $couponCode);
                $this->customerRepository->save($result);
            }
        }

        return $result;
    }
}
