<?php
namespace Objectsource\Sso\Helper;

class Sso extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function getApiBaseUrl()
    {
        return $this->scopeConfig->getValue('patrickholford_api/general/base_url');
    }

    public function isSsoEnabled()
    {
        return $this->scopeConfig->getValue('patrickholford_api/general/enable_sso');
    }

    public function getApiUrl($endpoint)
    {
        $baseUrl = $this->getApiBaseUrl();
        if ($baseUrl) {
            return $baseUrl . $endpoint;
        }
        return false;
    }
}
