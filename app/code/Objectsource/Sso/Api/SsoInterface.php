<?php

// eg: http://localhost/index.php/rest/V1/sso/login/2

namespace Objectsource\Sso\Api;

interface SsoInterface
{
    /**
     * Login customer
     *
     * @param int $uid
     * @param string $d
     * @param string $t
     * @param string $h
     * @return string
     */
    public function login($uid, $d, $t, $h);
}