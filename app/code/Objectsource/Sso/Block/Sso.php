<?php

namespace Objectsource\Sso\Block;

class Sso extends \Magento\Framework\View\Element\Template
{
    const SSO_SRC = 'https://www.patrickholford.com/sso';
    const SSO_PASSPHRASE = "PHHDSSO_*&!";

    protected $ssoHelper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Objectsource\Sso\Helper\Sso $ssoHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->ssoHelper = $ssoHelper;
    }


    public function isLoggedOn()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        return $customerSession->isLoggedIn();
    }
    
    public function getSsoSource()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        
        $uid = $customerSession->getCustomer()->getSsoId();
        $d = date("d-m-Y");
        $t = date("H-i");
        $h = md5($uid.self::SSO_PASSPHRASE.$d.$t);
        
        $url = self::SSO_SRC;
        $url .= "?uid=$uid";
        $url .= "&d=$d";
        $url .= "&t=$t";
        $url .= "&h=$h";
        
        return  $url;
    }

    public function isEnabled()
    {
        return $this->ssoHelper->isSsoEnabled();
    }
}