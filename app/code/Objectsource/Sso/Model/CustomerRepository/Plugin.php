<?php

namespace Objectsource\Sso\Model\CustomerRepository;

class Plugin {

    const SSO_GET_USER = "user.php?mode=GetUser";
    const SSO_CREATE_USER = "user.php?mode=CreateUser";
    const SSO_UPDATE_USER = "user.php?mode=UpdateUser";
    const SSO_GET_PASSWORD_HASH = "user.php?mode=UserGetPasswordHash";

    protected $request;

    protected $_userRecord = array(
        "ID"                        =>  0,
        "HDUserID"                  =>  0,
        "PHUserID"                  =>  0,
        "Salutation"                =>  null,
        "FirstName"                 =>  null,
        "LastName"                  =>  null,
        "Email"                     =>  null,
        "PasswordHash"              =>  null,
        "Telephone"                 =>  null,
        "Gender"                    =>  null,
        "DateOfBirth"               =>  null,
        "Ethnicity"                 =>  null,
        "ProfileImage"              =>  null,
        "SecretUrl"                 =>  null,
        "SecretQuestion"            =>  null,
        "SecretAnswer"              =>  null,
        "MembershipType"            =>  "0000000100",
        "MembershipExpired"         =>  null,
        "MembershipEnd"             =>  null,
        "Free30minCall"             =>  null,
        "Source"                    =>  null,
        "QuestionnaireReminderDate" =>  null,
        "EmailOptInPH"              =>  null,
        "EmailOptInHD"              =>  null,
        "LastHDOrderDate"           =>  null,
        "TotalHDOrdersValue"        =>  null,
        "NumberOfHDOrders"          =>  null,
        "LastLogin"                 =>  null,
        "member_goal"               =>  null
    );

    protected $_checkoutSession;

    protected $ssoHelper;

    public function objectToArray($object) {
        if (is_object($object)) {
            return array_map(__METHOD__, get_object_vars($object));
        }
        else if (is_array($object)) {
            return array_map(__METHOD__, $object);
        }
        else {
            return $object;
        }
    }

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Objectsource\Sso\Helper\Sso $ssoHelper
    ) {
        $this->request = $request;
        $this->_checkoutSession = $checkoutSession;
        $this->ssoHelper = $ssoHelper;
    }

    public function aroundSave(
        \Magento\Customer\Api\CustomerRepositoryInterface $subject,
        \Closure $proceed,
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        $passwordHash = null
    )
    {
        $id = $customer->getId();

        // Get Customer, if not creating one
        if ($id) {
            if ($getUserUrl = $this->ssoHelper->getApiUrl(self::SSO_GET_USER)) {
                $_userDetails = array(
                    "HDUserID" => $id
                );
                $client = new \Zend_Http_Client();
                $client->setUri($getUserUrl);
                $client->setConfig(['maxredirects' => 0, 'timeout' => 60]);
                $json = json_encode($_userDetails);
                $client->setRawData($json, 'application/json');
                $response = $client->request();
                $responseBody = $response->getBody();
                $this->_userRecord = $this->objectToArray(json_decode($responseBody));
                if (isset($this->_userRecord[0])) {
                    $this->_userRecord = $this->_userRecord[0];
                }
            }
        }

        // Save customer
        $result = false;
        try {
            $result = $proceed($customer, $passwordHash);
        } catch (Exception $e) {
            // NOTHING
        }

        // Build export of customer to central API
        $this->_userRecord["FirstName"] = $result->getFirstname();
        $this->_userRecord["LastName"] = $result->getLastname();
        $this->_userRecord["Email"] = $result->getEmail();
        $this->_userRecord["MembershipType"] = ($this->_userRecord["MembershipType"] | "0000000100");
        $this->_userRecord["HDUserID"] = $result->getId();
        $this->_userRecord["EmailOptInPH"] = $result->getCustomAttribute('email_opt_in_ph') ? 1 : 0;
        $this->_userRecord["EmailOptInHD"] = $result->getCustomAttribute('email_opt_in_hd') ? 1 : 0;

        // Get new password hash if passwords changed
        if ((!$id || $this->request->getPost("change_password") == 1) && $this->request->getPost("password") == $this->request->getPost("password_confirmation")) {
            if ($getPasswordHashUrl = $this->ssoHelper->getApiUrl(self::SSO_GET_PASSWORD_HASH)) {
                $client = new \Zend_Http_Client();
                $client->setUri($getPasswordHashUrl);
                $client->setConfig(['maxredirects' => 0, 'timeout' => 60]);
                $json = json_encode(['Password' => $this->request->getPost("password")]);
                $client->setRawData($json, 'application/json');
                $response = $client->request();
                $responseBody = $response->getBody();
                $responseBody = json_decode($responseBody);

                $this->_userRecord["PasswordHash"] = $responseBody->Hash;
            }
        }

        // Create/Update customer on central API
        $updateUserUrl = $this->ssoHelper->getApiUrl(self::SSO_UPDATE_USER);
        $createUserUrl = $this->ssoHelper->getApiUrl(self::SSO_CREATE_USER);
        if ($createUserUrl || $updateUserUrl) {
            $client = new \Zend_Http_Client();
            if ($id != 0) {
                $client->setUri($updateUserUrl);
                $this->_userRecord["DateUpdated"] = date("Y-m-d H:i:s");
            } else {
                $client->setUri($createUserUrl);
            }
            $client->setConfig(['maxredirects' => 0, 'timeout' => 60]);
            $json = json_encode($this->_userRecord);
            $client->setRawData($json, 'application/json');
            $response = $client->request();
            $responseBody = $response->getBody();
            $responseBody = json_decode($responseBody);
        }

        return $result;
    }
}
