<?php
namespace Objectsource\Sso\Model\Address;

class Plugin {

    const SSO_GET_USER = "user.php?mode=GetUser";
    const SSO_UPDATE_USER = "user.php?mode=UpdateUser";

    protected $_userRecord;
    
    protected $_addressRecord = array(
        "id"                =>  0,
        "hdaddressid"       =>  0,
        "customer_id"       =>  0,
        "region"            =>  [ "region_code"=>null, "region"=>null, "region_id"=>0 ],
        "region_id"         =>  0,
        "country_id"        =>  null,
        "street"            =>  null,
        "telephone"         =>  null,
        "postcode"          =>  null,
        "city"              =>  null,
        "firstname"         =>  null,
        "lastname"          =>  null,
        "default_shipping"  =>  0,
        "default_billing"   =>  0
    );

    protected $ssoHelper;

    public function __construct(
        \Objectsource\Sso\Helper\Sso $ssoHelper
    ) {
        $this->ssoHelper = $ssoHelper;
    }


    public function objectToArray($object) {
        if (is_object($object)) {
            return array_map(__METHOD__, get_object_vars($object));
        }
        else if (is_array($object)) {
            return array_map(__METHOD__, $object);
        }
        else {
            return $object;
        }
    }

    public function aroundSave(\Magento\Customer\Model\Address $subject, $proceed)
    {
        $id = $subject->getId();

        // Get Customer
        $customer = $subject->getCustomer();
        $_userDetails = array(
            "HDUserID"  =>  $customer->getId()
        );
        if ($getUserUrl = $this->ssoHelper->getApiUrl(self::SSO_GET_USER)) {
            $client = new \Zend_Http_Client();
            $client->setUri($getUserUrl);
            $client->setConfig(['maxredirects' => 0, 'timeout' => 60]);
            $json = json_encode($_userDetails);
            $client->setRawData($json, 'application/json');
            $response = $client->request();
            $responseBody = $response->getBody();
            $this->_userRecord = $this->objectToArray(json_decode($responseBody));
            if (isset($this->_userRecord[0])) {
                $this->_userRecord = $this->_userRecord[0];
            }
        }

        // Save address
        $result = $proceed();

        if ($subject->getIsCustomerSaveTransaction()) {
            return $result;
        }
        // Build export to central db of address
        $this->_addressRecord["id"]                 = $subject->getSsoId();
        $this->_addressRecord["hdaddressid"]        = $subject->getId();
        $this->_addressRecord["customer_id"]        = $customer->getId();
        $this->_addressRecord["region"]             = array(
                                                        "region_code"   =>  $subject->getRegionCode(),
                                                        "region"        =>  $subject->getRegion(),
                                                        "region_id"     =>  $subject->getRegionId()
                                                        );
        $this->_addressRecord["region_id"]          = $subject->getRegionId();
        $this->_addressRecord["country_id"]         = $subject->getCountryId();
        $this->_addressRecord["street"]             = implode(" ", $subject->getStreet());
        $this->_addressRecord["telephone"]          = $subject->getTelephone();
        $this->_addressRecord["postcode"]           = $subject->getPostcode();
        $this->_addressRecord["city"]               = $subject->getCity();
        $this->_addressRecord["firstname"]          = $subject->getFirstname();
        $this->_addressRecord["lastname"]           = $subject->getLastname();
        $this->_addressRecord["default_shipping"]   = $subject->getIsDefaultShipping();
        $this->_addressRecord["default_billing"]    = $subject->getisDefaultBilling();
        
        // Update/create address in customer
        $new = true;
        for ($i=0; $i<count($this->_userRecord["addresses"]); $i++) {
            $address = $this->_userRecord["addresses"][$i];
            if ($address["hdaddressid"] == $id) {
                $this->_userRecord["addresses"][$i] = $this->_addressRecord;
                $new = false;
                break;
            }
        }
        if ($new) {
            $this->_userRecord["addresses"][] = $this->_addressRecord;
        }
        
        // Update Customer addresses
        $updateUserUrl = $this->ssoHelper->getApiUrl(self::SSO_UPDATE_USER);
        if ($updateUserUrl) {
            $client = new \Zend_Http_Client();
            $client->setUri($updateUserUrl);
            $this->_userRecord["DateUpdated"] = date("Y-m-d H:i:s");
            $client->setConfig(['maxredirects' => 0, 'timeout' => 60]);
            $json = json_encode($this->_userRecord);
            $client->setRawData($json, 'application/json');
            $response = $client->request();
            $responseBody = $response->getBody();
            $responseBody = json_decode($responseBody);
        }

        return $result;
    }  
}
