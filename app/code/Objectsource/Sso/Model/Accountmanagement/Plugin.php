<?php

namespace Objectsource\Sso\Model\Accountmanagement;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Exception;

class Plugin {

    const SSO_RESET_PASSWORD = "user.php?mode=UserResetPassword";
    const SSO_LOGIN = "user.php?mode=UserLogin";
    
    protected $request;
    protected $customerFactory;
    protected $eventManager;
    protected $customerRepository;
    protected $ssoHelper;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        CustomerFactory $customerFactory,
        ManagerInterface $eventManager,
        CustomerRepositoryInterface $customerRepository,
        \Objectsource\Sso\Helper\Sso $ssoHelper
    ) {
        $this->request = $request;
        $this->customerFactory = $customerFactory;
        $this->eventManager = $eventManager;
        $this->customerRepository = $customerRepository;
        $this->ssoHelper = $ssoHelper;
    }
    
    public function aroundAuthenticate($subject, $proceed, $username, $password)
    {    
        $_passwordDetails = array( 
            "Email"     => $username,
            "Password"  => $password
        );
        $result = false;
        $retry = false;

        try {
            $result = $proceed($username, $password);
        }
        catch (\Exception $e) {
            $retry=true;
        }

        if ($retry) {
            $client = new \Zend_Http_Client();
            $loginUrl = $this->ssoHelper->getApiUrl(self::SSO_LOGIN);
            if ($loginUrl) {
                $client->setUri($loginUrl);
                $client->setMethod('POST');
                $client->setConfig(['maxredirects' => 0, 'timeout' => 60]);
                $json = json_encode($_passwordDetails);
                $client->setRawData($json, 'application/json');
                $response = $client->request();
                $responseBody = $response->getBody();
                $responseBody = json_decode($responseBody);

                if (isset($responseBody->UserLogin)) {
                    throw new \Exception("Invalid login or password.");
                }
                else {
                    $responseBody = $responseBody[0];
                    try {
                        $customer = $this->customerRepository->get($username);
                    }
                    catch (\Exception $f) {
                        throw new \Exception("Invalid login or password.");
                    }
                    $customerModel = $this->customerFactory->create()->updateData($customer);
                    $this->eventManager->dispatch(
                        'customer_customer_authenticated',
                        ['model' => $customerModel, 'password' => $password]
                    );
                    $this->eventManager->dispatch('customer_data_object_login', ['customer' => $customer]);
                    return $customer;
                }
            }
        }

        return $result;
    }
}