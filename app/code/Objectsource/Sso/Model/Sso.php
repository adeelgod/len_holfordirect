<?php

namespace Objectsource\Sso\Model;

use Objectsource\Sso\Api\SsoInterface;

class Sso implements SsoInterface
{   
    const SSO_CHECK_USER_EXISTS = "https://accounts.patrickholford.com/user.php?mode=UserExists";

    const SSO_PASSPHRASE = "PHHDSSO_*&!";
    
    protected function checkHash($uid, $d, $t, $h)
    {
        $hashCheck = md5($uid.self::SSO_PASSPHRASE.$d.$t);
        return $h == $hashCheck;
    }
    
    public function login($uid, $d, $t, $h)
    {
        if ($this->checkHash($uid, $d, $t, $h)) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customers = $objectManager->create('Magento\Customer\Model\Customer')->getCollection();
            $customers->addAttributeToFilter("sso_id", $uid);
            
            foreach ($customers as $customer) {
                $client = new \Zend_Http_Client();
                $client->setUri(self::SSO_CHECK_USER_EXISTS);
                $client->setConfig(['maxredirects' => 0, 'timeout' => 60]);
                $json = json_encode(["Email" => $customer->getEmail()]);
                $client->setRawData($json, 'application/json');
                $response = $client->request();
                $responseBody = $response->getBody();        
                $responseBody = json_decode($responseBody);
                
                if ($responseBody->UserExists) {
                    $session = $objectManager->create('Magento\Customer\Model\Session');
                    $session->setCustomerAsLoggedIn($customer);
                }
                
                return "";
            }
        }
        
        return "";
    }
}
