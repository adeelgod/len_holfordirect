<?php

namespace Objectsource\Sso\Model\Customer;

class Plugin {

    const SSO_GET_USER = "user.php?mode=GetUser";
    const SSO_CREATE_USER = "user.php?mode=CreateUser";
    const SSO_UPDATE_USER = "user.php?mode=UpdateUser";
    const SSO_GET_PASSWORD_HASH = "user.php?mode=UserGetPasswordHash";
            
    protected $request;
    
    protected $_userRecord = array(
        "ID"                        =>  0,
        "HDUserID"                  =>  0,
        "PHUserID"                  =>  0,
        "Salutation"                =>  null,
        "FirstName"                 =>  null,
        "LastName"                  =>  null,
        "Email"                     =>  null,
        "PasswordHash"              =>  null,
        "Telephone"                 =>  null,
        "Gender"                    =>  null,
        "DateOfBirth"               =>  null,
        "Ethnicity"                 =>  null,
        "ProfileImage"              =>  null,
        "SecretUrl"                 =>  null,
        "SecretQuestion"            =>  null,
        "SecretAnswer"              =>  null,
        "MembershipType"            =>  "0000000100",
        "MembershipExpired"         =>  null,
        "MembershipEnd"             =>  null,
        "Free30minCall"             =>  null,
        "Source"                    =>  null,
        "QuestionnaireReminderDate" =>  null,
        "EmailOptInPH"              =>  null,
        "EmailOptInHD"              =>  null,
        "LastHDOrderDate"           =>  null,
        "TotalHDOrdersValue"        =>  null,
        "NumberOfHDOrders"          =>  null,
        "LastLogin"                 =>  null,
        "member_goal"               =>  null
    );

    protected $_checkoutSession;

    protected $ssoHelper;

    public function objectToArray($object) {
        if (is_object($object)) {
            return array_map(__METHOD__, get_object_vars($object));
        }
        else if (is_array($object)) {
            return array_map(__METHOD__, $object);
        }
        else {
            return $object;
        }
    }
    
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Objectsource\Sso\Helper\Sso $ssoHelper
    ) {
        $this->request = $request;
        $this->_checkoutSession = $checkoutSession;
        $this->ssoHelper = $ssoHelper;
    }

    public function beforeSave(\Magento\Customer\Model\Customer $subject)
    {
        $emailOptinPH = $this->_checkoutSession->getData('email_optin_ph') ? 1 : 0;
        $emailOptinHD = $this->_checkoutSession->getData('email_optin_hd') ? 1 : 0;

        if ($emailOptinPH) {
            $subject->setData('email_opt_in_ph', $emailOptinPH);
            $this->_checkoutSession->unsetData('email_optin_ph');
        }

        if ($emailOptinHD) {
            $subject->setData('email_opt_in_hd', $emailOptinHD);
            $this->_checkoutSession->unsetData('email_optin_hd');
        }
    }
}
