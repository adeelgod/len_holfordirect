<?php

namespace Objectsource\Related\Block\Cart;

class Items extends \Magento\Checkout\Block\Cart\Crosssell
{

    /**
     * Product collection factory
     *
     * @var \Aheadworks\Wbtab\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Repository
     */
    protected $productAttributeRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    // Make sure divisible by 3
    const ITEMS_TO_GET = 6;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Catalog\Model\Product\LinkFactory $productLinkFactory,
        \Magento\Quote\Model\Quote\Item\RelatedProducts $itemRelationsList,
        \Magento\CatalogInventory\Helper\Stock $stockHelper,
        \Magento\Framework\Module\Manager $moduleManager,
        \Aheadworks\Wbtab\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        array $data
        )
    {
        parent::__construct($context, $checkoutSession, $productVisibility, $productLinkFactory, $itemRelationsList, $stockHelper, $data);
        $this->productCollectionFactory = $productCollectionFactory;
        $this->moduleManager = $moduleManager;
        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }


    public function getItems()
    {
        if (!$this->getData('items')) {
            $crossSellItems = parent::getItems();

            $goalItems = $this->getGoalItems();

            $customersAlsoPurchasedItems = $this->getCustomersAlsoPurchasedItems();

            $allItems = array();

            // Try to add 2 of each item source (rememember there might not be two from each)
            $this->_addItems($allItems, $crossSellItems, self::ITEMS_TO_GET / 3, 'crosssell');
            $this->_addItems($allItems, $goalItems, self::ITEMS_TO_GET / 3, 'goal', 'random');
            $this->_addItems($allItems, $customersAlsoPurchasedItems, self::ITEMS_TO_GET / 3, 'customersalsopurchased');

            while(self::ITEMS_TO_GET - count($allItems)) {
                // If we didn't get enough, then look at what sources have items
                $sources = $this->_sourcesWithItems($crossSellItems, $goalItems, $customersAlsoPurchasedItems);

                // If we don't have any more sources, then just break out since there's nothing we can show.
                if (!count($sources)) {
                    break;
                }

                // Pick on of the sources at random and add a product from it
                shuffle($sources);
                switch (array_shift($sources)) {
                    case 'crosssells':
                        $this->_addItems($allItems, $crossSellItems, 1, 'crosssell');
                        break;
                    case 'goals':
                        $this->_addItems($allItems, $goalItems, 1, 'goal', 'random');
                        break;
                    case 'customersalsopurchased':
                        $this->_addItems($allItems, $customersAlsoPurchasedItems, 1, 'customersalsopurchased');
                        break;
                }
            };

            $this->setData('items', $allItems);
        }
        return $this->getData('items');


    }

    public function getGoalItems()
    {
        $customer = $this->customerSession->getCustomer();

        $memberGoal = $customer->getMemberGoal();

        if (!$memberGoal) {
            return array();
        }

        $attributeValue = null;
        // Get the value of the member goal
        $goal = $this->productAttributeRepository->get('goal');
        foreach ($goal->getOptions() as $option) {
            //$manufacturerOption->getValue();  // Value
            if ($option->getLabel() == $memberGoal) {
                $attributeValue = $option->getValue();
            }
        }

        if (!$attributeValue) {
            return array();
        }

        $quoteProductIds = $this->_getCartProductIds();
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('goal', $attributeValue, 'finset')
            ->addFilter('entity_id', $quoteProductIds, 'nin')
            ->create();

        $productsArray = array();
        $result = $this->productRepository->getList($searchCriteria);
        $products = $result->getItems();
        foreach($products as $product) {
            $productsArray []= $product;
        }

        return $productsArray;
    }

    public function getCustomersAlsoPurchasedItems()
    {

        $items = array();
        $sortedItems = array();

        $quoteProductIds = $this->_getCartProductIds();
        foreach ($quoteProductIds as $productId) {
            /** @var \Aheadworks\Wbtab\Model\ResourceModel\Product\Collection $wbtabCollection */
            $wbtabCollection = $this->productCollectionFactory->create();

            $wbtabCollection
                ->addAttributeToSelect('required_options')
                ->addWbtabFilter($productId, $quoteProductIds);

            if ($this->moduleManager->isEnabled('Magento_Checkout')) {
                $this->_addProductAttributesAndPrices($wbtabCollection);
            }

            $wbtabCollection->load();

            foreach ($wbtabCollection as $item) {
                if (isset($items[$item->getId()])) {
                    $sortedItems[$item->getId()]['rating'] += $item->getRating();
                } else {
                    $sortedItems[$item->getId()] = array();
                    $sortedItems[$item->getId()]['product'] = $item;
                    $sortedItems[$item->getId()]['rating'] = $item->getRating();
                }
            }
        }

        // Sort then descending by the 'rating' value, so we get the most relevant at the top.
        usort($sortedItems, array($this, 'sortByRating'));

        foreach ($sortedItems as $item) {
            $items []= $item['product'];
        }

        return $items;
    }

    public function sortByRating($a, $b) {
        return $a['rating'] < $b['rating'];
    }

    protected function _addItems(&$allItems, &$collection, $count, $identifier, $mode='ordered')
    {
        for ($i = 0; $i < $count; $i++) {
            if (count($collection)) {
                if ($mode == 'random') {
                    shuffle($collection);
                }
                $product = array_shift($collection);
                $product->setSource($identifier);
                // Add with the product ID as the index so that we never have duplicates
                $allItems [$product->getId()] = $product;
            }
        }
    }

    /**
     * @param $crossSellItems
     * @param $goalItems
     * @param $customersAlsoPurchasedItems
     * @return array
     */
    protected function _sourcesWithItems($crossSellItems, $goalItems, $customersAlsoPurchasedItems)
    {
        $sources = array();
        if (count($crossSellItems)) {
            $sources [] = 'crosssells';
        }

        if (count($goalItems)) {
            $sources [] = 'goals';
        }

        if (count($customersAlsoPurchasedItems)) {
            $sources [] = 'customersalsopurchased';
        }
        return $sources;
    }

}
