<?php

namespace Objectsource\EmailCart\Block;

use Magento\Catalog\Helper\Image;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\View\Element\Template;

class CartEmailBuilder extends Template
{

    /**
     * @var Session
     */
    private $session;
    /**
     * @var Image
     */
    private $imageHelper;
    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * CartEmailBuilder constructor.
     * @param Image $imageHelper
     * @param Session $session
     * @param Template\Context $context
     * @param CustomerSession $customerSession
     * @param CustomerRepository $customerRepository
     * @param array $data
     */
    public function __construct(
        Image $imageHelper,
        Session $session,
        Template\Context $context,
        CustomerSession $customerSession,
        CustomerRepository $customerRepository,
        array $data = [])
    {
        $this->session = $session;
        $this->imageHelper = $imageHelper;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getCartItems()
    {
        $result = [];
        foreach($this->session->getQuote()->getAllVisibleItems() as $item){
            $product = $item->getProduct();
            $image = $this->imageHelper->init($product, 'product_page_image_small');
            $result[] = [
                'product_name'      => $product->getName(),
                'product_image_url' => $image->constrainOnly(false)->keepAspectRatio(true)->keepFrame(true)->getUrl(),
                'qty'               => $item->getQty()
            ];
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getCustomerAddress()
    {
        $address = $this->customerSession->getCustomer()->getDefaultBillingAddress();
        if($address && $address->getId()) return $this->cleanAddress($address);
        $address = $this->customerSession->getCustomer()->getDefaultShippingAddress();
        if($address && $address->getId()) return $this->cleanAddress($address);
        return '';
    }

    /**
     * @return string
     */
    public function getPractitionerCode()
    {
        $customer = $this->customerRepository->getById($this->customerSession->getId());
        $code = $customer->getCustomAttribute('practitioner_code');
        return $code ? $code->getValue() : "You don't have a code yet. Please get in touch with the Holford Direct team to receive one.";
    }

    /**
     * @param \Magento\Customer\Model\Address $address
     * @return string
     */
    private function cleanAddress($address)
    {
        $ret = preg_replace("/\r+/","\n", $address->format('text'));
        $ret = preg_replace("/\n+/","\n", $ret);
        return $ret;
    }
}