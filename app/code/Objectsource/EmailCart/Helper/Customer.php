<?php

namespace Objectsource\EmailCart\Helper;

use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Model\Session;

class Customer extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Session
     */
    private $customerSession;
    /**
     * @var GroupRepositoryInterface
     */
    private $customerGroupRepository;

    /**
     * Customer constructor.
     * @param Session $customerSession
     * @param GroupRepositoryInterface $customerGroupRepository
     */
    public function __construct(
        Session $customerSession,
        GroupRepositoryInterface $customerGroupRepository
    )
    {
        $this->customerSession = $customerSession;
        $this->customerGroupRepository = $customerGroupRepository;
    }

    /**
     * @return bool
     */
    public function isPractitioner()
    {
        $groupId = $this->customerSession->getCustomer()->getGroupId();
        $group = $this->customerGroupRepository->getById($groupId);

        if($group->getCode() != 'Practitioner'){
            return false;
        }
        return true;
    }
}
