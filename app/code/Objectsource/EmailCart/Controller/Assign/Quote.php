<?php

namespace Objectsource\EmailCart\Controller\Assign;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NotFoundExceptionFactory;

class Quote extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $session;
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;
    /**
     * @var NotFoundExceptionFactory
     */
    private $exceptionFactory;

    public function __construct(
        \Magento\Checkout\Model\Session $session,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        NotFoundExceptionFactory $exceptionFactory,
        Context $context
    )
    {
        $this->session = $session;
        $this->quoteRepository = $quoteRepository;
        $this->exceptionFactory = $exceptionFactory;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        if($this->_request->getParam('qid')){
            $currentQuote = $this->session->getQuote();
            $qid = (int)$this->_request->getParam('qid');
            if($currentQuote->getId() != $qid){
                $this->quoteRepository->delete($currentQuote);
            } else if($currentQuote->getId() == $qid){
                throw $this->exceptionFactory->create(['phrase' => __('Quote already assigned')]);
            }

            $quote = $this->quoteRepository->get($qid);
            $this->session->replaceQuote($quote);
            $this->quoteRepository->save($quote);
            $this->_redirect('checkout/cart/');
        } else {
            throw $this->exceptionFactory->create(['phrase' => __('')]);
        }
    }
}