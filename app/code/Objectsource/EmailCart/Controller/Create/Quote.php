<?php

namespace Objectsource\EmailCart\Controller\Create;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\View\Result\PageFactory;

class Quote extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    /**
     * @var Session
     */
    private $customerSession;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var \Magento\Quote\Model\QuoteManagement
     */
    private $quoteManagement;
    /**
     * @var \Magento\Quote\Api\Data\CartInterface
     */
    private $quote;
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;
    /**
     * @var \Magento\Quote\Api\Data\CartItemInterfaceFactory
     */
    private $cartItemInterfaceFactory;
    /**
     * @var \Magento\Quote\Api\GuestCartItemRepositoryInterface
     */
    private $cartItemRepository;
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlBuilder;
    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $image;
    /**
     * @var \Objectsource\EmailCart\Helper\Customer
     */
    private $customerHelper;


    public function __construct(
        \Objectsource\EmailCart\Helper\Customer $customerHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Quote\Api\CartManagementInterface $quoteManagement,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemInterfaceFactory,
        \Magento\Quote\Api\GuestCartItemRepositoryInterface $cartItemRepository,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Catalog\Helper\Image $image,
        Context $context,
        PageFactory $resultPageFactory,
        Session $session)
    {
        $this->customerHelper = $customerHelper;
        $this->_resultPageFactory = $resultPageFactory;
        $this->session = $session;
        $this->customerSession = $customerSession;
        $this->quoteManagement = $quoteManagement;
        $this->quoteRepository = $quoteRepository;
        $this->cartItemInterfaceFactory = $cartItemInterfaceFactory;
        $this->cartItemRepository = $cartItemRepository;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlBuilder;
        $this->image = $image;
        parent::__construct($context);
    }

    public function execute()
    {
        $address = $this->_request->getParam('address');
        $email = $this->_request->getParam('email');
        $discountCode = $this->_request->getParam('discount_code');
        $clientName = $this->_request->getParam('client_name');
        if($email && $address && $discountCode && $clientName){
            $quoteForClient = $this->cloneQuote($discountCode);

            $storeScope = ScopeInterface::SCOPE_STORE;

            $transport = $this->transportBuilder
                ->setTemplateIdentifier($this->scopeConfig->getValue('objectsource_email_cart/template/email', $storeScope))
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->storeManager->getStore()->getId(),
                    ]
                )
                ->setTemplateVars([
                    'address'           => nl2br($address),
                    'notes'             => nl2br($this->_request->getParam('notes')),
                    'url'               => $this->urlBuilder->getUrl('emailcart/assign/quote', ['qid' => $quoteForClient->getId()]),
                    'client_name'       => $clientName,
                    'products_table'    => $this->getProductsTable()
                ])
                ->setFrom($this->scopeConfig->getValue('objectsource_email_cart/email/sender', $storeScope))
                ->setReplyTo(
                    $this->customerSession->getCustomer()->getEmail()
                )
                ->addTo($email)
                ->getTransport();

            $transport->sendMessage();

            $this->messageManager->addSuccessMessage(__('Your recommendation has been sent'));
        } else {
            $this->messageManager->addErrorMessage(__('Unable to send email as we are missing information'));
        }
        $this->_redirect('emailcart/create/');
    }

    private function cloneQuote($discountCode)
    {
        $newQuoteId = $this->quoteManagement->createEmptyCart();
        /* @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->get($newQuoteId);
        $quote->setCouponCode($discountCode);
        foreach($this->session->getQuote()->getAllVisibleItems() as $item){
            $newItem = $this->cloneItem($item, $quote);
            foreach($item->getChildren() as $childItem){
                $this->cloneItem($childItem, $quote, $newItem);
            }
        }
        return $quote;
    }

    private function cloneItem($item, $quote, $parentItem = null)
    {
        $newItem = clone($item);
        if($parentItem){
            $newItem->setParentItem($parentItem);
        }
        $quote->addItem($newItem);
        $this->quoteRepository->save($quote);
        return $newItem;
    }

    private function getProductsTable()
    {
        $table = "<table>";
        $table .= "<tr><td width='15%'></td><td with='70%' style='font-weight: bold;'>Product Name</td><td width='15%' style='font-weight: bold;'>Code</td></tr>";
        foreach($this->session->getQuote()->getAllVisibleItems() as $item){
            $tr = "<tr>";
            $product = $item->getProduct();
            $tr .= "<td>" . $this->getProductSmallImageHtml($product) . "</td><td>" . $product->getName() . "</td><td>" . $product->getSku() . "</td>";
            $tr .= "</tr>";
            $table .= $tr;
        }
        $table .= "</table>";
        return $table;
    }

    private function getProductSmallImageHtml($product){
        $productImage = $this->image->init($product, 'product_page_image_small');
        $url = $productImage->constrainOnly(false)->keepAspectRatio(true)->keepFrame(true)->getUrl();
        return "<img src='{$url}' />";
    }
    /**
     * Dispatch request
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {
        if (!$this->customerSession->authenticate() || !$this->customerHelper->isPractitioner()) {
            $this->_actionFlag->set('', 'no-dispatch', true);
        }
        return parent::dispatch($request);
    }
}