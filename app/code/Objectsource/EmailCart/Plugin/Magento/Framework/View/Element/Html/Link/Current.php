<?php
namespace Objectsource\EmailCart\Plugin\Magento\Framework\View\Element\Html\Link;
use Objectsource\EmailCart\Helper\Customer;

class Current
{
    /**
     * @var Customer
     */
    private $customerHelper;

    /**
     * Current constructor.
     * @param Customer $customerHelper
     */
    public function __construct(
        Customer $customerHelper
    )
    {
        $this->customerHelper = $customerHelper;
    }

    public function aroundToHtml(\Magento\Framework\View\Element\Html\Link\Current $subject, callable $proceed)
    {
        if($subject->getPath() == 'emailcart/create' && !$this->customerHelper->isPractitioner()){
            return '';
        } else {
            return $proceed();
        }
    }
}
