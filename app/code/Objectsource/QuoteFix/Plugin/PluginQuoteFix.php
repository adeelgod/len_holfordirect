<?php

namespace Objectsource\QuoteFix\Plugin;

class PluginQuoteFix
{
  
	protected $quoteFactory;
	protected $customerRepositoryInterface;
	protected $_session;
	protected $customerSession;
	protected $addressFactory;

	public function __construct(
		\Magento\Quote\Model\QuoteFactory $quoteFactory,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Customer\Model\Session $session,
		\Magento\Quote\Model\Quote\AddressFactory $addressFactory
	) {
		$this->customerRepositoryInterface = $customerRepositoryInterface;
		$this->quoteFactory = $quoteFactory;
		$this->_session = $session;
		$this->customerSession = $customerSession;
		$this->addressFactory = $addressFactory;
	}
  
	public function beforeSave(\Magento\Quote\Model\QuoteRepository $subject) {
	  if ($this->_session->isLoggedIn()) {
		$email = $this->customerSession->getCustomer()->getEmail();
		$customerId = $this->customerSession->getCustomer()->getId();
		
		$quote = $this->quoteFactory
					  ->create()
					  ->getCollection()
					  ->addFieldToFilter('customer_id',$customerId);
		
		
		foreach($quote as $qt) {
		  $address = $this->addressFactory->create()->getCollection()->addFieldToFilter('quote_id',$qt->getId());
		  foreach($address as $adr) {
			if($adr->getCustomerId() != $customerId || $adr->getEmail() != $email) {
				$adr->delete();
			}
		  }
		}
	  }
	}
  
}