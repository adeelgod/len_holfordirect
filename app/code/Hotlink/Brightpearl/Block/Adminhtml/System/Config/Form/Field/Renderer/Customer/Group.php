<?php
namespace Hotlink\Brightpearl\Block\Adminhtml\System\Config\Form\Field\Renderer\Customer;

class Group extends \Magento\Framework\View\Element\Html\Select
{

    /**
     * @var \Magento\Customer\Model\Config\Source\Group
     */
    protected $customerConfigSourceGroup;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Customer\Model\Config\Source\Group $customerConfigSourceGroup,
        array $data = []
    ) {
        $this->customerConfigSourceGroup = $customerConfigSourceGroup;
        parent::__construct(
            $context,
            $data
        );
    }

    public function setInputName( $value )
    {
        return $this->setName( $value );
    }

    public function _toHtml()
    {
        if ( !$this->getOptions() )
            {
                $options = $this->customerConfigSourceGroup->toOptionArray();
                foreach ( $options as $item )
                    {
                        if ( $item[ 'value' ] )
                            {
                                $value = $this->escapeJsQuote( $item[ 'value' ] );
                                $label =  $this->escapeJsQuote( $item[ 'label' ] );
                                $this->addOption( $value, $label );
                            }
                    }
            }
        return parent::_toHtml();
    }

}
