<?php
namespace Hotlink\Brightpearl\Model\Api\Service;

class Transport extends \Hotlink\Brightpearl\Model\Api\Transport
{
    protected $token;

    public function setToken( $token )
    {
        $this->token = $token;
        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getDevRef()
    {
        return \Hotlink\Brightpearl\Model\Platform::DEV_REF;
    }

    public function getAppRef()
    {
        return \Hotlink\Brightpearl\Model\Platform::APP_REF_M2;
    }

    protected function validate()
    {
        parent::validate();

        if ( !\Zend_Validate::is($this->getToken(), 'NotEmpty') ) {
            $this->_exceptionHelper()->throwTransport('Missing required [token]');
        }

        if (!\Zend_Validate::is($this->getDevRef(), 'NotEmpty')) {
            $this->_exceptionHelper()->throwTransport('Missing required [devRef]');
        }

        if (!\Zend_Validate::is($this->getAppRef(), 'NotEmpty')) {
            $this->_exceptionHelper()->throwTransport('Missing required [appRef]');
        }

        return $this;
    }

    protected function getHeaders(\Hotlink\Framework\Model\Api\Request $request)
    {
        return array_merge( parent::getHeaders($request),
                            array( 'brightpearl-instance-token: '.$this->getToken(),
                                   'brightpearl-dev-ref: '.$this->getDevRef(),
                                   'brightpearl-app-ref: '.$this->getAppRef()) ) ;
    }

}