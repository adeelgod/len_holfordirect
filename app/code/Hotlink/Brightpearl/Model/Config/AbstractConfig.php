<?php
namespace Hotlink\Brightpearl\Model\Config;

abstract class AbstractConfig extends \Hotlink\Framework\Model\Config\AbstractConfig
{
    /**
     * @var \Magento\Config\Model\Config
     */
    protected $configConfig;

    /**
     * @var \Hotlink\Brightpearl\Helper\Config\Backend
     */
    protected $brightpearlConfigBackendHelper;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Hotlink\Framework\Helper\Config\Field $configFieldHelper,

        \Magento\Config\Model\Config $configConfig,
        \Hotlink\Brightpearl\Helper\Config\Backend $brightpearlConfigBackendHelper,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        array $data = []
    ) {
        $this->configConfig = $configConfig;
        $this->encryptor = $encryptor;
        $this->brightpearlConfigBackendHelper = $brightpearlConfigBackendHelper;

        parent::__construct($storeManager, $scopeConfig, $configFieldHelper, $data);
    }

    protected function _getSection()
    {
        return 'hotlink_brightpearl';
    }

    protected function saveValue($value, $field, $storeId = null , $website = null, $inherit = false)
    {
        $this->configConfig
            ->setSection( $this->_getSection() )
            ->setWebsite( $website )
            ->setStore( $storeId )
            ->setGroups(
                array(
                     $this->_getGroup() => array(
                        'fields' => array(
                            $field => array(
                                'value' => $value,
                                'inherit' => $inherit
                            )
                        )
                    )
                )
            )->save();

        return $this;
    }

    protected function _unserialize( $value )
    {
        return $this->brightpearlConfigBackendHelper->unserialize( $value );
    }

}