<?php
namespace Hotlink\Brightpearl\Model\Interaction\Environment;

abstract class AbstractEnvironment extends \Hotlink\Framework\Model\Interaction\Environment\AbstractEnvironment
{

    protected $brightpearlConfigAuthorisation;

    public function __construct(
        \Hotlink\Framework\Helper\Exception $exceptionHelper,
        \Hotlink\Framework\Helper\Reflection $reflectionHelper,
        \Hotlink\Framework\Helper\Report $reportHelper,
        \Hotlink\Framework\Helper\Factory $factoryHelper,
        \Hotlink\Framework\Helper\Html\Form\Environment $htmlFormEnvironmentHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Hotlink\Framework\Model\Interaction\AbstractInteraction $interaction,

        \Hotlink\Brightpearl\Model\Config\Authorisation $brightpearlConfigAuthorisation,
        $storeId
    )
    {
        parent::__construct( $exceptionHelper,
                             $reflectionHelper,
                             $reportHelper,
                             $factoryHelper,
                             $htmlFormEnvironmentHelper,
                             $storeManager,
                             $interaction,
                             $storeId );

        $this->brightpearlConfigAuthorisation = $brightpearlConfigAuthorisation;

    }

    public function getAccountCode()
    {
        return $this->brightpearlConfigAuthorisation->getAccountCode( $this->getStoreId() );
    }

    public function getApiTimeout( $storeId = null )
    {
        return $this->getConfig()->getApiTimeout( $storeId );
    }

    public function getApiQueryLimit( $storeId = null )
    {
        return $this->_getApiConfig()->getQueryLimit( $storeId );
    }

    public function getAuthToken()
    {
        return $this->_getAuthConfig()->getToken( $this->getStoreId() );
    }

    protected function _getApiConfig()
    {
        return $this->factory()->get( '\Hotlink\Brightpearl\Model\Config\Api' );
    }

    protected function _getAuthConfig()
    {
        return $this->factory()->get( '\Hotlink\Brightpearl\Model\Config\Authorisation' );
    }
}
