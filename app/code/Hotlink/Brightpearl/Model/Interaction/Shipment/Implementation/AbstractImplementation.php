<?php
namespace Hotlink\Brightpearl\Model\Interaction\Shipment\Implementation;

abstract class AbstractImplementation extends \Hotlink\Framework\Model\Interaction\Implementation\AbstractImplementation
{

    protected $apiServiceSearchPeriod;
    protected $apiServiceSearchOrder;
    protected $apiServiceSearchWarehouseGoodsoutnote;
    protected $apiIdset;
    protected $apiServiceWarehouse;
    protected $apiServiceOrder;
    protected $orderService;
    protected $shipmentFactory;
    protected $transactionFactory;
    protected $lookupShippingMethodFactory;
    protected $shipmentTrackingFactory;
    protected $orderCollectionFactory;
    protected $dataObjectFactory;
    protected $notifierInterface;

    public function __construct(
        \Hotlink\Framework\Helper\Exception $exceptionHelper,
        \Hotlink\Framework\Helper\Reflection $reflectionHelper,
        \Hotlink\Framework\Helper\Report $reportHelper,
        \Hotlink\Framework\Helper\Factory $factoryHelper,

        \Magento\Sales\Model\Service\OrderService $orderService,  // TODO: lonesome

        \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Hotlink\Brightpearl\Helper\Api\Service\Search\Period $apiServiceSearchPeriod,
        \Hotlink\Brightpearl\Helper\Api\Service\Search\Order $apiServiceSearchOrder,
        \Hotlink\Brightpearl\Helper\Api\Service\Search\Warehouse\GoodsoutNote $apiServiceSearchWarehouseGoodsoutnote,
        \Hotlink\Brightpearl\Helper\Api\Service\Order $apiServiceOrder,
        \Hotlink\Brightpearl\Helper\Api\Idset $apiIdset,
        \Hotlink\Brightpearl\Helper\Api\Service\Warehouse $apiServiceWarehouse,
        \Hotlink\Brightpearl\Model\Lookup\Shipping\MethodFactory $lookupShippingMethodFactory,
        \Hotlink\Brightpearl\Model\ShipmentFactory $shipmentTrackingFactory,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Sales\Model\Order\Shipment\NotifierInterface $notifierInterface
    )
    {
        parent::__construct( $exceptionHelper, $reflectionHelper, $reportHelper, $factoryHelper );

        $this->orderService = $orderService;
        $this->shipmentFactory = $shipmentFactory;
        $this->transactionFactory = $transactionFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->apiServiceSearchPeriod = $apiServiceSearchPeriod;
        $this->apiServiceSearchOrder = $apiServiceSearchOrder;
        $this->apiServiceSearchWarehouseGoodsoutnote = $apiServiceSearchWarehouseGoodsoutnote;
        $this->apiServiceOrder = $apiServiceOrder;
        $this->apiIdset = $apiIdset;
        $this->apiServiceWarehouse = $apiServiceWarehouse;
        $this->lookupShippingMethodFactory = $lookupShippingMethodFactory;
        $this->shipmentTrackingFactory = $shipmentTrackingFactory;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->notifierInterface = $notifierInterface;
    }

    protected function apiSearchOrders( $lookbehindDate, $pageSize, $first )
    {
        $report = $this->getReport();

        $date = new \DateTime( $lookbehindDate, new \DateTimeZone( 'UTC' ) );
        $updatedOn = $this->apiServiceSearchPeriod->toAfter( $date );

        $report->info( 'Searching for orders where updatedOn = ' . $updatedOn )->indent();

        $result = $this->_apiSearch( $this->apiServiceSearchOrder,
                                     array( 'updatedOn' => $updatedOn ),
                                     $pageSize,
                                     $first,
                                     'updatedOn',
                                     'DESC' );
        $report->unindent();

        return $result;
    }

    protected function apiSearchGoodsOutNotes( $lookbehindDate, $pageSize, $first )
    {
        $report = $this->getReport();

        $env     = $this->getEnvironment();
        $storeId = $env->getStoreId();
        $config  = $env->getConfig();
        $channel = $config->getChannel( $storeId );

        $date = new \DateTime( $lookbehindDate, new \DateTimeZone( 'UTC' ) );
        $createdOn = $this->apiServiceSearchPeriod->toAfter( $date );

        $report->info( 'Searching for goods-out notes where createdOn= '.$createdOn.', and channel = '.$channel )->indent();

        $result = $this->_apiSearch( $this->apiServiceSearchWarehouseGoodsoutnote,
                                     array( 'createdOn' => $createdOn,
                                            'channel'   => $channel ),
                                     $pageSize,
                                     $first,
                                     $config->getSortBy( $storeId ),
                                     $config->getSortDirection( $storeId ) );

        $report->unindent();

        return $result;
    }

    protected function _apiSearch( $api, $filters, $pageSize, $first, $sortBy, $sortDirection )
    {
        $report = $this->getReport();

        $env     = $this->getEnvironment();
        $config  = $env->getConfig();

        $storeId = $env->getStoreId();
        $account = $env->getAccountCode();

        $report->debug( "pageSize=$pageSize, firstResult=$first" );

        $result = $report($api, 'search', $storeId, $account, $filters, $pageSize, $first, $sortBy, $sortDirection);

        $pag  = $result->getPagination();
        $nr   = $pag->getData('resultsReturned');
        $more = $pag->getData('morePagesAvailable') ? 'Yes' : 'No';

        $report->debug( "results returned=$nr, more available=$more" );

        return $result;
    }


    protected function apiGetOrderNotes( $noteType, $orderId, $noteId )
    {
        $report      = $this->getReport();

        $orderIdSet = is_array( $orderId )
            ? $this->apiIdset->unorderedList( $orderId )
            : ( is_null( $orderId )
                ? \Hotlink\Brightpearl\Helper\Api\Idset::ALL
                : $this->apiIdset->single( $orderId ) );

        $noteIdSet = is_array( $noteId )
            ? $this->apiIdset->unorderedList( $noteId )
            : ( is_null( $noteId )
                ? null
                : $this->apiIdset->single( $noteId ) );

        $report->info( "Fetching order [$orderIdSet] shipping notes [$noteIdSet]" )->indent();

        $notes = $report( $this->apiServiceWarehouse,
                          $this->getNoteApiMethod( $noteType ),
                          $this->getEnvironment()->getStoreId(),
                          $this->getEnvironment()->getAccountCode(),
                          $orderIdSet,
                          $noteIdSet );
        $nr = count($notes);
        $report->debug( "results returned=$nr" )->unindent();

        return ( $notes )
            ? ( ( is_scalar($noteId) && ( is_null($orderId) || is_scalar($orderId) ) )
                ? array_shift($notes)
                : $notes )
            : null;
    }

    protected function apiGetOrder( $orderId )
    {
        $report      = $this->getReport();

        $idSet = is_array( $orderId )
            ? $this->apiIdset->unorderedList( $orderId )
            : $this->apiIdset->single( $orderId );

        $report->info( "Fetching orders [$idSet]" )->indent();

        $orders = $report( $this->apiServiceOrder,
                           'getOrders',
                           $this->getEnvironment()->getStoreId(),
                           $this->getEnvironment()->getAccountCode(),
                           $idSet );

        $nr = count($orders);
        $report->debug( "results returned=$nr" )->unindent();

        return ( $orders )
            ? ( is_array( $orderId )
                ? $orders
                : array_shift($orders) )
            : null;
    }

    protected function importNotes( $noteType,
                                    array $bpNotes,
                                    array $bpOrders,
                                    \Magento\Sales\Model\ResourceModel\Order\Collection $mageOrders,
                                    $sleep = 0 )
    {
        $report = $this->getReport();

        $report->info( "Start importing $noteType notes")->indent();

        $config = $this->getEnvironment()->getConfig();

        foreach ($bpNotes as $noteId => $note) {
            $bpOrder = isset($bpOrders[ $note->getData('orderId') ]) ? $bpOrders[ $note->getData('orderId') ] : null;

            if ($bpOrder and $mageOrder = $mageOrders->getItemByColumnValue('increment_id', $bpOrder->getData('externalRef'))) {

                $trackingRecord = $this->shipmentTrackingFactory->create();
                $trackingRecord->setBrightpearlId( $noteId );
                $trackingRecord->setShipmentType( $noteType  );

                $notify = $config->getNotifyCustomer( $mageOrder->getStoreId() );
                $this->importNote( $noteId, $trackingRecord, $note, $bpOrder, $mageOrder, $notify, $sleep );
            }
        }

        $report->unindent();
    }

    protected function importNote( $noteId,
                                   \Hotlink\Brightpearl\Model\Shipment $trackingRecord,
                                   \Hotlink\Brightpearl\Model\Platform\Data $bpNote,
                                   \Hotlink\Brightpearl\Model\Platform\Data $bpOrder,
                                   \Magento\Sales\Model\Order$mageOrder,
                                   $notify,
                                   $sleep = 0 )
    {
        $report = $this->getReport();

        $report->info('Importing note ['.$noteId.'],'.
                      ' bp order ['.$bpOrder->getId().'],'.
                      ' magento order ['.$mageOrder->getIncrementId().']')
            ->indent();

        if ($mageOrder->canShip()) {
            $qtys = $this->mapQtys($bpNote, $bpOrder, $mageOrder);

            if ($qtys) {

                $tracking = $this->getTrackingInfo( $bpNote, $mageOrder->getStoreId() );
                $tracking = ( count( $tracking ) == 0 ) ? null : [ $tracking ];
                $shipment = $this->shipmentFactory->create( $mageOrder, $qtys, $tracking );

                $success = $this->saveShipment( $shipment, $notify );

                if ($success) {
                    $this->saveTracking($shipment, $trackingRecord);
                }

                if ( $sleep > 0 ) {
                    usleep($sleep);
                }
            }
            else {
                $report->debug('No qtys to create shipment from');
            }
        }
        else {
            $report->debug("Magento does not allow this order to be shipped. canShip returned false!");
        }

        $report->unindent();
    }

    protected function mapQtys( \Hotlink\Brightpearl\Model\Platform\Data $note,
                                \Hotlink\Brightpearl\Model\Platform\Data $bpOrder,
                                \Magento\Sales\Model\Order $mageOrder )
    {
        $report = $this->getReport();
        $report->info('Preparing shippment qtys')->indent();

        $bpMageOrderItemMap = $this->getBpMageOrderItemMap($bpOrder, $mageOrder);

        $shipmentQtys = array();
        foreach ($note->getData('orderRows') as $_bpOrderRowId => $_rowBatches) {

            if (!isset($bpMageOrderItemMap[$_bpOrderRowId])) {
                continue;
            }

            $map          = $bpMageOrderItemMap[$_bpOrderRowId];
            $orderItem    = $map->getMageOrderItem();
            $orderItemId  = $orderItem->getId();
            $orderItemSku = $orderItem->getSku();
            $qtyAvailable = $orderItem->getQtyToShip();

            $report
                ->debug('Processing: BP item id=['.$_bpOrderRowId.']; '.
                        'Magento item id=['.$orderItemId.'], sku=['.$orderItemSku.']')->indent();

            foreach ($_rowBatches as $_batch) {

                $qtyToShip = $_batch->getQuantity();

                if ( $bpParentRowId = $map->getBpParentRowId() ) {  // bundle product

                    $parentMap           = $bpMageOrderItemMap[$bpParentRowId];
                    $mageParentOrderItem = $parentMap->getMageOrderItem();

                    if ( !$this->isShipmentSeparately( $mageParentOrderItem ) ) {
                        $qtyAvailable = $mageParentOrderItem->getQtyToShip();
                        $orderItemId  = $mageParentOrderItem->getId();

                        $report->debug('Item cannot be shipped separately due to bundle shipping settings = "together".'.
                                       ' Shipping parent item with id ['.$orderItemId.'] instead!');
                    }
                }

                $accumulated  = isset( $shipmentQtys[$orderItemId] ) ? $shipmentQtys[$orderItemId] : 0;
                $qtyAvailable = max(0, $qtyAvailable - $accumulated);

                if ($qtyAvailable == 0) {
                    $report->debug('Skipping, available qty to ship is 0 (zero).');
                }
                else {
                    $minToShip = min( $qtyAvailable, $qtyToShip );

                    if ($qtyAvailable < $qtyToShip) {
                        $report->debug('BP item qty ['.$qtyToShip.'] is grater than Magento item qty available ['.
                                       $qtyAvailable.'], therefore minimum qty ['.$minToShip.'] is considered!');
                    }

                    $shipmentQtys[ $orderItemId ] = $accumulated + $minToShip;

                    $report->debug('Mapped qty = '.$minToShip);
                }

                $report->unindent();
            }
        }

        $report->unindent();

        return $shipmentQtys;
    }

    protected function saveShipment( \Magento\Sales\Model\Order\Shipment $shipment, $sendEmail )
    {
        $report = $this->getReport();
        $order  = $shipment->getOrder();

        $shipment->register();
        // https://github.com/magento/magento2/issues/4320
        $shipment->setEmailSent($sendEmail);

        $order->setCustomerNoteNotify($sendEmail);

        $transaction = $this->transactionFactory->create();
        $transaction->addObject($shipment);
        $transaction->addObject($order);

        $error = false;
        try {

            $transaction->save();

            $report->info('Shipment was created successfully')->incsuccess();
        }
        catch ( \Exception $e) {
            $error = true;
            $report->error('There was a problem creating the shipment: '. $e->getMessage())->incfail();
        }

        if ($sendEmail && !$error) {
            $this->notifierInterface->notify($order, $shipment, null);
        }

        return !$error;
    }

    protected function saveTracking( \Magento\Sales\Model\Order\Shipment $shipment,
                                     \Hotlink\Brightpearl\Model\Shipment $trackingRecord)
    {
        $report = $this->getReport();

        $trackingRecord->setShipmentId( $shipment->getId() );
        $trackingRecord->setCreatedAt( gmdate( \Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT ) );

        try {
            $trackingRecord->save();
            $report->debug( 'Shipment tracking record was created successfully' );
        }
        catch ( \Exception $e ) {
            $report->error( 'There was a problem creating the shipment tracking record: '. $e->getMessage() );
        }
    }

    protected function getTrackingInfo( \Hotlink\Brightpearl\Model\Platform\Data $note, $storeId )
    {
        $report = $this->getReport();

        $tracking = [];

        $report->info( 'Adding tracking information' )->indent();

        $bpShippingMethodId = $note->getShipping()->getData('shippingMethodId');

        if ( $carierCode = $this->getMagentoCarrier($bpShippingMethodId, $storeId) ) {
            $bpShippingMethod = $this->lookupShippingMethodFactory->create()->load($bpShippingMethodId, 'brightpearl_id');

            if ( $bpShippingMethod->getId() ) {

                $title  = $bpShippingMethod->getCode() . ' - ' . $bpShippingMethod->getName();
                $number = $note->getShipping()->getData('reference');

                if ( $number ) {
                    $report->debug('code=['.$carierCode.'], title=['.$title.'], ref=['.$number.']');

                    $tracking =  [ 'carrier_code' => $carierCode,
                                   'title'        => $title,
                                   'number'       => $number ];
                }
                else {
                    $report->warn('Shipping is missing "reference" field ');
                }
            }
            else {
                $report->warn('BP shipping method with id ['.$bpShippingMethodId.'] was not found in the database');
            }
        }
        else {
            $report->debug('Tracking not created');
        }

        $report->unindent();

        return $tracking;
    }

    protected function getBpMageOrderItemMap( \Hotlink\Brightpearl\Model\Platform\Data $bpOrder,
                                              \Magento\Sales\Model\Order $mageOrder )
    {
        $report             = $this->getReport();
        $bpOrderRows        = $bpOrder->getData('orderRows');
        $bpMageOrderItemMap = array();

        foreach ($bpOrderRows as $bpOrderRowId => $bpOrderRow) {
            if ( $extRef = $bpOrderRow->getData('externalRef') ) {
                if ( $mageOrderItem = $mageOrder->getItemById($extRef) ) {

                    $map = $this->dataObjectFactory->create();

                    $map->setMageOrderItem($mageOrderItem);

                    $composition = $bpOrderRow->getData('composition');
                    $parentRowId = isset($composition['parentOrderRowId'])
                        ? $composition['parentOrderRowId']
                        : null;

                    $map->setBpParentRowId($parentRowId);

                    $bpMageOrderItemMap[$bpOrderRowId] = $map;
                }
                else {
                    $report->warn('Order item with id ['.$extRef.'] was not found in Magento');
                }
            }
            else {
                $report->warn('BP Order Item with id ['.$bpOrderRowId.'] is missing externalRef field');
            }
        }

        return $bpMageOrderItemMap;
    }

    protected function isShipmentSeparately( \Magento\Sales\Model\Order\Item $item)
    {
        if ($options = $item->getProductOptions()) {
            return (
                isset($options['shipment_type']) and
                $options['shipment_type'] == \Magento\Catalog\Model\Product\Type\AbstractType::SHIPMENT_SEPARATELY
                );
        }
    }

    protected function loadMagentoOrder( $incrementId  )
    {
        $report = $this->getReport();

        $info = is_array( $incrementId )
            ? implode( ',', $incrementId )
            : $incrementId;

        $report->info('Loading Magento orders ['.$info.']')->indent();

        $orders = $this->orderCollectionFactory->create()
            ->addFieldToFilter( 'increment_id', array( 'in' => $incrementId ) )
            ->load();

        $nr = $orders->count();
        $report->debug( "results found=$nr" )->unindent();

        return ( $nr > 0 )
            ? ( is_array( $incrementId )
                ? $orders
                : $orders->getItemByColumnValue( 'increment_id', $incrementId ) )
            : null;
    }

    protected function getMagentoCarrier( $bpShippingMethodId, $storeId )
    {
        $carrier = null;
        $map     = $this->getEnvironment()->getConfig()->getShippingOptionsMap($storeId);

        foreach ($map as $row) {

            $brightpearl = isset($row['brightpearl']) ? $row['brightpearl'] : null;
            $magento     = isset($row['magento'])     ? $row['magento']     : null;

            if ( $bpShippingMethodId == $brightpearl ) {
                $carrier = $magento;
                break;
            }
        }

        if ( is_null( $carrier ) )
            {
                $this->getReport()->warn( 'No Magento carrier found for BP shipping method [' . $bpShippingMethodId . ']' );
            }

        return $carrier;
    }

    protected function getNoteApiMethod( $noteType )
    {
        $method = null;

        switch ($noteType) {
        case \Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Shipment\Type::GOODS_OUT:
            $method = 'getGoodsoutNote';
            break;
        case \Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Shipment\Type::DROP_SHIP:
            $method = 'getDropshipNote';
            break;
        }

        return $method;
    }

    protected function shipAllOrderItems( \Magento\Sales\Model\Order $mageOrder, $notify )
    {
        $report = $this->getReport();

        $qtys = array();
        foreach ( $mageOrder->getAllItems() as $item ) {

            if ( $toShip = $item->getQtyToShip() ) {
                $qtys[ $item->getId() ] = $toShip;
            }
        }

        if ($qtys) {
            $report->debug( 'creating shipment for all remaining items and qtys' );
            $shipment = $this->shipmentFactory->create( $mageOrder, $qtys );
            $this->saveShipment( $shipment, $notify );
        }

        return $this;
    }

    protected function checkNote( \Hotlink\Brightpearl\Model\Platform\Data $note )
    {
        $report = $this->getReport();
        $valid  = true;

        $isTransfer = $note->getData('transfer') == true;
        $isShipped = false;
        if ( $status = $note->getData('status') )
            {
                $isShipped = $status->getData('shipped') == true;
            }

        if ( $isTransfer ) {
            $report->debug("Note rejected, internal transfer");
            $valid = false;
        }
        else if ( !$isShipped ) {
            $report->debug("Note rejected, not shipped yet");
            $valid = false;
        }

        return $valid;
    }

    protected function platformDataColumn( $input, $columnKey, $indexColumn = null )
    {
        if ( is_null($input) ) return $input;

        $result = array();
        foreach ($input as $data) {
            $value = $this->_dataColumn( $data, $columnKey );
            if ( $indexColumn ) {
                $result[ $this->_dataColumn( $data, $indexColumn ) ] = $value;
            } else {
                $result[] = $value;
            }
        }
        return $result;
    }

    protected function _dataColumn( $data, $key )
    {
        if ( is_array( $key ) ) {
            $current = array_shift($key);
            if (!$key) {
                return $this->_dataColumn( $data, $current );
            }
            return $this->_dataColumn( $data->getData( $current ), $key ); // rec
        }
        return $data->getData( $key );
    }

}
