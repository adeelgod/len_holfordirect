<?php
namespace Hotlink\Brightpearl\Model\Interaction\Prices\Import;

class Implementation extends \Hotlink\Framework\Model\Interaction\Implementation\AbstractImplementation
{

    protected $storeManager;
    protected $catalogProductFactory;
    protected $productCollectionFactory;
    protected $catalogHelper;
    protected $apiServiceWorkflowHelper;
    protected $clockHelper;

    public function __construct(
        \Hotlink\Framework\Helper\Exception $exceptionHelper,
        \Hotlink\Framework\Helper\Reflection $reflectionHelper,
        \Hotlink\Framework\Helper\Report $reportHelper,
        \Hotlink\Framework\Helper\Factory $factoryHelper,

        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $catalogProductFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Helper\Data $catalogHelper,
        \Hotlink\Brightpearl\Helper\Api\Service\Workflow $apiServiceWorkflowHelper,
        \Hotlink\Framework\Helper\Clock $clockHelper
    )
    {
        $this->storeManager = $storeManager;
        $this->catalogProductFactory = $catalogProductFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogHelper = $catalogHelper;
        $this->apiServiceWorkflowHelper = $apiServiceWorkflowHelper;
        $this->clockHelper = $clockHelper;

        parent::__construct(
            $exceptionHelper,
            $reflectionHelper,
            $reportHelper,
            $factoryHelper );
    }

    protected function _getName()
    {
        return 'Hotlink Brightpearl Prices Import';
    }

    //
    //  This interaction is usually invoked from the admin store (via admin UI, or cron), therefore the environment
    //  storeid is usually zero. In order for cron to execute the interaction and trigger must be enabled for Admin.
    //  Since Admin is therefore always enabled at runtime, Admin always resides in the list of websites to process.
    //  Running price import at Admin level is therefore obligatory.
    //
    //  When "General/Catalog/Catlog/Price/Catalog Price Scope" is global, the implementation updates the global price
    //  in the Admin store, but also still updates prices each website, however the data is not used by Magento.
    //
    //  When "General/Catalog/Catlog/Price/Catalog Price Scope" is website, the implementation behaves the same, but
    //  now Magento uses the extra data.
    //
    public function execute()
    {
        $environment = $this->getEnvironment();
        $report = $this->getReport();
        $report( $environment, "status" );

        $skipAttributes = $environment->getParameter( 'skip_attributes' )->getValue();
        $skipTier       = $environment->getParameter( 'skip_tier' )->getValue();

        if ( $skipAttributes && $skipTier )
            {
                $report->warn( "No activities selected" );
            }
        else
            {
                $scopes = $environment->getParameter( 'websites' )->getValue();
                $skuFilter = $environment->getParameter( 'skus' )->getValue();
                if ( count( $scopes ) )
                    {
                        foreach ( $scopes as $scopeCode => $websites )
                            {
                                foreach ( $websites as $websiteCode => $websiteId )
                                    {
                                        $this->process( $websiteId, $skipAttributes, $skipTier, $skuFilter );
                                    }
                            }
                    }
            }
    }

    public function process( $websiteId, $skipAttributes, $skipTier, $skuFilter )
    {
        $report = $this->getReport();
        $idle = 0;
        $website      = $this->storeManager->getWebsite( $websiteId );
        $websiteName  = $website->getName();
        $defaultStore = $website->getDefaultStore();
        $storeId      = ( $defaultStore ? $defaultStore->getId()   : null );
        $storeName    = ( $defaultStore ? $defaultStore->getName() : null );

        if ( is_null( $storeId ) )
            {
                $report->warn( "Skipping, no default store found for website $websiteName [$websiteId]" );
                return;
            }

        if ( ! $this->getInteraction()->getConfig()->isEnabled( $storeId ) )
            {
                $report->warn( "Skipping, interaction disabled for website $websiteName [$websiteId]" );
                return;
            }

        // new env specific for current website. correct scoping is crucial for this interaction.
        $environment = $this->getOrCreateEnvironment( $storeId );
        if ( !$environment->isEnabled() )
            {
                $report->warn( "Skipping, interaction disabled for website $websiteName [$websiteId]" );
                return;
            }
        $trigger = $this->getInteraction()->getTrigger();
        if ( !$environment->isTriggerEnabled( $trigger ) )
            {
                $triggerName = $trigger->getName();
                $report->warn( "Skipping, interaction trigger [$triggerName] disabled for website $websiteName [$websiteId]" );
                return;
            }
    
        $report->info( "Processing for website $websiteName [$websiteId] and store $storeName [$storeId]" );

        $batch = $environment->getBatch();
        $bundleBasePriceList = $environment->getBasePriceList();

        $attributePriceMapping = $environment->getPriceAttributeMapping();
        if ( !$attributePriceMapping )
            {
                $report->debug( "No attribute mapping found" );
            }
        $tierPriceMapping = $environment->getTierPriceListMap();
        if ( !$tierPriceMapping )
            {
                $report->debug( "No tier price mapping found" );
            }

        $importAttrPrices = !$skipAttributes && $attributePriceMapping;
        $importTierPrices = !$skipTier       && $tierPriceMapping;

        if ( !$importAttrPrices && !$importTierPrices )
            {
                $report->debug( 'Skipping, there is nothing to do.' );
                return;
            }

        // Unset all cached attributes before starting.
        // This goes hand in hand with partial saving and
        // 'Attribute mapping' having (potentially) different values per website.
        $this->catalogProductFactory->create()->getResource()->unsetAttributes( null );

        //
        // build up collection
        //
        $collection = $this->productCollectionFactory->create()
                    ->setStoreId( $storeId )        // causes attributes to load from the correct store
                    ->addAttributeToSort( 'sku' )

                    // ISSUE:
                    // Even though the module [Hotlink Brightpearl] only loads the necessary attributes
                    // and also marks the save partial, there is a Magento Framework observer
                    // \Magento\Framework\EntityManager\Observer\AfterEntitySave which loads all attributes (!!!),
                    // which causes the next observer
                    // \Magento\CatalogUrlRewrite\Observer\ProductProcessUrlRewriteSavingObserver::execute
                    // to identify that 'visibility' has changed because original_data[visibility] != data[visibility].
                    // Because 'visibility' has changed, it [the second observer] triggers a url_rewrite re-calculation
                    // which creates a duplicate url-key which throws an exception "URL key for specified store already exists.",
                    // causing the save to halt.

                    // FIX:
                    // Load visibility attribute which makes it available in 'original_data' and therefore
                    // the second observer \Magento\CatalogUrlRewrite\Observer\ProductProcessUrlRewriteSavingObserver::execute
                    // does not see it as changed.

                    // IMPORTANT:
                    // When this issues comes back, open the second observer,
                    // \Magento\CatalogUrlRewrite\Observer\ProductProcessUrlRewriteSavingObserver::execute and check
                    // if Magento has changed the if condition in 'execute' function. If the code checks for a different
                    // attribute add it to select.

                    ->addAttributeToSelect( 'visibility' );

        if ( $prodTypes = $environment->getProductTypes() )
            {
                $report->debug( "Filtering products by type: [". implode( ",", $prodTypes ) ."]" );
                $collection->addFieldToFilter( 'type_id', [ 'in' => $prodTypes ] );
            }
        else
            {
                $report->warn( "Skipping, No product type(s) selected" );
                return;
            }

        if ( $websiteId != $this->getAdminWebsiteId() )
            {
                $collection->addWebsiteFilter( $websiteId );
            }

        if ( $skuFilter )
            {
                if ( count( $skuFilter ) > 0 )
                    {
                        $collection->addFieldToFilter( "sku", array( 'in' => $skuFilter ) );
                    }
            }

        if ( $importAttrPrices )
            {
                foreach ( $attributePriceMapping as $map )
                    {
                        $code = $map[ \Hotlink\Brightpearl\Model\Config\Field\Price::ATTRIBUTE_CODE ];
                        $collection->addAttributeToSelect( $code ); // only load selected attributes
                    }
                $collection->addAttributeToSelect( 'price_type' ); // needed for bundle products
            }

        //
        // collect price lists across config maps
        //
        $priceLists = $this->collectPriceLists( $attributePriceMapping, $tierPriceMapping, $bundleBasePriceList, $website );

        //
        // pagination
        //
        $collection->setPageSize( $batch );
        $pages = ( $collection->getSize() > 0 ) ? $collection->getLastPageNumber() : 0;
        if ( $pages == 0 )
            {
                $report->debug( "No products to process" );
                return;
            }

        $currentPage = 0;
        while ( ++$currentPage <= $pages )
            {
                $report
                    ->debug( "Processing batch $currentPage of $pages" )
                    ->setBatch( $currentPage )
                    ->indent();
                $collection
                    ->clear()
                    ->setCurPage( $currentPage )
                    ->addWebsiteNamesToResult()
                    ->load();

                $skus = $collection->walk( 'getSku' );
                $report->addReference( $skus );

                if ( $apiPriceLists = $this->_apiFetchPriceLists( $environment, $priceLists, $skus ) )
                    {
                        if ( $this->_checkPriceListHeader( $apiPriceLists, $website ) )
                            {
                                foreach ( $collection as $product )
                                    {
                                        $report->info( 'Processing sku ['. $product->getSku() . ']' )->indent();
                                        $product->setStoreId( $storeId );
                                        $attrPricesChanged = false;
                                        $tierPricesChanged = false;
                                        //
                                        //  Attribute price update
                                        //
                                        if ( $importAttrPrices )
                                            {
                                                $attrPricesChanged = $this->applyAttributePriceUpdates(
                                                    $website,
                                                    $product,
                                                    $attributePriceMapping,
                                                    $apiPriceLists,
                                                    $bundleBasePriceList );
                                            }
                                        //
                                        //  Tier price update
                                        //
                                        if ( $importTierPrices )
                                            {
                                                $tierPricesChanged = $this->applyTierPriceUpdates(
                                                    $website,
                                                    $product,
                                                    $tierPriceMapping,
                                                    $apiPriceLists,
                                                    $bundleBasePriceList );
                                            }
                                        // only save product if necessary
                                        if ( $attrPricesChanged || $tierPricesChanged )
                                            {
                                                $idle += $this->_saveProduct($product);
                                            }
                                        else
                                            {
                                                $report->debug( 'Product save not needed.' );
                                            }
                                        $report->unindent();
                                    }
                            }
                        else
                            {
                                $report->debug( 'Processing halted due to incompatibilities between pricelist(s) and website' )->unindent();
                                break;
                            }
                    }
                else
                    {
                        $report->debug( 'No prices returned from Brightpearl API' );
                    }
                $report->unindent();
            }
        if ( $idle > 0 )
            {
                $report->trace( 'Idle for ' . $idle . ' seconds.' );
            }
    }

    protected function collectPriceLists( array $attrMap,
                                          array $tierMap,
                                          $bundleBasePriceList,
                                          \Magento\Store\Model\Website $website )
    {
        $websiteId = $website->getId();
        $priceLists = array();

        foreach ( $attrMap as $_map )
            {
                $priceLists[] = $_map[ \Hotlink\Brightpearl\Model\Config\Field\Price::PRICE_LIST ];
            }

        foreach ( $tierMap as $_item )
            {
                $mwid  = $_item[ \Hotlink\Brightpearl\Model\Config\Field\Price::WEBSITE ];
                $match = ( $mwid == $websiteId || $this->getAdminWebsiteId() ); // [ALL WEBSITES]
                if ( $match )
                    {
                        $priceLists[] = $_item[ \Hotlink\Brightpearl\Model\Config\Field\Price::PRICE_LIST ];
                    }
            }
        $priceLists[] = $bundleBasePriceList;
        return array_unique( $priceLists );
    }

    protected function applyAttributePriceUpdates(\Magento\Store\Model\Website $website,
                                                  \Magento\Catalog\Model\Product $product,
                                                  array $configMap,
                                                  array $apiPriceLists,
                                                  $basePriceList)
    {
        $report    = $this->getReport();
        $isBundle  = ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE);
        $sku       = $product->getSku();
        $resource  = $product->getResource();
        // $pWebsites = $product->getWebsites();  // M1
        $pWebsites = $product->getWebsiteIds(); // M2
        $updates   = array();

        $report->info("Applying attribute price updates")->indent();

        $wid = $website->getId();
        if ($wid != $this->getAdminWebsiteId() && !in_array($wid, $pWebsites)) {
            // do not process product for website if product is not assigned to website, but allow admin website
            $report->debug("not assigned to website [$wid]");
            return;
        }

        $noPriceInList = array();
        foreach ($configMap as $map) {
            $attrCode   = $map[\Hotlink\Brightpearl\Model\Config\Field\Price::ATTRIBUTE_CODE];
            $attr       = $resource->getAttribute($attrCode);
            $listId     = $map[\Hotlink\Brightpearl\Model\Config\Field\Price::PRICE_LIST];
            $behaviour  = $map[\Hotlink\Brightpearl\Model\Config\Field\Price::BEHAVIOUR];
            $currValue  = $product->getData($attrCode);
            $clearValue = $behaviour == \Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Price\Missing\Behaviour::BEHAVIOUR_CLEAR;
            $price = $this->_getSkuSinglePrice($apiPriceLists, $listId, $sku);
            if ($price) {

                if ($isBundle) {
                    // only update price if bundle price type is fixed
                    if ($attrCode === 'price' && $product->getPriceType() == \Magento\Bundle\Model\Product\Price::PRICE_TYPE_FIXED) {
                        $updates[] = array('code' => $attrCode, 'value' => $price);
                    }

                    if ($attrCode === 'special_price') {
                        $percent = $this->getPercent($price, $apiPriceLists, $basePriceList, $sku);
                        if ($percent) {
                            $updates[] = array( 'code' => $attrCode, 'value' => $percent);
                        }
                    }
                }
                else {
                    $updates[] = array( 'code' => $attrCode, 'value' => $price);
                }
            }
            else {
                $noPriceInList[] = $listId;

                if ($clearValue && $currValue) { // clear value but only if there is a current value
                    $updates[] = array( 'code' => $attrCode, 'value' => '[cleared]');
                    if ($attr->getIsRequired()) {
                        $report->warn("cleared value of required attribute [$attrCode]");
                    }
                }
            }
        }

        if ($noPriceInList) {
            $info = implode( '], [', array_unique($noPriceInList) );
            $report->debug( "no price found in list(s): [$info]" );
        }

        if (!empty($updates)) {

            $message = '';
            foreach ($updates as $changed) {
                $attr = $changed['code'];
                $val  = $changed['value'];

                $message .= $attr . '=' . $val .'; ';

                if ($val === '[cleared]') {
                    $val = null;
                }

                $product->setData($attr, $val);
                $resource->getAttribute($attr);
            }

            $report->debug("mapped $message");
        }
        else {
            $report->debug("no attributes mapped");
        }

        $report->unindent();

        return !empty($updates);
    }

    protected function applyTierPriceUpdates(\Magento\Store\Model\Website $website,
                                             \Magento\Catalog\Model\Product $product,
                                             array $tierMap,
                                             array $apiPriceLists,
                                             $basePriceList)
    {
        $sku         = $product->getSku();
        $isBundle    = ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE);
        $report      = $this->getReport();
        $priceGlobal = $this->catalogHelper->isPriceGlobal();
        $resource    = $product->getResource();
        $wid         = $website->getId();

        $report->info("Applying tier price updates")->indent();

        $allTierPrices  = array();
        $websiteTierPrices = array();
        $noPriceInList = array();
        foreach ($tierMap as $_map) {
            $websiteId = ($priceGlobal
                          ? $this->getAdminWebsiteId()
                          : $_map[\Hotlink\Brightpearl\Model\Config\Field\Price::WEBSITE]);
            $custGroup = $_map[\Hotlink\Brightpearl\Model\Config\Field\Price::CUSTOMER_GROUP];
            $listId    = $_map[\Hotlink\Brightpearl\Model\Config\Field\Price::PRICE_LIST];
            $break     = $_map[\Hotlink\Brightpearl\Model\Config\Field\Price::PRICE_BREAK];
            $excBrk1   = ($break == \Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Price\Tier\BreakTier::EXCLUDE_BREAK_1);

            $prices = $this->_getSkuMultiPrice($apiPriceLists, $listId, $sku);
            if ($prices) {
                foreach ($prices as $qty => $price) {

                    if ($qty === 1 && $excBrk1) continue;

                    $tierPrice = null;

                    if ($isBundle) {
                        if ($discountPercent = $this->getDiscountPercent($price, $apiPriceLists, $basePriceList, $sku)) {
                            $tierPrice = array( 'price'       => $discountPercent,
                                                 'website_id'  => $websiteId,
                                                 'cust_group'  => $custGroup,
                                                 'price_qty'   => $qty,
                                                 'delete'      => '' );
                        }
                    }
                    else {
                        $tierPrice = array( 'price'       => $price,
                                            'website_id'  => $websiteId,
                                            'cust_group'  => $custGroup,
                                            'price_qty'   => $qty,
                                            'delete'      => '' );
                    }

                    if (!is_null($tierPrice)) {
                        $allTierPrices[] = $tierPrice;

                        if ($websiteId == $wid || $priceGlobal) {
                            $websiteTierPrices[] = $tierPrice;
                        }
                    }
                }
            }
            else {
                $noPriceInList[] = $listId;
            }
        }

        if ($noPriceInList) {
            $info = implode( '], [', array_unique($noPriceInList) );
            $report->debug( "no price found in list(s): [$info]" );
        }

        // set product on edit mode so that getTierPrice() does not fallback to admin
        // prices in case there is no value for the desired website
        // @see Mage_Catalog_Model_Product_Attribute_Backend_Groupprice_Abstract::afterLoad
        $product->setData('_edit_mode', true);

        // store existing tier prices to determine if save is needed
        $currentTierPrices = $product->getTierPrice();

        // unset previously set price, to force saving prices for desired website Id
        $product->setTierPrice(null);
        // let the resource load the attribute
        $product->getTierPrice();

        // validate price for scope GLOBAL
        $product->setTierPrice($allTierPrices);

        $valid = true;
        $changes = false;

        try {
            $resource->getAttribute('tier_price')->getBackend()->validate($product);
        }
        catch ( \Exception $e) {
            $report->error("tier price validation failed - ".$e->getMessage());

            $product->getResource()->unsetAttributes('tier_price');
            $product->setTierPrice(false); // do not change tier prices when validation fails

            $valid = false;
        }

        if ($valid) {

            $changes = ($currentTierPrices != $websiteTierPrices);

            if ( $changes ) {
                // save prices for CURRENT website scope
                $product->setTierPrice($websiteTierPrices);

                if (empty($websiteTierPrices)) {
                    $report->debug("cleared tier prices");
                }
                else {
                    $updates = json_encode($websiteTierPrices);
                    $report->debug("mapped tier prices - ".$updates);
                }
            }
            else {
                $report->debug("no tier prices updated");
            }
        }

        $report->unindent();

        return $changes;
    }

    protected function _apiFetchPriceLists( \Hotlink\Framework\Model\Interaction\Environment\AbstractEnvironment $env,
                                            array $priceLists,
                                            array $skus )
    {
        $report    = $this->getReport();
        $api       = $this->apiServiceWorkflowHelper;
        $exception = null;
        $apiLists  = array();

        try
            {
                $apiLists = $report( $api, 'getProductPricing', $env->getStoreId(), $env->getAccountCode(), $priceLists, $skus );
            }
        catch ( \Hotlink\Framework\Model\Exception\Base $e )
            {
                $exception = $e;
            }
        catch ( \Hotlink\Brightpearl\Model\Exception\AbstractException $e )
            {
                $exception = $e;
            }

        if ( $exception )
            {
                $report->error( "Unable to fetch product pricing from Brightpearl", $exception );
                return null;
            }

        $indexId = \Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Price\Lists::ID;
        $indexPrices = \Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Price\Lists::PRICES;

        // aggregate lists
        $resultLists = array();
        foreach ( $apiLists as $list )
            {
                $id     = $list[ $indexId     ];
                $prices = $list[ $indexPrices ];

                if ( isset( $resultLists[ $id ] ) )
                    {
                        $prices = $resultLists[ $id ][ $indexPrices ];
                        $resultLists[ $id ][ $indexPrices ] = array_merge( $prices, $prices );
                    }
                else
                    {
                        $resultLists[ $id ] = $list;
                    }
            }
        return $resultLists;
    }

    protected function getOrCreateEnvironment($storeId)
    {
        return $this->hasEnvironment($storeId)
            ? $this->getEnvironment($storeId)
            : $this->createEnvironment($storeId);
    }

    protected function _checkPriceListHeader(array $apiPriceLists, \Magento\Store\Model\Website $website)
    {
        $wCurr      = $website->getBaseCurrencyCode();
        $wCode      = $website->getName();
        $wIncTax    = $website->getConfig( \Magento\Tax\Model\Config::CONFIG_XML_PATH_PRICE_INCLUDES_TAX );
        $wIncTaxTxt = $wIncTax ? 'Including Tax' : 'Excluding Tax';

        $report = $this->getReport();
        $valid = true;
        foreach ($apiPriceLists as $_list) {
            $lCode      = $_list[\Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Price\Lists::CODE];
            $lCurr      = $_list[\Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Price\Lists::CURRENCY_CODE];
            $lIncTax    = $_list[\Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Price\Lists::GROSS];
            $lIncTaxTxt = $lIncTax ? 'Including Tax' : 'Excluding Tax';

            $incompatibilities = array();
            if ($lCurr != $wCurr) {
                $incompatibilities[] = "[$wCurr] <> [$lCurr]";
            }
            if ($lIncTax != $wIncTax) {
                $incompatibilities[] = "[$wIncTaxTxt] <> [$lIncTaxTxt]";
            }
            if ($incompatibilities) {
                $err = implode(',', $incompatibilities);
                $report->error("Website [$wCode] and List [$lCode] have incompatible settings: $err");
                $valid = false;
            }
        }

        return $valid;
    }

    protected function getDiscountPercent($price, array $apiPriceLists, $basePriceList, $sku)
    {
        $report          = $this->getReport();
        $discountPercent = null;

        $basePrice = $this->_getSkuSinglePrice($apiPriceLists, $basePriceList, $sku);
        if ($basePrice) {

            if ((float) $basePrice === 0.0) {
                $report->warn("[$sku] could not calculate discount percent - division by 0 (zero)");
            }
            else if ((float) $basePrice < $price) {
                $report->warn("[$sku] could not calculate discount percent - base price < price");
            }
            else {
                $discountPercent = (1 - ($price / $basePrice)) * 100;
            }
        }
        else {
            $report->warn("[$sku] could not calculate discount percent - base price missing");
        }

        return $discountPercent;
    }

    protected function getPercent($price, array $apiPriceLists, $basePriceList, $sku)
    {
        $report  = $this->getReport();
        $percent = null;

        if ($basePrice = $this->_getSkuSinglePrice($apiPriceLists, $basePriceList, $sku)) {

            if ((float) $basePrice === 0.0) {
                $report->warn("[$sku] could not calculate percent - division by 0 (zero)");
            }
            else if ($basePrice < $price) {
                $report->warn("[$sku] could not calculate percent - base price < price");
            }
            else {
                $percent = 100 * $price / $basePrice;
            }
        }
        else {
            $report->warn("[$sku] could not calculate percent - base price missing");
        }

        return $percent;
    }

    protected function _getSkuSinglePrice(array $lists, $listId, $sku)
    {
        if ($prices = $this->getSkuListPrices($lists, $listId, $sku)) {

            if (isset($prices[1])) {
                return $prices[1];
            }
        }

        return null;
    }

    protected function _getSkuMultiPrice(array $lists, $listId, $sku)
    {
        if ($prices = $this->getSkuListPrices($lists, $listId, $sku)) {
            return $prices;
        }

        return null;
    }

    protected function getSkuListPrices(array $lists, $listId, $sku)
    {
        if (isset($lists[$listId][\Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Price\Lists::PRICES][$sku])) {

            return $lists[$listId][\Hotlink\Brightpearl\Model\Config\Source\Brightpearl\Price\Lists::PRICES][$sku];
        }

        return null;
    }

    protected function _saveProduct(\Magento\Catalog\Model\Product $product)
    {
        $sleep  = $this->getEnvironment()->getSleep();
        $report = $this->getReport();
        $sku    = $product->getSku();
        $idle   = 0;

        try {

            $product->getResource()->isPartialSave(true);
            $product->save();

            if ($sleep > 0) {
                $idleStart = $this->clockHelper->microtime_float();
                usleep($sleep);
                $idleEnd = $this->clockHelper->microtime_float();
                $idle = $idleEnd - $idleStart;
            }

            $report->info("[$sku] successfully saved")->incSuccess();
        }
        catch ( \Exception $e) {
            $report->error("[$sku] unable to save - ".$e->getMessage())->incFail();
        }

        return $idle;
    }

    protected function getWebsites(\Hotlink\Framework\Model\Interaction\Environment\AbstractEnvironment $env)
    {
        $report    = $this->getReport();
        $validWIds = array();

        $param = $env->getParameter('websites')->getValue();
        if (is_null($param) || (is_array($param) && empty($param))) {
            $report->error('Website(s) is required');
        }
        else {
            // collect ids
            $websiteIds = array();

            if (isset($param['admin'])) {
                $websiteIds = array_values($param['admin']);
            }
            if (isset($param['website'])) {
                $websiteIds = array_merge($websiteIds, array_values($param['website']));
            }

            foreach ($websiteIds as $wId) {
                try {

                    $this->storeManager->getWebsite($wId); // Magento throws exception on invalid website id.
                    $validWIds[] = $wId;
                }
                catch ( \Exception $e) {
                    $report->error("Invalid website with id [$wId]");
                }
            }
        }

        return $validWIds;
    }

    protected function getAdminWebsiteId()
    {
        return 0;
    }

}
