<?php
namespace Hotlink\Brightpearl\Model\Lookup\Order;

class Status extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init( 'Hotlink\Brightpearl\Model\ResourceModel\Lookup\Order\Status' );
    }
}