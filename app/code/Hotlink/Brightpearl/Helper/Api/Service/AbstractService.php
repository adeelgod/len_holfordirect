<?php
namespace Hotlink\Brightpearl\Helper\Api\Service;

abstract class AbstractService extends \Hotlink\Brightpearl\Helper\Api\AbstractApi
{

    protected $brightpearlConfigAuthorisation;
    protected $brightpearlApiServiceTransportFactory;

    public function __construct(
        \Hotlink\Brightpearl\Helper\Exception $exceptionHelper,
        \Hotlink\Framework\Helper\Report $reportHelper,
        \Hotlink\Brightpearl\Model\Config\Api $brightpearlConfigApi,

        \Hotlink\Brightpearl\Model\Config\Authorisation $brightpearlConfigAuthorisation,
        \Hotlink\Brightpearl\Model\Api\Service\TransportFactory $brightpearlApiServiceTransportFactory
    )
    {
        parent::__construct( $exceptionHelper, $reportHelper, $brightpearlConfigApi );
        $this->brightpearlConfigAuthorisation = $brightpearlConfigAuthorisation;
        $this->brightpearlApiServiceTransportFactory = $brightpearlApiServiceTransportFactory;
    }

    protected function _getTransport($storeId = null, $timeout = null)
    {
        $config    = $this->brightpearlConfigAuthorisation;
        $transport = $this->brightpearlApiServiceTransportFactory->create();

        $transport
            ->setBaseUrl( $config->getApiDomain( $storeId ) )
            ->setToken( $config->getToken( $storeId ) );

        if ( !is_null( $timeout ) ) {
            $transport->setTimeout( $timeout );
        }

        return $transport;
    }

}