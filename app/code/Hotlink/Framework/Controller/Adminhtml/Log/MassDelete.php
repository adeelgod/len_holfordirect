<?php
namespace Hotlink\Framework\Controller\Adminhtml\Log;

class MassDelete extends \Hotlink\Framework\Controller\Adminhtml\Log\AbstractLog
{

    protected $filter;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,

        \Magento\Framework\Registry $registry,
        \Hotlink\Framework\Helper\Report $reportHelper,
        \Magento\Ui\Component\MassAction\Filter $filter
    )
    {
        parent::__construct( $context,
                             $registry,
                             $reportHelper
        );
        $this->filter = $filter;
    }

    public function execute()
    {
        $component = $this->filter->getComponent();
        $this->filter->prepareComponent($component);
        $this->filter->applySelectionOnTargetProvider();
        $dataProvider = $component->getContext()->getDataProvider();

        $items = $dataProvider->getSearchResult()->getItems() ?: [];

        $entryIds = array_keys( $items );
        if ( !is_array( $entryIds ) ) {
            $this->messageManager->addError( __('Please select record(s)') );
        }
        else {
            try {
                foreach ( $entryIds as $entryId ) {
                    $this->reportHelper->delete( $entryId );
                }
                $this->messageManager->addSuccess( __('Total of %1 record(s) successfully deleted', count( $entryIds ) ) );
            }
            catch ( \Exception $e ) {
                $this->messageManager->addError( $e->getMessage() );
            }
        }

        return $this->_redirect( '*/*/' );
    }

}
