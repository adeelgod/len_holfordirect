<?php
namespace Hotlink\Framework\Model\Report;

interface IReportData
{

    public function getReportDataRenderer();

}
