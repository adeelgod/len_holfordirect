<?php

/**
 * Copyright © 2015 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\OrdersExportTool\Helper;

/**
 *  common helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_modelProduct = null;
    protected $_messageManager = null;

    /**
     *
     * @param \Magento\Framework\App\Helper\Context       $context
     * @param \Magento\Store\Model\StoreManager           $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Catalog\Model\Product              $modelProduct
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\Product $modelProduct,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_modelProduct = $modelProduct;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execPhp(
        $script,
        $order = null,
        $data = [],
        $item = null
    ) {
        return "Php features are disabled. In order to activate them please have a look at : https://www.wyomind.com/magento2/orders-export-tool-magento.html?section=faq";
    }

    public function executePhpScripts(
        $preview,
        $output,
        $order = null,
        $data = [],
        $item = null
    ) {

        $matches = [];
        preg_match_all("/(?<script><\?php(?<php>.*)\?>)/sU", $output, $matches);

        $i = 0;
        foreach (array_values($matches["php"]) as $phpCode) {
            $val = null;

            $displayErrors = ini_get("display_errors");
            ini_set("display_errors", 0);

            if (($val = $this->execPhp($phpCode, $order, $data, $item)) === false) {
                if ($preview) {
                    ini_set("display_errors", $displayErrors);
                    throw new \Exception("Syntax error in " . $phpCode . " : " . error_get_last()["message"]);
                } else {
                    ini_set("display_errors", $displayErrors);
                    $this->messageManager->addError("Syntax error in <i>" . $phpCode . "</i><br>." . error_get_last()["message"]);
                    throw new \Exception();
                }
            }
            ini_set("display_errors", $displayErrors);

            if (is_array($val)) {
                $val = implode(",", $val);
            }
            $output = str_replace($matches["script"][$i], $val, $output);
            $i++;
        }

        return $output;
    }

    /**
     * Get all db instances
     * @return array
     */
    public function getEntities()
    {
        return [
            "order" => [
                "code" => "order",
                "label" => "Order",
                "syntax" => "order",
                "table" => "sales_order",
                "filterable" => true
            ],
            "order_item" => [
                "code" => "order_item",
                "label" => "Product",
                "syntax" => "product",
                "table" => "sales_order_item",
                "filterable" => true
            ],
            "order_shipping_address" => [
                "code" => "order_shipping_address",
                "label" => "Shipping address",
                "syntax" => "shipping",
                "table" => "sales_order_address",
                "filterable" => false
            ],
            "order_billing_address" => [
                "code" => "order_billing_address",
                "label" => "Billing address",
                "syntax" => "billing",
                "table" => "sales_order_address",
                "filterable" => false
            ],
            "order_payment" => [
                "code" => "order_payment",
                "label" => "Payment",
                "syntax" => "payment",
                "table" => "sales_order_payment",
                "filterable" => false
            ],
            "invoice" => [
                "code" => "invoice",
                "label" => "Invoice",
                "syntax" => "invoice",
                "table" => "sales_invoice",
                "filterable" => true
            ],
            "shipment" => [
                "code" => "shipment",
                "label" => "Shipment",
                "syntax" => "shipment",
                "table" => "sales_shipment",
                "filterable" => false
            ],
            "creditmemo" => [
                "code" => "creditmemo",
                "label" => "Creditmemo",
                "syntax" => "creditmemo",
                "table" => "sales_creditmemo",
                "filterable" => false
            ]
        ];
    }

    /**
     * Order the item of an array
     * @param type $a
     * @param type $b
     * @return boolean
     */
    public static function cmp(
        $a,
        $b
    ) {
        return ($a['field'] < $b['field']) ? (-1) : 1;
    }

    /**
     * return the porfile from which the order item must be exported
     * @param type $_item
     * @return string
     */
    public function getExportTo($_item)
    {
        if ($_item->getExportTo()) {
            return $_item->getExportTo();
        } else {
            return $this->_modelProduct->load($_item->getProductId())->getData('export_to');
        }
    }

    public function stripTagsContent(
        $text,
        $tags = '',
        $invert = false
    ) {

        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if (is_array($tags) and count($tags) > 0) {
            if ($invert == false) {
                return preg_replace('@<(?!(?:' . implode('|', $tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            } else {
                return preg_replace('@<(' . implode('|', $tags) . ')\b.*?>.*?</\1>@si', '', $text);
            }
        } elseif ($invert == false) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return strip_tags($text);
    }
}
