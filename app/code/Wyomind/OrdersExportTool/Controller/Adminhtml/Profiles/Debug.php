<?php

/**
 * Copyright © 2015 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\OrdersExportTool\Controller\Adminhtml\Profiles;

/**
 * Debug profiles action
 */
class Debug extends \Wyomind\OrdersExportTool\Controller\Adminhtml\Profiles\AbstractProfiles
{

    /**
     * Execute action
     */
    public function execute()
    {
        $request = $this->getRequest();

        $id = $request->getParam('id');
        $limit = $request->getParam('limit');

        $model = $this->_objectManager->create('Wyomind\OrdersExportTool\Model\Profiles');
        $model->load($id);
        $model->limit = $limit;
        $model->debugEnabled = true;
        $model->logEnabled = true;

        try {
            $model->generate($request);
            $resultRaw = $this->_resultRawFactory->create();
            return $resultRaw->setContents($model->debugData);
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Unable to generate the data feed.') . '<br/><br/>' . $e->getMessage());
        }
    }
}
