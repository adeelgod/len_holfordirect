<?php

/*
 * Copyright © 2015 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\OrdersExportTool\Observer;

class CheckoutSubmitAllAfter implements \Magento\Framework\Event\ObserverInterface
{

    protected $_helperData;
    protected $_modelProfiles;

    /**
     *
     * @param \Wyomind\Core\Helper\Data                $helperData
     * @param \Wyomind\OrdersExportTool\Model\Profiles $modelProfiles
     */
    public function __construct(
        \Wyomind\Core\Helper\Data $helperData,
        \Wyomind\OrdersExportTool\Model\ProfilesFactory $modelProfilesFactory
    ) {
        $this->_helperData = $helperData;
        $this->_modelProfiles = $modelProfilesFactory->create();
    }

    /**
     * Execute on or several profile on event order submit after all
     * @param type $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $collection = $this->_modelProfiles->getCollection()->searchProfiles(explode(',', $this->_helperData->getStoreConfig("ordersexporttool/advanced/execute_on_checkout")));
        foreach ($collection as $profile) {
            if ($profile->getId()) {
                $profile->generate();
            }
        }
    }
}
