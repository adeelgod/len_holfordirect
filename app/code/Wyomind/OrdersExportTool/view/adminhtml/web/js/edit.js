/**
 * Copyright © 2015 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
OrdersExportTool = {
    action: {
        delete: function () {

        },
        updateExportTo: function () {

        },
    },
    filters: {
        updateStates: function () {
            var values = new Array();
            jQuery('.state').each(function (i) {
                if (jQuery(this).prop('checked')) {
                    values.push(jQuery(this).attr('identifier'));
                }
            });
            jQuery('#states').val(values.join());
            OrdersExportTool.filters.updateUnSelectLinksStates();
        },
        loadStates: function () {
            var values = jQuery('#states').val();
            values = values.split(',');
            values.each(function (v) {
                jQuery('#state_' + v).prop('checked', true);
                jQuery('#state_' + v).parent().addClass('selected');
            });
        },
        updateCustomerGroups: function () {
            var values = new Array();
            jQuery('.customer_group').each(function (i) {
                if (jQuery(this).prop('checked')) {
                    values.push(jQuery(this).attr('identifier'));
                }
            });
            jQuery('#customer_groups').val(values.join());
            OrdersExportTool.filters.updateUnSelectLinksCustomerGroups();
        },
        loadCustomerGroups: function () {
            var values = jQuery('#customer_groups').val();
            values = values.split(',');
            values.each(function (v) {
                jQuery('#customer_group_' + v).prop('checked', true);
                jQuery('#customer_group_' + v).parent().addClass('selected');
            });
        },
        isAllStatesSelected: function () {
            var all = true;
            jQuery(document).find('.state').each(function () {
                if (jQuery(this).prop('checked') === false)
                    all = false;
            });
            return all;
        },
        isAllCustomerGroupsSelected: function () {
            var all = true;
            jQuery(document).find('.customer_group').each(function () {
                if (jQuery(this).prop('checked') === false)
                    all = false;
            });
            return all;
        },
        updateUnSelectLinks: function () {
            OrdersExportTool.filters.updateUnSelectLinksStates();
            OrdersExportTool.filters.updateUnSelectLinksCustomerGroups();
        },
        updateUnSelectLinksStates: function () {
            if (OrdersExportTool.filters.isAllStatesSelected()) {
                jQuery('#states-selector').find('.select-all').removeClass('visible');
                jQuery('#states-selector').find('.unselect-all').addClass('visible');
            } else {
                jQuery('#states-selector').find('.select-all').addClass('visible');
                jQuery('#states-selector').find('.unselect-all').removeClass('visible');
            }
        },
        updateUnSelectLinksCustomerGroups: function () {
            if (OrdersExportTool.filters.isAllCustomerGroupsSelected()) {
                jQuery('#customer-groups-selector').find('.select-all').removeClass('visible');
                jQuery('#customer-groups-selector').find('.unselect-all').addClass('visible');
            } else {
                jQuery('#customer-groups-selector').find('.select-all').addClass('visible');
                jQuery('#customer-groups-selector').find('.unselect-all').removeClass('visible');
            }
        },
        selectAll: function (elt) {
            var fieldset = elt.parents('.fieldset')[0];
            jQuery(fieldset).find('input[type=checkbox]').each(function () {
                jQuery(this).prop('checked', true);
                jQuery(this).parent().addClass('selected');
            });
            OrdersExportTool.filters.updateStates();
            OrdersExportTool.filters.updateCustomerGroups();
            elt.removeClass('visible');
            jQuery(fieldset).find('.unselect-all').addClass('visible');
        },
        /**
         * Click on unselect all link
         * @param {type} elt
         * @returns {undefined}
         */
        unselectAll: function (elt) {
            var fieldset = elt.parents('.fieldset')[0];
            jQuery(fieldset).find('input[type=checkbox]').each(function () {
                jQuery(this).prop('checked', false);
                jQuery(this).parent().removeClass('selected');
            });
            OrdersExportTool.filters.updateStates();
            OrdersExportTool.filters.updateCustomerGroups();
            elt.removeClass('visible');
            jQuery(fieldset).find('.select-all').addClass('visible');
        },
        loadAdvancedFilters: function () {
            if (jQuery('#attributes').val() == "") {
                jQuery('#attributes').val("[]");
            }
            var filters = jQuery.parseJSON(jQuery('#attributes').val());
            if (filters === null) {
                filters = new Array();
                jQuery('#attributes').val(JSON.stringify(filters));
            }
            var counter = 0;
            while (filters[counter]) {
                filter = filters[counter];


                jQuery('#attribute_' + counter).prop('checked', filter.checked);
                jQuery('#name_attribute_' + counter).val(filter.code);
                jQuery('#value_attribute_' + counter).val(filter.value);
                jQuery('#condition_attribute_' + counter).val(filter.condition);
                // OrdersExportTool.filters.updateRow(counter, filter.code);
                jQuery('#name_attribute_' + counter).prop('disabled', !filter.checked);
                jQuery('#condition_attribute_' + counter).prop('disabled', !filter.checked);
                jQuery('#value_attribute_' + counter).prop('disabled', !filter.checked);
                jQuery('#pre_value_attribute_' + counter).prop('disabled', !filter.checked);
                jQuery('#pre_value_attribute_' + counter).val(filter.value);
                counter++;
            }
        },
        updateAdvancedFilters: function () {
            var newval = {};
            var counter = 0;
            jQuery('.advanced_filters').each(function () {
                var checkbox = jQuery(this).find('#attribute_' + counter).prop('checked');
                // is the row activated
                if (checkbox) {
                    jQuery('#name_attribute_' + counter).prop('disabled', false);
                    jQuery('#condition_attribute_' + counter).prop('disabled', false);
                    jQuery('#value_attribute_' + counter).prop('disabled', false);
                } else {
                    jQuery('#name_attribute_' + counter).prop('disabled', true);
                    jQuery('#condition_attribute_' + counter).prop('disabled', true);
                    jQuery('#value_attribute_' + counter).prop('disabled', true);
                }

                var name = jQuery(this).find('#name_attribute_' + counter).val();
                var condition = jQuery(this).find('#condition_attribute_' + counter).val();
                var value = jQuery(this).find('#value_attribute_' + counter).val();
                var val = {checked: checkbox, code: name, condition: condition, value: value};
                newval[counter] = val;
                counter++;
            });
            jQuery('#attributes').val(JSON.stringify(newval));
        },
        updateRow: function (id, attribute_code) {

            jQuery('#name_attribute_' + id).parent().removeClass('multiple-value').addClass('one-value').removeClass('dddw');
            if (jQuery('#condition_attribute_' + id).val() === "null" || jQuery('#condition_attribute_' + id).val() === "notnull") {
                jQuery('#value_attribute_' + id).css('display', 'none');
            } else {
                jQuery('#value_attribute_' + id).css('display', 'inline');
            }

        }
    },
    templater: {
        current: null,
        currentValue: 0,
        clear: function () {
            jQuery('#extra_header').val('');
            jQuery('#header').val('');
            jQuery('#body').val('');
            jQuery('#footer').val('');
        },
        switch : function () {

            if (OrdersExportTool.templater.current != OrdersExportTool.templater.getType()) {
                if (confirm("Changing file type from/to xml will clear all your setting.\ Do you want to continue ?")) {
                    OrdersExportTool.templater.clear();
                    if (OrdersExportTool.templater.getType() == "xml") {
                        OrdersExportTool.templater.xml.switchTo();
                    } else {
                        OrdersExportTool.templater.txt.switchTo();
                    }

                } else {
                    OrdersExportTool.templater.setTypeValue(OrdersExportTool.templater.currentValue);
                }
            }
        }
        ,
        initialize: function () {
            if (OrdersExportTool.templater.getType() == "xml") {
                OrdersExportTool.templater.xml.switchTo();
            } else {
                OrdersExportTool.templater.txt.switchTo();
            }
        },
        getTypeValue: function () {
            return jQuery('#type').val();
        },
        getType: function () {
            if (jQuery('#type').val() == 1)
                return "xml";
            else
                return "txt";
        },
        setTypeValue: function (value) {
            jQuery('#type').val(value)
        },
        txt: {
            popup: {
                current: null,
                close: function () {
                    jQuery(".overlay-txtTemplate").css({"display": "none"});
                },
                validate: function () {
                    jQuery(OrdersExportTool.templater.txt.popup.current).val(CodeMirrorTxt.getValue());
                    OrdersExportTool.templater.txt.popup.current = null;
                    OrdersExportTool.templater.txt.popup.close();
                    OrdersExportTool.templater.txt.fieldsToJson();
                },
                open: function (content, field) {
                    jQuery(".overlay-txtTemplate").css({"display": "block"});
                    OrdersExportTool.templater.codeMirrorTxt.refresh();
                    CodeMirrorTxt.setValue(content);
                    OrdersExportTool.templater.txt.popup.current = field;

                }
            },
            switchTo: function () {
                jQuery('#header').parents(".field").css("display", "none");
                jQuery('#body').parents(".field").css("display", "none");
                jQuery(".txt-type").each(function () {
                    jQuery(this).parents(".field").css("display", "block");
                });
                jQuery(".txt-type:not(.not-required)").each(function () {
                    jQuery(this).addClass("required-entry")
                });
                jQuery(".xml-type").each(function () {
                    jQuery(this).parents(".field").css("display", "none");
                    jQuery(this).removeClass("required-entry");
                });
                OrdersExportTool.templater.current = "txt";
                OrdersExportTool.templater.currentValue = OrdersExportTool.templater.getTypeValue();
                content = "<div id='fields'>";
                content += "     Column name";
                content += "      <span style='margin-left:96px'>Pattern</span>";
                content += "<ul class='fields-list' id='fields-list'></ul>";
                content += "<button type='button' class='add-field' onclick='OrdersExportTool.templater.txt.addField(\"\",\"\",true)'>Insert a new field</button>";
                content += "<div class='overlay-txtTemplate'>\n\
                            <div class='container-txtTemplate'> \n\
                            <textarea id='codemirror-txtTemplate'>&nbsp;</textarea>\n\
                            <button type='button' class='validate' onclick='OrdersExportTool.templater.txt.popup.validate()'>Validate</button><button type='button' class='cancel' onclick='OrdersExportTool.templater.txt.popup.close()'>Cancel</button>\n\
                            </div>\n\
                            </div>";
                content += "</div>";
                jQuery(content).insertAfter("#extra_header")

                CodeMirrorTxt = CodeMirror.fromTextArea(document.getElementById('codemirror-txtTemplate'), {
                    matchBrackets: true,
                    mode: "application/x-httpd-php",
                    indentUnit: 2,
                    indentWithTabs: false,
                    lineWrapping: true,
                    lineNumbers: false,
                    styleActiveLine: true
                });

                OrdersExportTool.templater.txt.jsonToFields();
                jQuery("#fields-list").sortable({
                    revert: true,
                    axis: "y",
                    stop: function () {
                        OrdersExportTool.templater.txt.fieldsToJson();
                    }
                });
                OrdersExportTool.templater.codeMirror.destroy();
                OrdersExportTool.templater.txt.fieldsToJson();
            },
            addField: function (header, body, refresh) {
                content = "<li class='txt-fields'>";
                content += "   <input class='txt-field  header-txt-field input-text ' type='text' value=\"" + header.replace(/"/g, "&quot;") + "\"/>";
                content += "   <input class='txt-field  body-txt-field input-text ' type='text' value=\"" + body.replace(/"/g, "&quot;") + "\"/>";
                content += "   <input type='button' class='txt-field remove-field' onclick='OrdersExportTool.templater.txt.removeField(this)' value='\u2716'\n\/>";
                content += "</li>";
                jQuery("#fields-list").append(content);
                if (refresh)
                    OrdersExportTool.templater.txt.fieldsToJson();
            },
            removeField: function (elt) {
                jQuery(elt).parents('li').remove();
                OrdersExportTool.templater.txt.fieldsToJson();
            },
            fieldsToJson: function () {
                data = new Object;
                data.header = new Array;
                c = 0;
                jQuery('INPUT.header-txt-field').each(function () {
                    data.header[c] = jQuery(this).val();
                    c++;
                });
                data.body = new Array;
                c = 0;
                jQuery('INPUT.body-txt-field').each(function () {
                    data.body[c] = jQuery(this).val();
                    c++;
                });
                jQuery('#header').val('{"header":' + JSON.stringify(data.header) + "}");
                jQuery('#body').val('{"body":' + JSON.stringify(data.body) + "}");
            },
            jsonToFields: function () {
                data = new Object;
                if (jQuery('#header').val() == '') {
                    jQuery('#header').val('{"header":[]}');
                }
                var header = [];
                try {
                    header = jQuery.parseJSON(jQuery('#header').val()).header;
                } catch(e) {
                    header = [];
                }
                
                if (jQuery('#body').val() == '') {
                    jQuery('#body').val('{"body":[]}');
                } 
                
                var body = [];
                try {
                    body = jQuery.parseJSON(jQuery('#body').val()).body;
                } catch(e) {
                    body = [];
                }
                 

                data.header = header;
                data.body = body;

                i = 0;
                data.body.each(function () {
                    OrdersExportTool.templater.txt.addField(data.header[i], data.body[i], false);
                    i++;
                })
            }
        },
        xml: {
            switchTo: function () {

                jQuery('#header').parents(".field").css("display", "block");
                jQuery('#body').parents(".field").css("display", "block");
                jQuery('#ordersexporttool_form').removeClass('text')

                jQuery(".xml-type").each(function () {
                    jQuery(this).addClass("required-entry")
                    jQuery(this).parents(".field").css("display", "block");
                });
                jQuery(".txt-type").each(function () {
                    jQuery(this).removeClass("required-entry")
                    jQuery(this).parents(".field").css("display", "none");
                });
                OrdersExportTool.templater.current = "xml";
                OrdersExportTool.templater.currentValue = OrdersExportTool.templater.getTypeValue();
                jQuery('#fields').remove();
                OrdersExportTool.templater.codeMirror.enable()
            }

        },
        codeMirrorTxt: {
            refresh: function () {
                setTimeout(function () {
                    CodeMirrorTxt.refresh();
                }, 1000);
            }

        },
        codeMirror: {
            enable: function () {
                CodeMirrorHeader = CodeMirror.fromTextArea(document.getElementById('header'), {
                    matchBrackets: true,
                    mode: "application/x-httpd-php",
                    indentUnit: 2,
                    indentWithTabs: false,
                    lineWrapping: true,
                    lineNumbers: true,
                    styleActiveLine: true
                });
                CodeMirrorBody = CodeMirror.fromTextArea(document.getElementById('body'), {
                    matchBrackets: true,
                    mode: "application/x-httpd-php",
                    indentUnit: 2,
                    indentWithTabs: false,
                    lineWrapping: true,
                    lineNumbers: true,
                    styleActiveLine: true
                });
                CodeMirrorFooter = CodeMirror.fromTextArea(document.getElementById('footer'), {
                    matchBrackets: true,
                    mode: "application/x-httpd-php",
                    indentUnit: 2,
                    indentWithTabs: false,
                    lineWrapping: true,
                    lineNumbers: true,
                    styleActiveLine: true
                });
                // to be sure that the good value will be well stored in db
                CodeMirrorHeader.on('blur', function () {
                    jQuery('#header').val(CodeMirrorHeader.getValue());
                }); // to be sure that the good value will be well stored in db
                CodeMirrorBody.on('blur', function () {
                    jQuery('#body').val(CodeMirrorBody.getValue());
                });
                // to be sure that the good value will be well stored in db
                CodeMirrorFooter.on('blur', function () {
                    jQuery('#footer').val(CodeMirrorFooter.getValue());
                });
            },
            refresh: function () {
                setTimeout(function () {
                    CodeMirrorHeader.refresh();
                    CodeMirrorBody.refresh();
                    CodeMirrorFooter.refresh();
                }, 1000);

            },
            destroy: function () {

                if (typeof CodeMirrorHeader != 'undefined')
                    CodeMirrorHeader.toTextArea();
                if (typeof CodeMirrorBody != 'undefined')
                    CodeMirrorBody.toTextArea();
                if (typeof CodeMirrorFooter != 'undefined')
                    CodeMirrorFooter.toTextArea();
            }

        }
    },
    /**
     * All about cron tasks
     */
    cron: {
        /**
         * Load the selected days and hours
         */
        loadExpr: function () {
            if (jQuery('#scheduled_task').val() == "") {
                jQuery('#scheduled_task').val('{"days":[],"hours":[]}');
            }
            var val = jQuery.parseJSON(jQuery('#scheduled_task').val());
            if (val !== null) {
                val.days.each(function (elt) {
                    jQuery('#d-' + elt).parent().addClass('selected');
                    jQuery('#d-' + elt).prop('checked', true);
                });
                val.hours.each(function (elt) {
                    var hour = elt.replace(':', '');
                    jQuery('#h-' + hour).parent().addClass('selected');
                    jQuery('#h-' + hour).prop('checked', true);
                });
            }
        },
        /**
         * Update the json representation of the cron schedule
         */
        updateExpr: function () {
            var days = new Array();
            var hours = new Array();
            jQuery('.cron-box.day').each(function () {
                if (jQuery(this).prop('checked') === true) {
                    days.push(jQuery(this).attr('value'));
                }
            });
            jQuery('.cron-box.hour').each(function () {
                if (jQuery(this).prop('checked') === true) {
                    hours.push(jQuery(this).attr('value'));
                }
            });
            jQuery('#scheduled_task').val(JSON.stringify({days: days, hours: hours}));
        }
    },
    /**
     * All about Preview/Library boxes
     */
    boxes: {
        library: false,
        preview: false,
        init: function () {
            /* maxter box */
            jQuery('<div/>', {
                id: 'master-box',
                class: 'master-box'
            }).appendTo('#html-body');
            /* preview tag */
            jQuery('<div/>', {
                id: 'preview-tag',
                class: 'preview-tag box-tag'
            }).appendTo('#html-body');
            jQuery('<div/>', {
                text: 'Preview'
            }).appendTo('#preview-tag');
            /* library tag */
            jQuery('<div/>', {
                id: 'library-tag',
                class: 'library-tag box-tag'
            }).appendTo('#html-body');
            jQuery('<div/>', {
                text: 'Library'
            }).appendTo('#library-tag');

            /* preview tab */
            jQuery('<div/>', {// preview master box
                id: 'preview-master-box',
                class: 'preview-master-box visible'
            }).appendTo('#master-box');
            jQuery('<span/>', {//  button
                id: 'preview-refresh-btn',
                class: 'preview-refresh-btn',
                html: '<span class="preview-refresh-btn-icon"></span> <span> Refresh</span>'
            }).appendTo('#preview-master-box');
            jQuery('<textarea/>', {// preview content
                id: 'preview-area',
                class: 'preview-area'
            }).appendTo('#preview-master-box');
            jQuery('<div/>', {// preview content
                id: 'preview-table-area',
                class: 'preview-table-area'
            }).appendTo('#preview-master-box');
            jQuery('<div/>', {// loader 
                id: 'preview-box-loader',
                class: 'box-loader',
                html: '<div class="ajax-loader"></load>'
            }).appendTo('#preview-master-box');
            /* library tab */
            jQuery('<div/>', {// library master box
                id: 'library-master-box',
                class: 'library-master-box visible'
            }).appendTo('#master-box');
            jQuery('<div/>', {// library content
                id: 'library-area',
                class: 'library-area'
            }).appendTo('#library-master-box');
            jQuery('<div/>', {// loader 
                id: 'library-box-loader',
                class: 'box-loader',
                html: '<div class="ajax-loader"></load>'
            }).appendTo('#library-master-box');

        },
        /**
         * Close the box
         * @returns {undefined}
         */
        hideLoaders: function () {
            jQuery(".box-loader").css("display", "none");
        },
        showLoader: function (name) {
            jQuery("#" + name + "-box-loader").css("display", "block");
        },
        close: function () {
            jQuery('.box-tag').each(function () {
                jQuery(this).removeClass('opened');
                jQuery(this).removeClass('selected');
            });
            jQuery('.master-box').removeClass('opened');
            jQuery('#library-master-box').removeClass('visible');
            jQuery('#preview-master-box').removeClass('visible');
            jQuery('#report-master-box').removeClass('visible');
        },
        /**
         * Open the preview box when no box opened
         * @returns {undefined}
         */
        openPreview: function () {
            jQuery("#preview-tag").addClass('selected');
            // translate tags
            jQuery('.box-tag').each(function () {
                jQuery(this).addClass('opened');
            });
            // translates main box
            jQuery('.master-box').addClass('opened');
            // on affiche le preview
            jQuery('#library-master-box').removeClass('visible');
            jQuery('#preview-master-box').addClass('visible');
            jQuery('#report-master-box').removeClass('visible');
        },
        /**
         * Open the library box when no box opened
         * @returns {undefined}
         */
        openLibrary: function () {
            jQuery("#library-tag").addClass('selected');
            // translate tags
            jQuery('.box-tag').each(function () {
                jQuery(this).addClass('opened');
            });
            // translates main box
            jQuery('.master-box').addClass('opened');
            // on affiche le preview
            jQuery('#library-master-box').addClass('visible');
            jQuery('#preview-master-box').removeClass('visible');
            jQuery('#report-master-box').removeClass('visible');
        },
        /**
         * Switch to the preview box
         * @returns {undefined}
         */
        switchToPreview: function () {
            jQuery('.box-tag').each(function () {
                jQuery(this).removeClass('selected');
            });
            jQuery("#preview-tag").addClass('selected');
            jQuery('#library-master-box').removeClass('visible');
            jQuery('#preview-master-box').addClass('visible');
            jQuery('#report-master-box').removeClass('visible');
        },
        /**
         * Switch to the library box
         * @returns {undefined}
         */
        switchToLibrary: function () {
            jQuery('.box-tag').each(function () {
                jQuery(this).removeClass('selected');
            });
            jQuery("#library-tag").addClass('selected');
            jQuery('#library-master-box').addClass('visible');
            jQuery('#preview-master-box').removeClass('visible');
            jQuery('#report-master-box').removeClass('visible');
        },
        /**
         * Refresh the preview
         * @returns {undefined}
         */
        refreshPreview: function () {
            if (!jQuery(this).hasClass('selected') && jQuery(this).hasClass('opened')) { // panneau ouvert sur library
                OrdersExportTool.boxes.switchToPreview();
            } else if (jQuery(this).hasClass('selected') && jQuery(this).hasClass('opened')) { // panneau ouvert sur preview
                OrdersExportTool.boxes.close();
            } else { // panneau non ouvert
                OrdersExportTool.boxes.openPreview();
            }
            var requestUrl = jQuery('#sample_url').val();
            /* initialize the preview box with CodeMirror */


            OrdersExportTool.boxes.showLoader("preview");
            if (typeof request != "undefined") {
                request.abort();
            }
            OrdersExportTool.boxes.preview = true;
            if (OrdersExportTool.templater.getType() == 'txt') {
                OrdersExportTool.templater.txt.fieldsToJson();
            }
            request = jQuery.ajax({
                url: requestUrl,
                type: 'POST',
                showLoader: false,
                data: jQuery("#edit_form").serialize(),
                success: function (data) {
//                    if (data.indexOf("ajaxExpired") !== -1) {
//                        data = "Expired Session";
//                    } 
                    if (OrdersExportTool.templater.getType() == 'txt') {
                        if (typeof CodeMirrorPreview != 'undefined')
                            CodeMirrorPreview.toTextArea();
                        TablePreview.innerHTML = data.data;
                        jQuery(TablePreview).css({display: 'block'});
                    } else {
                        TablePreview.innerHTML = null;
                        jQuery(TablePreview).css({display: 'none'});


                        if (typeof CodeMirrorPreview == 'undefined') {
                            CodeMirrorPreview = CodeMirror.fromTextArea(document.getElementById('preview-area'), {
                                matchBrackets: true,
                                mode: "application/x-httpd-php",
                                indentUnit: 2,
                                indentWithTabs: false,
                                lineWrapping: true,
                                lineNumbers: true,
                                styleActiveLine: true
                            });
                        }
                        CodeMirrorPreview.setValue(data.data);

                    }


                    OrdersExportTool.boxes.hideLoaders()
                },
                error: function (xhr, status, error) {
                    if (typeof CodeMirrorPreview != 'undefined')
                        CodeMirrorPreview.toTextArea();
                    TablePreview.innerHTML = error;
                    jQuery(TablePreview).css({display: 'block'});
                    OrdersExportTool.boxes.hideLoaders()

                }
            });
        },
        /**
         * Initialize the library boxe
         * @returns {undefined}
         */

        loadLibrary: function () {
            OrdersExportTool.boxes.library = true;
            var requestUrl = jQuery('#library_url').val();
            OrdersExportTool.boxes.showLoader("library");
            if (typeof request != "undefined") {
                request.abort();
            }
            request = jQuery.ajax({
                url: requestUrl,
                type: 'GET',
                showLoader: false,
                success: function (data) {

                    jQuery('#library-area').html(data);
                    OrdersExportTool.boxes.hideLoaders();
                }
            });
        },
        /**
         * Load a sample of product for an attribute in the library boxe
         * @param {type} elt
         * @returns {undefined}
         */
        loadLibrarySamples: function (elt) {
            var requestUrl = jQuery('#library_sample_url').val();
            var code = elt.attr('code');
            var field = elt.attr('field');
            //  var store_id = jQuery('#store_id').val();
            if (elt.find('span').hasClass('opened')) {
                elt.find('span').addClass('closed').removeClass('opened');
                elt.parent().next().find('td').html("");
                elt.parent().next().removeClass('visible');
                return;
            }

            if (typeof request != "undefined") {
                request.abort();
            }
            request = jQuery.ajax({
                url: requestUrl,
                data: {
                    code: code,
                    field: field,
                    // store_id: store_id,
                },
                type: 'GET',
                showLoader: false,
                success: function (data) {

                    data = jQuery.parseJSON(data);
                    elt.parent().next().addClass('visible');
                    var html = "<table class='inner-attribute'>";
                    if (data.length > 0) {
                        data.each(function (elt) {
                            html += "<tr><td class='name'><td><b>" + elt.value + "</b></td></tr>";
                        });
                        html += "</table>";
                    } else {
                        html = "No data found.";
                    }

                    elt.find('span').addClass('opened').removeClass('closed');
                    elt.parent().next().find('td').html(html);
                }
            });
        }
    }
};


require(["jquery", "mage/mage"], function ($) {
    $(function () {
        jQuery(document).ready(function () {
            /* ========= Editor templater ========================= */

            jQuery("#profiles_edidt_tabs_template_section").on('click', function () {
                if (OrdersExportTool.templater.getType() == "xml")
                    OrdersExportTool.templater.codeMirror.refresh();
            })



            OrdersExportTool.templater.initialize()

            jQuery("#type").on('change', function () {
                OrdersExportTool.templater.switch()
            })
            jQuery(".body-txt-field").on('change', function () {
                OrdersExportTool.templater.txt.fieldsToJson();
            })
            jQuery(".header-txt-field").on('change', function () {
                OrdersExportTool.templater.txt.fieldsToJson();
            })
            jQuery(document).on('focus',".body-txt-field", function () {
                OrdersExportTool.templater.txt.popup.open(jQuery(this).val(), this);
            });
            jQuery(document).on('focus',".header-txt-field", function () {
                OrdersExportTool.templater.txt.popup.open(jQuery(this).val(), this);
            });


            /* ========= Filters ========================= */

            /* select advanced filters */

            // change attribute select 
            jQuery(document).on('change', '.name-attribute,.condition-attribute', function (evt) {
                var id = jQuery(this).attr('identifier');
                var attribute_code = jQuery('#name_attribute_' + id).val();
                OrdersExportTool.filters.updateRow(id, attribute_code);
            });
            jQuery(document).on('change', '.checked-attribute,.statement-attribute,.name-attribute,.condition-attribute,.value-attribute', function (evt) {
                OrdersExportTool.filters.updateAdvancedFilters();
            });
            OrdersExportTool.filters.loadAdvancedFilters();
            jQuery(document).on("click", ".state", function (evt) {
                var elt = jQuery(this);
                elt.parent().toggleClass('selected');
                OrdersExportTool.filters.updateStates();
            });
            OrdersExportTool.filters.loadStates();
            jQuery(document).on("click", ".customer_group", function (evt) {
                var elt = jQuery(this);
                elt.parent().toggleClass('selected');
                OrdersExportTool.filters.updateCustomerGroups();
            });
            OrdersExportTool.filters.loadCustomerGroups();
            /* un/select all */
            jQuery(document).on("click", ".select-all", function (evt) {
                var elt = jQuery(this);
                OrdersExportTool.filters.selectAll(elt);
            });
            jQuery(document).on("click", ".unselect-all", function (evt) {
                var elt = jQuery(this);
                OrdersExportTool.filters.unselectAll(elt);
            });
            OrdersExportTool.filters.updateUnSelectLinks();
            /* select advanced filters */

            // change attribute select 
            jQuery(document).on('change', '.name-attribute,.condition-attribute', function (evt) {
                var id = jQuery(this).attr('identifier');
                var attribute_code = jQuery('#name_attribute_' + id).val();
                OrdersExportTool.filters.updateRow(id, attribute_code);
            });
            /* ========= Cron tasks  ================== */

            jQuery(document).on('change', '.cron-box', function () {
                jQuery(this).parent().toggleClass('selected');
                OrdersExportTool.cron.updateExpr();
            });
            OrdersExportTool.cron.loadExpr();
            /* template editor */



            /* ========= Preview + Library  ================== */

            OrdersExportTool.boxes.init();
            /* click on preview tag */
            jQuery(document).on('click', '.preview-tag.box-tag', function () {
                if (!jQuery(this).hasClass('selected') && jQuery(this).hasClass('opened')) { // panneau ouvert sur library
                    OrdersExportTool.boxes.switchToPreview();
                } else if (jQuery(this).hasClass('selected') && jQuery(this).hasClass('opened')) { // panneau ouvert sur preview
                    OrdersExportTool.boxes.close();
                } else { // panneau non ouvert
                    OrdersExportTool.boxes.openPreview();
                }
            });

            CodePreview = document.getElementById('preview-area');
            TablePreview = document.getElementById('preview-table-area');

            /* click on library tag */
            jQuery(document).on('click', '.library-tag.box-tag', function () {
                if (!jQuery(this).hasClass('selected') && jQuery(this).hasClass('opened')) { // panneau ouvert sur preview
                    OrdersExportTool.boxes.switchToLibrary();
                } else if (jQuery(this).hasClass('selected') && jQuery(this).hasClass('opened')) { // panneau ouvert sur library
                    OrdersExportTool.boxes.close();
                } else { // panneau non ouvert
                    OrdersExportTool.boxes.openLibrary();
                }
            });


            /* click on refresh preview */
            jQuery(document).on('click', '.preview-refresh-btn', function () {
                OrdersExportTool.boxes.refreshPreview();
            });
            /* intialize the library content */
            /* Click on one tag */
            jQuery(document).on("click", '.box-tag', function () {
                if (jQuery(this).hasClass("preview-tag") && !OrdersExportTool.boxes.preview) {
                    OrdersExportTool.boxes.refreshPreview();
                }
                if (jQuery(this).hasClass("library-tag") && !OrdersExportTool.boxes.library) {
                    OrdersExportTool.boxes.loadLibrary();
                }

            })
            /* click on an attribute load sample */
            jQuery(document).on('click', '.load-attr-sample', function () {
                // OrdersExportTool.boxes.loadLibrarySamples(jQuery(this));
            });
            /* Click in library tab*/
            jQuery(document).on("click", ".items-library LI", function () {

                id = jQuery(this).attr('id');
                jQuery(".items-library LI").removeClass("selected");
                jQuery(this).addClass("selected");
                jQuery(".items-library TABLE").each(function () {
                    jQuery(this).css("display", "none")
                });

                jQuery(".items-library TABLE." + id).css("display", "table");
            })



        });
    });
});